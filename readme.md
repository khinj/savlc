﻿SaVLC
======

[Magyar leírás itt.](md/readme_hun.md)

SaVLC was developed mainly to have subtitles placed on screen as we wish. This program can handle *.srt sublitles files (SubRip subtitle text file format). Font size can be adjusted using Ctrl + mouse-wheel. You can also set the font type, font color and background color of the text-boxes. SaVLC has other functions known from VLC mediaplayer, e.g. faster - slower tempo, A-B section, brightness, contrast, saturation settings. Extra function: connecting to [CedictServer](https://bitbucket.org/khinj/cedictserver), it promptly searches the selected Chinese words in the CEDICT dictionary.

The program and the video sample can be downloaded from here: [SaVLC.zip, SaVLC - Minta Video - Sample Video.zip](https://1drv.ms/f/s!AhEINfn0bmWKgQavfQq5bVxJpoRv). After unzipping run the SaVLC\release\SaVLC.exe program.

![Image](md/SaVLC.jpg "screenshot")

**Web:** [https://bitbucket.org/khinj/SaVLC](https://bitbucket.org/khinj/SaVLC).

**Short description:**

I started to develop SaVLC application to help my Chinese language studies. I wanted to have a video player similar to VLC, but handling subtitles more sophisticated, like seeing Chinese, pinyin and translation subtitles at the same time, adjusting font size, subtitle location on the display more easily. This application was developed on Windows 10 operation system.

**For users:**

Download the `SaVLC.zip` file from [OneDrive](https://1drv.ms/f/s!AhEINfn0bmWKgQavfQq5bVxJpoRv), and unpack it. The `saVLC.exe` program is in `SaVLC\Bin\` map.  
After starting the application, press F1 to see what functions are available using hotkeys.  
You can also download [SaVLC - Minta Video - Sample Video.zip](https://1drv.ms/f/s!AhEINfn0bmWKgQavfQq5bVxJpoRv) file, which contains sample video and subtitle files, with this, you can see how SaVLC works.

 

**Optional extensions:**

* [CedictServer](https://bitbucket.org/khinj/cedictserver): using this, you can see the english translation of the selected chinese subtitle text.

* tessdata: You can use OCR (Optical Character Recognition) with some limitation (it does not work well, when the video's subtitle color is similar to the background color). Download `chi_sim.traineddata` and/or `chi_tra.traineddata` files from [Data Files for Version 3.04/3.05](https://github.com/tesseract-ocr/tesseract/wiki/Data-Files#data-files-for-version-304305) of tesseract website, and put these files into `SaVLC\tessdata\` directory.

**For developers:**

I used these "ingredients" for developing SaVLC:

* Windows 10 operation system (Win OS)

* Qt4.8

* Qt Creator 2.5.2

* MinGW (gcc (GCC) 4.8.1)

* Git

* [vlc-3.0.21.tar.xz](https://download.videolan.org/pub/videolan/vlc/3.0.21/)

* [vlc-3.0.21-win32.zip](https://download.videolan.org/pub/videolan/vlc/3.0.21/win32/)

* tesseract (3.05.01)

* leptonica (commit 3860acc9bb03e7cf79c47e7b798f1bdfeae1a4e6, this was the latest commit in master branch at 2017-12-15 20:08:05).

You may use different versions, but greater the chance that you have to modify the *.pro files or source code for successful building (especially in case of Qt).

After successful build, the `SaVLC\bin_dev\` directory will contain the `saVLC.exe`. It will also contain `libtessQt.a` and `tessQt.dll` files, which can be used in other projects using `baseapi.h` of tesseract and `allheaders.h` of leptonica.

1.) After you have Win OS, Qt4.8, Qt Creator, MinGW, Git installed, clone the [git repo of SaVLC](https://bitbucket.org/khinj/savlc/src/master/), like this:

    git clone https://bitbucket.org/khinj/savlc.git

2.) From `vlc-x.x.x.tar.xz` copy the files form `vlc-x.x.x\include\vlc\` into `SaVLC\src\vlc\include\vlc\` (e.g. use [7-Zip](https://www.7-zip.org/)).

3.) From `vlc-x.x.x-win32.zip` copy the `plugins` map (with all its content) and `libvlc.dll` and `libvlccore.dll` files form `vlc-x.x.x\` into `SaVLC\bin\`.

4.) In git-bash, use these commands:  

    cd SaVLC\src\tess\  
    git clone https://github.com/tesseract-ocr/tesseract  
    cd tesseract  
    git checkout 3.05.01  
    cd ..  
    git clone https://github.com/DanBloomberg/eptonica.git  
    cd leptonica  
    git checkout 3860acc9bb03e7cf79c47e7b798f1bdfeae1a4e6

5.) Edit the `SaVLC\src\tess\leptonica\src\environ.h` file so that these macros have the value of 0, except USE_BMPIO has 1:

    #define  HAVE_LIBJPEG     0  
    #define  HAVE_LIBTIFF     0  
    #define  HAVE_LIBPNG      0  
    #define  HAVE_LIBZ        0  
    #define  HAVE_LIBGIF      0  
    #define  HAVE_LIBUNGIF    0  
    #define  HAVE_LIBWEBP     0  
    #define  HAVE_LIBJP2K     0  
    ...  
    #define  USE_BMPIO        1  
    #define  USE_PNMIO        0  
    #define  USE_JP2KHEADER   0  
    #define  USE_PDFIO        0  
    #define  USE_PSIO         0

6.) In `SaVLC\src\tess\tesseract\ccutil\ccutil.h` file, after the first "#ifdef _WIN32" row insert this row:

    #define WINDLLNAME "libtesseract.dll"

7.) In Qt Creator open the `SaVLC\src\SaVLC.pro` file and do your modifications, then build the project. Good luck!

**Licence:**

My added part is completely free. Please see the licenses of the above mentioned "ingredients" on their web site. No warranty.
