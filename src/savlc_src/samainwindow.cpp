﻿#include <assert.h>     /* assert */
#include "samainwindow.h"
#include "beallitasform.h"
#include "felirat.h"
#include "pipe.h"

extern void billKezel(QKeyEvent *);
extern bool ablakMozgatas;
extern double videoHossz, tA, tB;
extern QApplication app;
extern void kilepes();
extern void feliratokMentese(bool kellMentes);
extern QLabel *imgSzerkesztes;
extern QLabel *imgMozgatas;
extern int pauseMod;
extern int feliratPause;
extern QList<Felirat*> feliratok;
extern int N_feliratok;
void qStToFile(QString fajlNev, QString st, bool withBOOM);
QString fileToQString(QString fajlNev);
extern double idoStToDouble(QString st);
extern QString idoDoubleToSt(double d);
extern QString iniFileSt;
extern void setParameter(QString *forras, QString parNev, QString ertek, bool torlesHaUres = false);
extern int hangEro, szinHo;
extern double vilagossag, elesseg, telitettseg, gamma, hangKesleltetes;
extern void alapMod();
extern QString exePath, videoFile;
extern QStringList argL;
extern void sugoBeallitas();
extern QString cedictDict;
extern void bkmClearCommands();
extern double ittJarokPos;
extern QString bkmFileName;
extern void setIttJarokTimeInIni(double t);
extern QString bkmFileSt;
extern QString bkmFajlStOriginal;
extern void qStToFile(QString fajlNev, QString st, bool withBOM = false);
extern void delIttJarokTimeInIni();

void Sleeper::usleep(unsigned long usecs) {
    QThread::usleep(usecs);
}

void Sleeper::msleep(unsigned long msecs) {
    QThread::msleep(msecs);
}

void Sleeper::sleep(unsigned long secs) {
    QThread::sleep(secs);
}

SaMainWindow::SaMainWindow() :
    QMainWindow()
{
    eredetiGlobalPos = QPoint(-1, -1);
    mFr = NULL;
    foglalt = false;
    potiFoglalt = false;
    connect(this, SIGNAL(setEgerKurzorSignal(QCursor)), this, SLOT(setEgerKurzorSlot(QCursor)));
    connect(this, SIGNAL(nyelvSignal(QString)), this, SLOT(nyelvValtas(QString)));
    connect(this, SIGNAL(cedictSignal(QString)), this, SLOT(cedictValtas(QString)));
    timerS = new QTimer();
    timerS->setSingleShot(true);
    connect(timerS, SIGNAL(timeout()), this, SLOT(openFileS()));
        //Azért, hogy a fájlmegnyitás a feliratablakok kirajzolása után történjen
        //(azért, hogy a világosság, telítettség stb. fájlmegnyitáskor beállítódjon).
    cedictKerdezFoglalt = false;
    colorDialog = new QColorDialog;

    nyelvMenu = new QMenu();
    nyelvMenuGroup =new QActionGroup(this);
    nyelvMenu->setIcon(QIcon(exePath + "../resource/" + "lang/flag_" + nyelv + ".png"));
    QStringList langL = QDir(exePath + "../resource/" + "lang/", "lang_*.lng").entryList();
    QSignalMapper *sm = new QSignalMapper;
    for (int i=0; i<langL.size(); i++) {
        QString st = langL[i];
        st = st.replace("lang_", "");
        st = st.replace(".lng", "");
        QAction *nyelvAction = nyelvMenu->addAction(
                QIcon(exePath + "../resource/" + "lang/flag_" + st + ".png"), st);
        nyelvMenuGroup->addAction(nyelvAction);
        nyelvAction->setCheckable(true);
        nyelvAction->connect(nyelvAction, SIGNAL(triggered()), sm, SLOT(map()));
        sm->setMapping(nyelvAction, st);
        connect(sm, SIGNAL(mapped(QString)),
                this, SIGNAL(nyelvSignal(QString)));
    }

    cedictMenuGroup = new QActionGroup(this);
    cedictMenu = new QMenu();
    QSignalMapper *smCedict = new QSignalMapper;
    QString st = "Cedict";
    QAction *action = cedictMenu->addAction(QIcon(exePath + "../resource/point.png"), st);
    cedictMenuGroup->addAction(action);
    action->setCheckable(true);
    action->connect(action, SIGNAL(triggered()), smCedict, SLOT(map()));
    smCedict->setMapping(action, st);
    connect(smCedict, SIGNAL(mapped(QString)),
            this, SIGNAL(cedictSignal(QString)));

    st = "JMdict";
    action = cedictMenu->addAction(QIcon(exePath + "../resource/point.png"), st);
    cedictMenuGroup->addAction(action);
    action->setCheckable(true);
    action->connect(action, SIGNAL(triggered()), smCedict, SLOT(map()));
    smCedict->setMapping(action, st);
    connect(smCedict, SIGNAL(mapped(QString)),
            this, SIGNAL(cedictSignal(QString)));

    st = satr("semmi");
    action = cedictMenu->addAction(QIcon(exePath + "../resource/point.png"), st);
    cedictMenuGroup->addAction(action);
    action->setCheckable(true);
    action->connect(action, SIGNAL(triggered()), smCedict, SLOT(map()));
    smCedict->setMapping(action, st);
    connect(smCedict, SIGNAL(mapped(QString)),
            this, SIGNAL(cedictSignal(QString)));

    ocr = new OcrOpt;
    tesztPad = new TesztPad();
    tesztPad->setObjectName("tesztPad");
    tesztPad->setGeometry(1300, 400, 600, 600);
    tesztPad->hide();
    tesztPad->ui->pot1->setMinimum(0);
    tesztPad->ui->pot1->setMaximum(255);
    tesztPad->ui->pot2->setMinimum(0);
    tesztPad->ui->pot2->setMaximum(255);
    tesztPad->ui->pot3->setMinimum(0);
    tesztPad->ui->pot3->setMaximum(255);
    tesztPad->ui->pot4->setMinimum(0);
    tesztPad->ui->pot4->setMaximum(255);
    tesztPad->ui->pot5->setMinimum(0);
    tesztPad->ui->pot5->setMaximum(255);
    tesztPad->ui->pot6->setMinimum(0);
    tesztPad->ui->pot6->setMaximum(255);
    tesztPad->ui->pot7->setMinimum(0);
    tesztPad->ui->pot7->setMaximum(255);
    tesztPad->ui->pot8->setMinimum(0);
    tesztPad->ui->pot8->setMaximum(255);
    tesztPad->ui->pot9->setMinimum(0);
    tesztPad->ui->pot9->setMaximum(255);
    tesztPad->ui->pot10->setMinimum(0);
    tesztPad->ui->pot10->setMaximum(255);
    optFoglalt = false;
    this->utolsoKijelolesFokusz = NULL;
    this->utolsoSzerkesztesFokusz = NULL;
    this->kellFeliratMentes=false;
    this->setFocusPolicy(Qt::NoFocus);

    connect(tesztPad->ui->pushButton_3, SIGNAL(clicked()), this, SLOT(showFeliratszinDialog()));

    cedictAblakState = 0;

    hScrollBar = new QScrollBar(Qt::Horizontal, this);
    vScrollBar = new QScrollBar(Qt::Vertical, this);
    hScrollBar->setTracking(true);
    vScrollBar->setTracking(true);
    hScrollBar->setObjectName("hScrollBar");
    vScrollBar->setObjectName("vScrollBar");
    connect(hScrollBar, SIGNAL(valueChanged(int)), this, SLOT(scrollWindowRefresh(int)));
    connect(vScrollBar, SIGNAL(valueChanged(int)), this, SLOT(scrollWindowRefresh(int)));
}

void SaMainWindow::closeEvent(QCloseEvent *event) {
    kilepes();
    delete(colorDialog);
    delete(tesztPad);
    delete(ocr);
    delete(timerS);
    delete(hScrollBar);
    delete(vScrollBar);
    QMainWindow::closeEvent(event);
}

//void SaMainWindow::mouseDoubleClickEvent(QMouseEvent *e) {
//    qWarning("SaMainWindow::mouseDoubleClickEvent:");
//    if (imgSzerkesztes->isVisible()==false) {
//        foablakKeretTg();
//    } else {
//        mFr = QApplication::focusWidget();
//        ((SaTextEdit*) mFr)->mouseDoubleClickEvent(e);
//    }
//}

void SaMainWindow::keyPressEvent(QKeyEvent *e) {
//    qWarning("SaMainWindow::keyPressEvent:");
    billKezel(e);
}

void SaMainWindow::mousePressEvent(QMouseEvent *e) {
//    qWarning("SaMainWindow::mousePressEvent:");
    if (!foglalt) {
        foglalt = true;

        bkmClearCommands();

        //Egér mutatás:
        QCursor c = this->cursor();
        c.setShape(Qt::ArrowCursor);
        setCursor(c);

        mouseButtonPressed = e->button();
        QPoint p = e->globalPos();
        mFr = egerAlatt(p);
        //qWarning("mFr: " + mFr->objectName().toLatin1());
        if (mFr) {
            QString objNev = mFr->objectName();
            if (objNev == "W_00 (1/ch)" || objNev == "W_03 (1)" ||
                    objNev == "W_09 (OCR)") {
                this->utolsoKijelolesFokusz = (SaTextEdit*) mFr;
            }
            if (mouseButtonPressed == Qt::LeftButton) {
                if (mFr->objectName().startsWith("W_") ||
                        mFr->objectName().startsWith("SaVLC")) {
                    if (ablakMozgatas) {
                        eredetiGlobalPos = p;
                        eredetiRect = mFr->geometry();
                        if (!isFullScreen() || objNev != "W_11 (Player)") {
                            mFr->raise();
                        }
                        imgMozgatas->raise();
                    } else if (mFr->objectName() == "W_11 (Player)") {
                        if (imgSzerkesztes->isHidden() &&
                                !player->isFullScreen()) {
                            eredetiGlobalPos = e->globalPos();
                            int i = qMin(this->width(), this->height());
                            //i = qMax(i / 200, 30);  //teszteléshez
                            i = qMax(i / 200, 1);
                            ocr->setKeretPress(e->globalPos(), i);
                        }
                    } else {
                        mFr->raise();
                    }
//                    qWarning("h objectName: " + mFr->objectName().toLatin1());
                    if (mFr->objectName() != "W_10 (Poti)" && mFr->objectName() != "W_11 (Player)") {
                        if (((SaTextEdit*) mFr)->idx <= 4) {
                            this->utolsoKijelolesFokusz = (SaTextEdit*) mFr;
                        }
                        ((SaTextEdit*) mFr)->mousePressEvent(e);
                    }
//                } else if (mFr->objectName().startsWith("SaVLC")) {
//                    qWarning("h SaVLC:");
//                    QMainWindow::mousePressEvent(e);
//                    ((SaMainWindow*) mFr)->mousePressEvent(e);
                } else if (mFr->objectName() == "tesztPad") {
                    //qWarning("h TesztPad:");
                    ((TesztPad*) mFr)->mousePressEvent(e);
                } else if (mFr->objectName().startsWith("W_") &&
                        mFr->objectName() <= "W_04 (2)" &&
                        imgSzerkesztes->isVisible()) {
                    utolsoSzerkesztesFokusz = (SaTextEdit *) mFr;
                } else if (ablakMozgatas && mFr->objectName().endsWith("ScrollBar")) {
                    QPoint p = mFr->mapFrom(this, e->pos());
                    QMouseEvent eve(e->type(), p, e->button(), e->buttons(), e->modifiers());
                    app.sendEvent(mFr, &eve);
                }
            } else if (mouseButtonPressed == Qt::RightButton) {
                if (objNev != "W_11 (Player)") {
                    QRegExp re("^W_(\\d*) ");
                    if (re.indexIn(mFr->objectName()) > -1) {
                        int idx = re.cap(1).toInt();
                        teL[idx]->showContextMenu(e->globalPos());
                    } else if (mFr == this) {
                        this->showContextMenu(e->globalPos());
                    }
                }
            }
        }
        foglalt = false;
    }
}

void SaMainWindow::mouseReleaseEvent(QMouseEvent *e) {
//    qWarning("SaMainWindow::mouseReleaseEvent, e->button(): %d", e->button());
    this->setEgerKurzor(QCursor(Qt::ArrowCursor));
    megfogas = ures;
    eredetiGlobalPos = QPoint(-1, -1);
    if (!ablakMozgatas && imgSzerkesztes->isHidden() && mFr &&
            mFr->objectName() == "W_11 (Player)" && !player->isFullScreen() && mouseButtonPressed == Qt::LeftButton) {
        ocr->setKeretRelease(e->globalPos());
    } else if (ablakMozgatas) {
        if (mFr->objectName().endsWith("ScrollBar")) {
            QPoint p = mFr->mapFrom(this, e->pos());
            QMouseEvent eve(e->type(), p, e->button(), e->buttons(), e->modifiers());
            app.sendEvent(mFr, &eve);
        } else {
            scrollBarsRefresh();
        }
    } else {
            QMainWindow::mouseReleaseEvent(e);
    }
    mFr = NULL;
    mouseButtonPressed = Qt::NoButton;
}

void SaMainWindow::mouseMoveEvent(QMouseEvent *e) {
//    qWarning("SaMainWindow::mouseMoveEvent, e->button(): %d", e->button());   // e->button() is 0 !!!
    if (!saMainWindowMouseMoveEventFoglalt) {
        saMainWindowMouseMoveEventFoglalt = true;
        int minTav = 16;
        int minTav2 = 9999999;
        QPoint p = e->globalPos();
        if (mFr == NULL || eredetiGlobalPos == QPoint(-1, -1)) {
            mFr = egerAlatt(p);
        }
        //        qWarning("mouseMoveEvent: mFr: " + mFr->objectName().toLatin1());
        if (ablakMozgatas) {
            if (!mFr->objectName().endsWith("ScrollBar") &&
                    (!isFullScreen() || mFr->objectName() != "W_11 (Player)")) {
//                qWarning("SaMainWindow::mouseMoveEvent, ablakmozgatas, %d, : " + mFr->objectName().toLatin1(), isFullScreen());
                if (eredetiGlobalPos == QPoint(-1, -1)){  //nincs megfogás:
                    //qWarning("mFr_2: " + mFr->objectName().toLatin1());
                    //QPoint p2 = mFr->mapFrom(this, p);
                    QPoint p2 = mFr->mapFromGlobal(p);
                    QRect r = mFr->geometry();
                    r.moveTo(0,0);
                    //qWarning("p.y(): %d, p2.y(): %d, mFr: %d, " +
                    //        mFr->objectName().toLatin1(), p.y(), p2.y(), mFr);
                    QString eset = "";
                    int tav;
                    tav = (p2 - r.topLeft()).manhattanLength();
                    if ( tav < minTav2) {
                        minTav2 = tav;
                        eset = "bf";
                    }
                    tav = (p2 - r.bottomLeft()).manhattanLength();
                    if ( tav < minTav2) {
                        minTav2 = tav;
                        eset = "ba";
                    }
                    tav = (p2 - r.bottomRight()).manhattanLength();
                    if ( tav < minTav2) {
                        minTav2 = tav;
                        eset = "ja";
                    }
                    tav = (p2 - r.topRight()).manhattanLength();
                    if ( tav < minTav2) {
                        minTav2 = tav;
                        eset = "jf";
                    }
                    if (mFr->width() < minTav*3 && mFr->height() < minTav*3) {
//                        qWarning("semmi1:");   //semmi
                    } else if (mFr->width() < minTav*3) {
                        if (minTav2 <= minTav) {
                            ; //semmi
                        } else {
                            eset = "kk";
                        }
                    } else if (mFr->height() < minTav*3) {
                        if (minTav2 <= minTav) {
                            ; //semmi
                        } else {
                            eset = "kk";
                        }
                    } else if (minTav2 <= minTav) {
                        ;   //semmi
                    } else {
                        if (p2.y() - r.top() < minTav) {
                            eset = "kf";
                        } else if (p2.x() - r.left() < minTav) {
                            eset = "bk";
                        } else if (r.bottom() - p2.y() < minTav) {
                            eset = "ka";
                        } else if (r.right() - p2.x() < minTav) {
                            eset = "jk";
                        } else {
                            eset = "kk";
                        }
                    }
                    megfogas = ures;
                    if ( eset == "bf" ) {
                        this->setEgerKurzor(QCursor(Qt::SizeFDiagCursor));
                        megfogas = balFelso;
                    } else if ( eset == "ja" ) {
                        this->setEgerKurzor(QCursor(Qt::SizeFDiagCursor));
                        megfogas = jobbAlso;
                    } else if ( eset == "jf" ) {
                        this->setEgerKurzor(QCursor(Qt::SizeBDiagCursor));
                        megfogas = jobbFelso;
                    } else if ( eset == "ba" ) {
                        this->setEgerKurzor(QCursor(Qt::SizeBDiagCursor));
                        megfogas = balAlso;
                    } else if ( eset == "bk" ) {
                        this->setEgerKurzor(QCursor(Qt::SizeHorCursor));
                        megfogas = bal;
                    } else if ( eset == "jk" ) {
                        this->setEgerKurzor(QCursor(Qt::SizeHorCursor));
                        megfogas = jobb;
                    } else if ( eset == "kf" ) {
                        this->setEgerKurzor(QCursor(Qt::SizeVerCursor));
                        megfogas = felso;
                    } else if ( eset == "ka" ) {
                        this->setEgerKurzor(QCursor(Qt::SizeVerCursor));
                        megfogas = also;
                    } else {
                        megfogas = kozep;
                    }
                } else {    //van megfogás:
//                    qWarning("SaMainWindow::mouseMoveEvent, van megfogas, mFr: " + mFr->objectName().toLatin1());
                    QRect r = mFr->geometry();
                    if ( megfogas == balFelso ) {
                        r.setTopLeft(p - eredetiGlobalPos + eredetiRect.topLeft());
                    } else if ( megfogas == jobbFelso ) {
                        r.setTopRight(p - eredetiGlobalPos + eredetiRect.topRight());
                    } else if (megfogas == jobbAlso ) {
                        r.setBottomRight(p - eredetiGlobalPos + eredetiRect.bottomRight());
                    } else if ( megfogas == balAlso ) {
                        r.setBottomLeft(p - eredetiGlobalPos + eredetiRect.bottomLeft());
                    } else if ( megfogas == felso ) {
                        r.setTop(p.y() - eredetiGlobalPos.y() + eredetiRect.y());
                    } else if ( megfogas == also ) {
                        r.setBottom(p.y() - eredetiGlobalPos.y() + eredetiRect.bottom());
                    } else if ( megfogas == bal ) {
                        r.setLeft(p.x() - eredetiGlobalPos.x() + eredetiRect.x());
                    } else if ( megfogas == jobb ) {
                        r.setRight(p.x() - eredetiGlobalPos.x() + eredetiRect.right());
                    } else {
                        r.moveTo(p - eredetiGlobalPos + QPoint(eredetiRect.x(), eredetiRect.y()));
                    }
                    if (r.width() < 20) {
                        r.setWidth(21);
                    }
                    if (r.height() < 20) {
                        r.setHeight(21);
                    }
                    mFr->setGeometry(r);
                }
            } else {    //van ablakmozgatás és ScrollBar:
                this->setEgerKurzor(QCursor(Qt::ArrowCursor));
                QPoint p = mFr->mapFrom(this, e->pos());
                QMouseEvent eve(e->type(), p, e->button(), e->buttons(), e->modifiers());
                app.sendEvent(mFr, &eve);
            }
            scrollBarsRefresh();
        } else if (imgSzerkesztes->isVisible()) {
            ((SaTextEdit*) mFr)->mouseMoveEvent(e);
        } else if (mFr->objectName() == "W_11 (Player)" && !player->isFullScreen() && mouseButtonPressed == Qt::LeftButton) {
            if (pauseMod == 0) {
                player->tgPlayPause();
            }
            ocr->setKeretMove(e->globalPos());
        }
        saMainWindowMouseMoveEventFoglalt = false;
    }
}

void SaMainWindow::wheelEvent(QWheelEvent *e) {
    QPoint p = e->globalPos();
    mFr = egerAlatt(p);
    if (mFr && e->modifiers() == Qt::ControlModifier) {
        if (mFr->objectName().startsWith("W_")) {
            QFont font = mFr->font();
            if (e->delta() > 0) {
                font.setPointSize(font.pointSize() + 1);
            } else if (e->delta() < 0) {
                font.setPointSize(font.pointSize() -1);
            }
            mFr->setFont(font);
        }
    }
}

QWidget *SaMainWindow::egerAlatt(QPoint p) {
    QWidget *wg = app.widgetAt(p);
    QWidget *er = this;
    if (wg) {
        QString st = wg->objectName();
        if (st.startsWith("W_") || st.startsWith("SaVLC") || st == "tesztPad" || st.endsWith("ScrollBar")) {
            er = wg;
        } else if (wg->parentWidget()){
            wg = wg->parentWidget();
            st = wg->objectName();
            if (st.startsWith("W_") || st.startsWith("SaVLC") || st == "tesztPad") {
                er = wg;
            }
        }
    }
    return er;
}

void SaMainWindow::changePoti(int pos) {
    if (!potiFoglalt) {
        tA = -1;
        tB = -1;
        changePos(pos/100.0*videoHossz);
    }
}

void SaMainWindow::changePos(double pos) {
    potiFoglalt = true;
    poti->setSliderPosition(pos/videoHossz*100);
    player->setPos(pos);
    potiFoglalt = false;
    if (imgSzerkesztes->isHidden()) {
        setFocus();
    }
}

void SaMainWindow::szerkesztesSlot(int idx) {
//    qWarning("SaMainWindow::szerkesztesSlot, idx: %d", idx);
    if (idx == 3) {
        QStringList stL = teL[idx]->toPlainText().split("\n");
        for (int i=0; i<stL.size(); i++) {
            teL[i]->setText2(stL[i]);
        }
    } else if (!teL[3]->hasFocus()) {
        teL[3]->setText2(teL[0]->toPlainText());
        if (teL[1]->toPlainText() + teL[2]->toPlainText() != "") {
            teL[3]->setText2(teL[3]->toPlainText() + "\n" + teL[1]->toPlainText());
        }
        if (teL[2]->toPlainText() != "") {
            teL[3]->setText2(teL[3]->toPlainText() + "\n" + teL[2]->toPlainText());
        }
    }
}

void SaMainWindow::feliratMentMent() {
    //qWarning("feliratMentMent");
    if (kellFeliratMentes) {
        for (int i=0; i<4; i++) {
            feliratok[teL[3]->feliratIdx]->L[i][feliratok[teL[i]->feliratIdx]->iAkt] =
                    teL[i]->toPlainText();
            feliratok[teL[3]->feliratIdx]->text[i] =
                    teL[i]->toPlainText();
        }
        subtitleFileUpdate(teL[3]->feliratIdx);
    }
    if (N_feliratok < 2) {
        teL[4]->setText2("");
    } else if (kellFeliratMentes) {
        feliratok[teL[4]->feliratIdx]->L[3][feliratok[teL[4]->feliratIdx]->iAkt] =
                teL[4]->toPlainText();
        feliratok[teL[4]->feliratIdx]->text[3] =
                teL[4]->toPlainText();

        //fájl kiírás:
        QString st = "";
        for (int i=0; i<feliratok[teL[4]->feliratIdx]->L[3].size(); i++) {
            if (i>0) {
                st = st + "\n";
            }
            st = st + QString::number(i+1) + "\n";
            st = st + idoDoubleToSt(feliratok[teL[4]->feliratIdx]->t1[i]);
            st = st + " --> ";
            st = st + idoDoubleToSt(feliratok[teL[4]->feliratIdx]->t2[i]) + "\n";
            st = st + feliratok[teL[4]->feliratIdx]->L[3][i] + "\n";
        }
        qStToFile(feliratok[teL[4]->feliratIdx]->abszolutFajlPath, st, true);
    }
    for (int i=0; i<5; i++) {
        teL[i]->setReadOnly(true);
    }
    imgSzerkesztes->hide();
    setKeretStyle(semmi);
    for (int i=0; i<=4; i++) {
        teL[i]->setBetuSzin(teL[i]->betuSzin);
        teL[i]->setHatterSzin(teL[i]->hatterSzin);
    }
    if (teL[0]->isVisible()) {  //azért, hogy a Cedict ablak ne tűnjön el felíratszerkesztés után
        QTextCursor cur = teL[0]->textCursor();
        cur.setPosition(cursorStartEndCh.x(), QTextCursor::MoveAnchor);
        cur.setPosition(cursorStartEndCh.y(), QTextCursor::KeepAnchor);
        teL[0]->setTextCursor(cur);
        mySelectionChanged(this->teL[0]);
    }
    player->timer->start(100);
}

void SaMainWindow::feliratMentKilep() {
//    qWarning("feliratMentKilep");
    for (int i=0; i<5; i++) {
        teL[i]->setReadOnly(true);
    }
    imgSzerkesztes->hide();
    setKeretStyle(semmi);
    for (int i=0; i<=4; i++) {
        teL[i]->setBetuSzin(teL[i]->betuSzin);
        teL[i]->setHatterSzin(teL[i]->hatterSzin);
    }
    if (teL[0]->isVisible()) {  //azért, hogy a Cedict ablak ne tűnjön el felíratszerkesztés után
        QTextCursor cur = teL[0]->textCursor();
        cur.setPosition(cursorStartEndCh.x(), QTextCursor::MoveAnchor);
        cur.setPosition(cursorStartEndCh.y(), QTextCursor::KeepAnchor);
        teL[0]->setTextCursor(cur);
        mySelectionChanged(this->teL[0]);
    }
    player->timer->start(100);
}

void SaMainWindow::cedictKerdez(QString st) {
    const QString SZEPA6 = QString::fromUtf8("÷6");    //Új FvChToEn formátum tagolása
    if (st != "") {
        if (!cedictKerdezFoglalt) {
            cedictKerdezFoglalt = true;

            //az aktuális betűméretet be kell állítani:
            QStringList cl = cedictStilusSt.split("|");
            cl[7]=QString::number(teL[7]->font().pointSize());
            cedictStilusSt=cl.join("|");

            teL[7]->setStilus(cedictStilusSt);  //esetleges dulpa klikk  miatt
            wchar_t *valasz = new wchar_t[32000];
            LPWSTR pipeNev2 = (LPTSTR) pipeNev.utf16();
//            LPWSTR kerdez = (LPTSTR)("FvChToEnNyers|" + st).utf16();
            LPWSTR kerdez = (LPTSTR)(cedictSzotar + st).utf16();
            pipeKerdez(pipeNev2, kerdez, valasz);
            QString st2 = QString::fromUtf16((const ushort *)valasz);
//            qWarning("SaMainWindow::cedictKerdez, st: " + st.toLatin1() +
//                    ", len: " + QString::number(wcslen(valasz)).toLatin1() + ", st2:\n" +
//                    st2.toLatin1());
            if (st2.right(1) == "/") {
                st2 = st2.left(st2.length()-1);
            }

            const double ARANY = 0.25;
            QColor col, col1, col2;
            col1 = teL[7]->betuSzin;
            col2 = teL[7]->hatterSzin;
            col.setRed(col2.red() + (col1.red() - col2.red()) * ARANY);
            col.setGreen(col2.green() + (col1.green() - col2.green()) * ARANY);
            col.setBlue(col2.blue() + (col1.blue() - col2.blue()) * ARANY);
            QString colSt = "rgb(" +
                    QString::number(col.red()) + ", " +
                    QString::number(col.green()) + ", " +
                    QString::number(col.blue()) + ")";
            QString col1St = "rgb(" +
                    QString::number(col1.red()) + ", " +
                    QString::number(col1.green()) + ", " +
                    QString::number(col1.blue()) + ")";
            QString col2St = "rgb(" +
                    QString::number(col2.red()) + ", " +
                    QString::number(col2.green()) + ", " +
                    QString::number(col2.blue()) + ")";

            QStringList valaszSorL = st2.split("\n");
            QString v2St = "";
            QString ujSor = "<br>";
            QString irj = QString::fromUtf8(" _,.?!\"/+-&@()%$#:;{}<>='*、，。？！；：》《（）");
            for (int i = 0; i < valaszSorL.size(); i++) {
                bool tovabb = false;
                for (int j=0; j<irj.length(); j++) {
                    if (valaszSorL[i] == irj.mid(j, 1) + SZEPA6 + SZEPA6) {
                        tovabb = true;
                        break;
                    }
                }
                if (tovabb) {
                    continue;
                }

                if (i == valaszSorL.size()-1) {
                    ujSor = "";
                }
                QStringList v2StL = valaszSorL[i].split(SZEPA6);
                if (v2StL.size() != 3) {
                    v2St = v2St + "\n" + valaszSorL[i];
                } else {
                    QString st = v2StL[2];
                    st = st.trimmed();
                    if (st.left(1) == "/") {
                        st = st.right(st.length()-1);
                    }
                    if (st.right(1) == "/") {
                        st = st.left(st.length()-1);
                    }
                    st = st.replace("/", "; ").trimmed();
                    v2St = v2St +
                            "<span style=\""
                            "color:" + col2St + ";" +
                            "background-color:" + col1St + ";" +
                            + "\">" + v2StL[0] + "</span>"

                            "<span style=\""
                            "color:" + col1St + ";" +
                            "background-color:" + col2St + ";" +
                            + "\">" + " - [" + "</span>"

                            "<span style=\""
                            "color:" + col1St + ";" +
                            "background-color:" + colSt + ";" +
                            + "\">" + v2StL[1] + "</span>"

                            "<span style=\""
                            "color:" + col1St + ";" +
                            "background-color:" + col2St + ";" +
                            + "\">" + "]: " + st + ujSor + "</span>";
                }
            }

            teL[7]->setText2(v2St);
            teL[7]->setLathato(true);
            cedictKerdezFoglalt = false;
        }
    }
}

void SaMainWindow::mySelectionChanged(SaTextEdit *ablak) {
    QString st = ablak->selBeallitas();
    utolsoKijelolesFokusz = ablak;
    if ( cedictDict != "semmi" &&
            (ablak->objectName() == "W_00 (1/ch)" ||
            ablak->objectName() == "W_03 (1)" ||
            ablak->objectName() == "W_07 (Cedict)" ||
            ablak->objectName() =="W_09 (OCR)") ) {
        if (st == "" && cedictAblakState > 0) {
            st = ablak->toPlainText();
            if (cedictAblakState == 2) {
                utolsoKijelolesFokusz = teL[7];
                teL[7]->setFocus();
                cedictAblakState = 1;
            }
        }
        if (ablak->idx != 7) {
            cedictKerdez(st);
        }
    }
}

void SaMainWindow::setKeretStyle(KeretStyle kt) {
    switch (kt) {
        case semmi:
            for(int i=0; i<teL.size(); i++) {
                QWidget *keret = teL[i]->keret;
                keret->hide();
                teL[i]->saSetStyleSheet("border", "0px");
                teL[i]->setHatterSzin(teL[i]->palette().color(QPalette::Base));
            }
//            player->keret->setGeometry(0, 0, player->width(), player->height());
            player->keret->move(0, 0);  //mert setGeometry segfault-ol
            player->keret->resize(player->width(), player->height());
            break;
        case szerkeszt:
            for(int i=0; i<5;i++) {
                QString st = teL[i]->styleSheet();
                st = st.replace(QRegExp("border: \\d*px"), "border: 3px");
                st = st.replace(QRegExp("border: 3px solid [^;]*;"), "border: 3px solid orange;");
                teL[i]->setStyleSheet(st);
                QWidget *keret = teL[i]->keret;
                st = keret->styleSheet();
                st = st.replace(QRegExp("border: \\d*px"), "border: 3px");
                st = st.replace(QRegExp("border: 3px solid [^;]*;"), "border: 3px solid black;");
                keret->setStyleSheet(st);
                keret->setGeometry(3, 3, teL[i]->width()-6, teL[i]->height()-6);
                keret->show();
                keret->lower(); //e nélkül klikkeléskor SaTextEdit::mousePressEvent() helyett a
                                //  SaMainWindow::mousePressEvent() hívódik meg, ami nem teszi lehetővé
                                //  a dupla klikkes szókijelölést
            }
            break;
        case mozgat:
            for(int i=0; i < teL.size(); i++) {
                QString st = teL[i]->styleSheet();
                st = st.replace(QRegExp("border: \\d*px"), "border: 4px");
                st = st.replace(QRegExp("border: 4px solid [^;]*;"), "border: 4px solid blue;");
                teL[i]->setStyleSheet(st);
                QWidget *keret = teL[i]->keret;
                st = keret->styleSheet();
                st = st.replace(QRegExp("border: \\d*px"), "border: 4px");
                st = st.replace(QRegExp("border: 4px solid [^;]*;"), "border: 4px solid red;");
                keret->setStyleSheet(st);
                keret->setGeometry(4, 4, teL[i]->width()-8, teL[i]->height()-8);
                keret->show();
            }
            if (!isFullScreen()) {
                player->keret->setGeometry(80, 8, player->width()-16, player->height()-16);
            }
            break;
        default:
            break;
    }
}

void SaMainWindow::setEgerKurzor(QCursor cur) {
    emit this->setEgerKurzorSignal(cur);
}

void SaMainWindow::setEgerKurzorSlot(QCursor cur) {
    this->setCursor(cur);
}

void SaMainWindow::showContextMenu(const QPoint &pt) {
    QMenu *menu = new QMenu;
    menu->addSeparator();
    QAction *hatterSzinAction = new QAction(satr("Háttérszín"), this);
    hatterSzinAction->connect(hatterSzinAction, SIGNAL(triggered()), this, SLOT(showHatterszinDialog()));
    menu->addAction(hatterSzinAction);
//    QAction *feliratSzinAction = new QAction(satr("Video felirat szín"), this);
//    feliratSzinAction->connect(feliratSzinAction, SIGNAL(triggered()), this,
//            SLOT(showFeliratszinDialog()));
//    menu->addAction(feliratSzinAction);
    QAction *foablakAction = new QAction(satr("Főablak keret ki/be"), this);
    foablakAction->connect(foablakAction, SIGNAL(triggered()), this, SLOT(foablakKeretTg()));
    menu->addAction(foablakAction);
    menu->addMenu(nyelvMenu);
    menu->addMenu(cedictMenu);
    menu->exec(pt);
    delete menu;
}

void SaMainWindow::showHatterszinDialog() {
    colorDialog->setCurrentColor(this->palette().color(QPalette::Window));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setHatterSzin(QColor)));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setFeliratSzin(QColor)));
    colorDialog->connect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setHatterSzin(QColor)));
    this->releaseMouse();
    colorDialog->show();
}

void SaMainWindow::showFeliratszinDialog() {
    colorDialog->setCurrentColor(this->ocr->videoFeliratSzin);
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setHatterSzin(QColor)));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setFeliratSzin(QColor)));
    colorDialog->connect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setFeliratSzin(QColor)));
    this->releaseMouse();
    colorDialog->show();
}

void SaMainWindow::setHatterSzin(QColor col) {
    //qWarning("SaMainWindow::setHatterSzin:");
    this->hide();
    //Ez a SaMainWindow háttérszíne:
    QPalette pal = this->palette();
    pal.setColor(QPalette::Base, col);
    this->setPalette(pal);
    //Ez a StyleSheet beállítás:
    QString ssst = "SaMainWindow {border: 0px solid blue; background:rgba(" +
            QString::number(col.red()) + ", " +
            QString::number(col.green()) + ", " +
            QString::number(col.blue()) + ", 100%)}";
            //SaTextEdit azért kell, hogy a context menü szürke maradjon
    this->setStyleSheet(ssst);
    this->show();
    alapMod();
}

void SaMainWindow::setFeliratSzin(QColor col) {
    //qWarning("SaMainWindow::setFeliratSzin:");
    this->ocr->videoFeliratSzin = col;
    tesztPad->setColor(col);
    bool b = tesztPad->isVisible();
    alapMod();
    tesztPad->setVisible(b);
}

void SaMainWindow::foablakKeretTg() {
    if (!ablakMozgatas) {
        //chFrame:
        qWarning("SaMainWindow::foablakKeretTg()");
        this->hide();
        int saX = this->x();
        int saY = this->y();
        int saW1 = this->frameGeometry().width();
        int saH1 = this->frameGeometry().height();
        if ( this->windowFlags() & (Qt::FramelessWindowHint)) {
            //qWarning("Frameless -> Window");
            //this->setWindowFlags(Qt::Window | Qt::WindowStaysOnTopHint);
            this->setWindowFlags(Qt::Window);
        } else {
            //qWarning("Window -> Frameless");
            //this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
            this->setWindowFlags(Qt::FramelessWindowHint);
        }
        this->move(saX, saY);
        int saW2 = this->frameGeometry().width();
        int saH2 = this->frameGeometry().height();
        int saW3 = this->width();
        int saH3 = this->height();
        this->resize(saW3-(saW2-saW1), saH3-(saH2-saH1));
        this->show();
    }
}

void SaMainWindow::nyelvValtas(QString lng) {
        if (lng != nyelv) {
            nyelv = lng;
            nyelvBeallitas();
            //qWarning("SaMainWindow::nyelvValtas (slot): " + lng.toLatin1());
        }
}

void SaMainWindow::cedictValtas(QString cedictDict0) {
    cedictDict = cedictDict0;
    if (cedictDict == "Cedict") {
        cedictSzotar = "FvChToEnNyers|";
    } else if (cedictDict == "JMdict") {
        cedictSzotar = "FvJpToEnNyers|";
    }
    cedictMenuSetChecked(cedictDict0);
    mySelectionChanged(utolsoKijelolesFokusz);
}

void SaMainWindow::cedictMenuSetChecked(QString cedictDict0) {
    for (int i=0; i<cedictMenu->actions().size(); i++) {
        if ( satr(cedictMenu->actions()[i]->text().toUtf8()) == satr(cedictDict0.toUtf8()) ) {
            cedictMenu->actions()[i]->setChecked(true);
            break;
        }
    }
}

void SaMainWindow::nyelvBeallitas() {
    nyelvSt = fileToQString(exePath + "../resource/" + "lang/lang_" + nyelv + ".lng");
    nyelvMenu->setIcon(QIcon(exePath + "../resource/" + "lang/flag_" + nyelv + ".png"));
    nyelvMenu->setTitle(satr("Nyelv"));
    cedictMenu->actions()[2]->setText(satr("semmi"));
    tesztPad->ui->checkBox->setText(satr("Mentés"));
    tesztPad->ui->checkBox->setToolTip(satr("Mentés SaVLC.ocr-be (*.ini helyett)"));
    tesztPad->ui->pushButton->setText(satr("Felismer"));
    tesztPad->ui->pushButton_3->setToolTip(satr("Video felirat szín"));
    cedictMenu->setTitle(satr("Cedict szótár"));
    sugoBeallitas();
    for (int i=0; i<nyelvMenu->actions().size(); i++) {
        if ( satr(nyelvMenu->actions()[i]->text().toUtf8()) == satr(nyelv.toUtf8()) ) {
            nyelvMenu->actions()[i]->setChecked(true);
            break;
        }
    }
}

QString SaMainWindow::satr(QByteArray ba) {
    QString st = QString::fromUtf8(ba);
    QString st2;
    QString k1 = QString::fromUtf8("<eredeti>") + st +
            QString::fromUtf8("</eredeti><fordítás>");
    int p1 = nyelvSt.indexOf(k1);
    if (p1 > -1) {
        p1 += k1.length();
        int p2 = nyelvSt.indexOf(QString::fromUtf8("</fordítás>"), p1);
        if (p2 > -1) {
            st2 = nyelvSt.mid(p1, p2-p1);
        } else {
            st2 = st;
        }
    } else {
        st2 = st;
    }
    return st2;
}

void SaMainWindow::optimalizal(int) {
    //qWarning("SaMainWindow::optimalizal:");
    if (!optFoglalt) {
        optFoglalt = true;
        if (this->ocr->workerThread->isRunning()) {
            this->ocr->workerThread->m_abort = true;
            if (!ocr->workerThread->wait(100)) {
                qWarning("Thread deadlock detected, bad things may happen !!!");
                this->ocr->workerThread->terminate();
                this->ocr->workerThread->wait();
            }
        }
        this->ocr->workerThread->run();
        optFoglalt = false;
    }
}

void SaMainWindow::felismer() {
    ocr->felismer();
}

void SaMainWindow::felismer2() {
    ocr->im1 = QImage(exePath + "../resource/" + "tmp.png");
    ocr->optimalizal();
    ocr->felismer();
}

int SaMainWindow::keresIdx(QList<double> list, double mIdo) {
    //azt a legnagyobb indexet adja vissza, amelynél list[idx] <= mIdo,
    //https://en.wikipedia.org/wiki/Binary_search_algorithm, finding the rightmost element
    int L = 0;
    int R = list.size();
    while (L < R) {
        int m = qFloor((L + R) / 2);
        if (list[m] > mIdo) {
            R = m;
        } else {
            L = m + 1;
        }
    }
    return R - 1;
}

void SaMainWindow::openFileS() {
    this->player->openFile(videoFile);
    if (ittJarokPos >= videoHossz) {
        ittJarokPos = -1.0;
        qWarning("Hello2");
    }
    if (tA >= videoHossz || tB >= videoHossz) {
        tA = -1.0;
        tB = -1.0;
    }

}

void SaMainWindow::saveIttJarokPos() {
    if (ittJarokPos >= 0.0) {
        if (bkmFileName == "") {
            setIttJarokTimeInIni(ittJarokPos);
        } else {
            setParameter(&bkmFileSt, "IttJarok", idoDoubleToSt(ittJarokPos));
            if (bkmFajlStOriginal != bkmFileSt) {
                qStToFile(bkmFileName, bkmFileSt);
                bkmFajlStOriginal = bkmFileSt;
            }
        }
    } else {
        if (bkmFileName == "") {
            delIttJarokTimeInIni();
        } else {
            setParameter(&bkmFileSt, "IttJarok", "");
            if (bkmFajlStOriginal != bkmFileSt) {
                qStToFile(bkmFileName, bkmFileSt);
                bkmFajlStOriginal = bkmFileSt;
            }
        }
    }
}

void SaMainWindow::kurzorBeallitas() {
    if (this->utolsoKijelolesFokusz != NULL && this->utolsoKijelolesFokusz->isVisible()) {
        QTextCursor cur = this->utolsoKijelolesFokusz->textCursor();
        if (cur.selectionStart() == 0 && cur.selectionEnd() == 0) {
            int pos;
            if (imgSzerkesztes->isVisible()) {
                pos = this->utolsoKijelolesFokusz->toPlainText().length()/2;
            } else {
                pos = 0;
                QMouseEvent eve(QEvent::MouseMove, QPoint(10,10), Qt::NoButton, Qt::NoButton,
                        Qt::NoModifier);
                this->utolsoKijelolesFokusz->mousePressEvent(&eve);
            }
            cur.setPosition(pos, QTextCursor::MoveAnchor);
            cur.setPosition(pos, QTextCursor::KeepAnchor);
            this->utolsoKijelolesFokusz->setTextCursor(cur);
        }
        this->utolsoKijelolesFokusz->raise();
        this->utolsoKijelolesFokusz->setFocus();
    }
}

void SaMainWindow::scrollBarsRefresh() {
//    qWarning("scrollBarsRefresh");
    if (!scrollBarsRefreshMutex.tryLock()) {
        return;
    }
    xyMMFv();
    if (!isFullScreen() && (xyMM.xMin < 0 || xyMM.xMax > width()-1)) {
        hScrollBar->setGeometry(0, height() - sbWidth, width() - sbWidth, sbWidth);
        hScrollBar->setMinimum(xyMM.xMin);
        hScrollBar->setMaximum(xyMM.xMax - width());
        hScrollBar->setPageStep(width());
        hScrollBar->setValue(0);
        hScrollBar->raise();
        hScrollBar->show();
//        qWarning("scrollBarsRefresh h:\n w: %d, xMin: %d, xMax: %d, value: %d, minimum: %d, maximum: %d, pageStep: %d",
//                width(), xyMM.xMin, xyMM.xMax, hScrollBar->value(),
//                 hScrollBar->minimum(), hScrollBar->maximum(), hScrollBar->pageStep());
    } else {
        hScrollBar->hide();
    }

    if (!isFullScreen() && (xyMM.yMin < 0 || xyMM.yMax > height()-1)) {
        vScrollBar->setGeometry(width() - sbWidth, 0, sbWidth, height() - sbWidth);
        vScrollBar->setMinimum(xyMM.yMin);
        vScrollBar->setMaximum(xyMM.yMax - height());
        vScrollBar->setPageStep(height());
        vScrollBar->setValue(0);
        vScrollBar->raise();
        vScrollBar->show();
//        qWarning("scrollBarsRefresh h:\n w: %d, xMin: %d, xMax: %d, value: %d, minimum: %d, maximum: %d, pageStep: %d",
//                width(), xyMM.xMin, xyMM.xMax, hScrollBar->value(),
//                 hScrollBar->minimum(), hScrollBar->maximum(), hScrollBar->pageStep());
    } else {
        vScrollBar->hide();
    }

    scrollBarsRefreshMutex.unlock();
}

void SaMainWindow::scrollWindowRefresh(int) {
    if (0 != hScrollBar->value() || 0 != vScrollBar->value()) {
        xyMMFv();
        for (int i=0; i<this->children().size(); i++) {
            QWidget *child = (QWidget*) (this->children()[i]);
            if (child->objectName().startsWith("W_")) {
                child->move(child->x() - hScrollBar->value(), child->y() - vScrollBar->value());
            }
        }
        scrollBarsRefresh();
    }
}

void SaMainWindow::resizeEvent(QResizeEvent *e) {
    if (this->utolsoKijelolesFokusz != NULL) {
        scrollBarsRefresh();
    }
}

void SaMainWindow::xyMMFv() {
    xyMM.xMin = 0;
    xyMM.xMax = this->width()-1;
    xyMM.yMin = 0;
    xyMM.yMax = this->height()-1;
    for (int i=0; i<this->children().size(); i++) {
        QWidget *child = (QWidget*) (this->children()[i]);
        if (child->objectName().startsWith("W_") && child->isVisible()) {
            if (child->x() + child->width()*0.5 < xyMM.xMin) {
                xyMM.xMin = child->x() + child->width()*0.5;
            }
            if (child->y() + child->height()*0.5 < xyMM.yMin) {
                xyMM.yMin = child->y() + child->height()*0.5;
            }
            if (child->x() + child->width()*0.5 > xyMM.xMax) {
                xyMM.xMax = child->x() + child->width()*0.5;
            }
            if (child->y() + child->height()*0.5 > xyMM.yMax) {
                xyMM.yMax = child->y() + child->height()*0.5;
            }
        }
    }
}

void SaMainWindow::subtitleFileUpdate (int feliratIdx) {
    //fájl kiírás:
    QString st = "";
    for (int i=0; i<feliratok[feliratIdx]->L[3].size(); i++) {
        if (i>0) {
            st = st + "\n";
        }
        st = st + QString::number(i+1) + "\n";
        st = st + idoDoubleToSt(feliratok[feliratIdx]->t1[i]);
        st = st + " --> ";
        st = st + idoDoubleToSt(feliratok[feliratIdx]->t2[i]) + "\n";
        st = st + feliratok[feliratIdx]->L[3][i] + "\n";
    }
    qStToFile(feliratok[feliratIdx]->abszolutFajlPath, st, true);
}

