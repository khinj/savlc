﻿#include "satextedit.h"
#include "samainwindow.h"
#include <QKeyEvent>
#include <QLabel>
#include <QAction>
#include <QMenu>

extern bool globalVisible;
extern void qtKey1();
extern bool ablakMozgatas;
extern void billKezel(QKeyEvent *);
extern QLabel *imgSzerkesztes;
extern QLabel *imgMozgatas;
extern double SaTextEdit5BCArany;
extern QString idoC, idoBGC1, idoBGC2;
extern void alapMod();
extern SaMainWindow *w;
extern long BILL_TgAlap;
extern bool kellRepaint;
extern bool isOCRmaradjonLathato;

SaTextEdit::SaTextEdit(QWidget *parent) :
    QTextEdit(parent)
{
    this->hide();
    connect(this, SIGNAL(setTextSignal(QString)), this, SLOT(setText(QString)));
    connect(this, SIGNAL(textChanged()), this, SLOT(szerkesztesSlot()));
    this->setAlignment(Qt::AlignCenter);
    this->setReadOnly(true);
    this->setCursorWidth(3);
    QString st = "SaTextEdit {";
    st += "border: 0px solid blue; ";
    st += "color: rgba(255, 255, 0); ";
        //SaTextEdit azért kell, hogy a context menü szürke maradjon
    st += "background: rgba(0, 0 ,0, 100%); ";
    st += "}";
    //qWarning("Hello1: st :" + st.toLatin1());
    this->setStyleSheet(st);
    this->saMouseEvent = NULL;
    this->setFocusPolicy(Qt::NoFocus);

    this->show();
    this->feliratIdx = -1;
    this->keret = new QWidget(this);
    this->keret->setStyleSheet("border: 3px solid yellow;");
    this->keret->setEnabled(false);
    this->keret->hide();
    this->setContextMenuPolicy(Qt::CustomContextMenu);
    this->colorDialog = new QColorDialog;
    this->fontDialog = new QFontDialog;
}

SaTextEdit::~SaTextEdit() {
    delete(colorDialog);
    if (saMouseEvent) {
        delete(saMouseEvent);
    }
}

void SaTextEdit::setText2(QString st0) {
    QString st = st0;
    //st = st.replace(QRegExp("<[^>]*>"), "");
//    if (this->idx == 0) {
//        qWarning("SaTextEdit::setText2, st0 = " + st0.toUtf8());
//    }
    if (this->toPlainText() != st) {
        emit setTextSignal(st);
    }
    //emit setTextSignal("SaTextEdit: HELLO, " + this->objectName());
}

QString SaTextEdit::getStilus() {
    QString st = this->font().family();
    st = st + "|" + QString::number(this->textColor().red());
    st = st + "|" + QString::number(this->textColor().green());
    st = st + "|" + QString::number(this->textColor().blue());
    QPalette pal = this->palette();
    st = st + "|" + QString::number(pal.color(QPalette::Base).red());
    st = st + "|" + QString::number(pal.color(QPalette::Base).green());
    st = st + "|" + QString::number(pal.color(QPalette::Base).blue());
    st = st + "|" + QString::number(this->font().pointSize());
    st = st + "|" + QString::number(this->lathato*1);
    st = st + "|" + QString::number(this->font().bold());
    return st;
}

void SaTextEdit::setStilus(QString st) {
//    qWarning("setStilus: st = " + st.toLatin1() + ", objectName: " + this->objectName().toLatin1());
    QStringList stL = st.split("|");
    QFont font = this->font();
    font.setFamily(stL[0]);
    font.setPointSize(stL[7].toInt());
    font.setBold(stL[9].toInt());
    this->setFont(font);
    QColor col(stL[1].toInt(), stL[2].toInt(), stL[3].toInt());
    this->setBetuSzin(col);

    //Háttérszín:
    col = QColor(stL[4].toInt(), stL[5].toInt(), stL[6].toInt());
    this->setHatterSzin(col);
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
    this->lathato = stL[8].contains("1");
    //qWarning("this->styleSheet(): " + this->styleSheet().toLatin1());
}

bool SaTextEdit::getLathato() {
    return this->lathato;
}

void SaTextEdit::setLathato(bool lathato0) {
    this->lathato = lathato0;
    if (idx <= 4 || idx ==7) {
        if (globalVisible && this->lathato) {
            this->setVisible(true);
            this->raise();
        } else {
            if (!this->isHidden()) {
                this->setVisible(false);
                if (w->utolsoKijelolesFokusz == this) {
                    w->utolsoKijelolesFokusz = NULL;
                }
                if (w->utolsoSzerkesztesFokusz == this) {
                    w->utolsoSzerkesztesFokusz = NULL;
                }
                this->lower();
                kellRepaint = true;
            }
        }
    } else {
        if (this->lathato) {
            this->setVisible(true);
            this->raise();
        } else {
            if (!this->isHidden()) {
                this->setVisible(false);
                if (w->utolsoKijelolesFokusz == this) {
                    w->utolsoKijelolesFokusz = NULL;
                }
                if (w->utolsoSzerkesztesFokusz == this) {
                    w->utolsoSzerkesztesFokusz = NULL;
                }
                this->lower();
                kellRepaint = true;
            }
        }
    }
    w->scrollBarsRefresh();
}

void SaTextEdit::keyPressEvent(QKeyEvent *e) {
//    qWarning("SaTextEdit::keyPressEvent: objectName" + this->objectName().toUtf8() + ", modifiers: 0x%08x, key: 0x%08x", (long)e->modifiers(), (long)e->key());
    if (e->modifiers() == Qt::NoModifier && e->key() == BILL_TgAlap) {
        billKezel(e);
    } else if (
            (e->modifiers() == Qt::ShiftModifier    && e->key() == Qt::Key_Shift) ||  //csak Shift
            (e->modifiers() == Qt::ControlModifier  && e->key() == Qt::Key_Control) ||  //csak Ctrl
            (e->modifiers() == Qt::AltModifier      && e->key() == Qt::Key_Alt) ||  //csak Alt
            (e->modifiers() == Qt::NoModifier       && e->key() == Qt::Key_CapsLock) ||  //csak CapsLock
            (e->modifiers() == Qt::KeypadModifier   && e->key() == Qt::Key_NumLock) ||  //csak NumLock
            (e->modifiers() == Qt::NoModifier       && e->key() == Qt::Key_Escape) ||  //csak Esc
            (e->modifiers() == Qt::NoModifier       && e->key() == Qt::Key_Meta) ||  //csak Window
            (e->modifiers() == Qt::ShiftModifier + Qt::ControlModifier
                    && e->key() == Qt::Key_Shift) ||  //csak Ctrl+Shift
            (e->modifiers() == Qt::AltModifier + Qt::ShiftModifier
                    && e->key() == Qt::Key_Shift) ||  //csak Alt+Shift
            (e->modifiers() == Qt::ControlModifier + Qt::AltModifier
                    && e->key() == Qt::Key_Control)) {  //csak Ctrl+Alt
        //semmi
    } else if (e->modifiers() == Qt::KeypadModifier ||                                  //Keypad + valami
               (e->modifiers() == Qt::ShiftModifier    && e->key() == Qt::Key_Tab + 1)) { //Shift + TAB
        w->keyPressEvent(e);
    } else if (e->matches(QKeySequence::Copy)) {
        QString st = "";
        if (this->selList.size() > 0) {
            for(int i=0; i<this->selList.size(); i++) {
                st = st + toPlainText().mid(
                        this->selList[i].x(),
                        this->selList[i].y() - selList[i].x());
            }
        } else {
            st = this->textCursor().selectedText();
        }
        QClipboard *clipboard = QApplication::clipboard();
        clipboard->setText(st);
    } else if (e->matches(QKeySequence::SelectAll)) {
        this->selTorol();
        QTextCursor cur = this->textCursor();
        cur.setPosition(0, QTextCursor::MoveAnchor);
        cur.setPosition(this->toPlainText().length(), QTextCursor::KeepAnchor);
        this->setTextCursor(cur);
        w->mySelectionChanged(this);
    } else if (!imgSzerkesztes->isHidden() ||
            this->objectName() == "W_09 (OCR)") {
        QTextEdit::keyPressEvent(e);
        QTextCursor cur = this->textCursor();
        int p1 = qMin(cur.position(), cur.anchor());
        int p2 = qMax(cur.position(), cur.anchor());
        this->selTorol();
        selList.append(QPoint(p1, p2));
        w->mySelectionChanged(this);
        this->setTextCursor(cur);
    } else if (this->objectName().startsWith("W_08") || this->objectName().startsWith("W_06")) {
        QTextEdit::keyPressEvent(e);
    } else {
        billKezel(e);
    }
}

void SaTextEdit::szerkesztesSlot() {
//    qWarning("SaTextEdit::szerkesztesSlot, idx: %d, hasFocus: %d", this->idx, this->hasFocus());
    if (imgSzerkesztes && !imgSzerkesztes->isHidden()) {
        emit(this->szerkesztesSignal(this->idx));
    } else if (((idx == 0 || idx == 3) && getLathato()) || idx == 9) {
        selTorol();
        w->cedictKerdez("");    //w->teL[7] eltüntetése
    }
}

void SaTextEdit::resizeEvent(QResizeEvent *e) {
    QTextEdit::resizeEvent(e);
    if (ablakMozgatas) {
        this->keret->resize(this->width()-8, this->height()-8);
    }
}

void SaTextEdit::showContextMenu(const QPoint &pt) {
    QMenu *menu = this->createStandardContextMenu();
    menu->addSeparator();

    if (this->idx == 7) {   //Cedict ablak kijelölt szövegére kell keresni
        QAction *cedictKeresAction = new QAction(w->satr("Cedict keres"), this);
        cedictKeresAction->connect(cedictKeresAction, SIGNAL(triggered()), this, SLOT(cedictKeresSlot()));
        menu->addAction(cedictKeresAction);
    }
    QAction *fontAction = new QAction(w->satr("Betűtípus"), this);
    fontAction->connect(fontAction, SIGNAL(triggered()), this, SLOT(showFontDialog()));
    menu->addAction(fontAction);
    QAction *betuSzinAction = new QAction(w->satr("Betűszín"), this);
    betuSzinAction->connect(betuSzinAction, SIGNAL(triggered()), this, SLOT(showBetuszinDialog()));
    menu->addAction(betuSzinAction);

    QAction *hatterSzinAction = new QAction(w->satr("Háttérszín"), this);
    hatterSzinAction->connect(hatterSzinAction, SIGNAL(triggered()), this, SLOT(showHatterszinDialog()));
    menu->addAction(hatterSzinAction);
//    QAction *feliratSzinAction = new QAction(w->satr("Video felirat szín"), this);
//    feliratSzinAction->connect(feliratSzinAction, SIGNAL(triggered()), this,
//            SLOT(showFeliratszinDialog()));
//    menu->addAction(feliratSzinAction);
    QAction *foablakAction = new QAction(w->satr("Főablak keret ki/be"), this);
    foablakAction->connect(foablakAction, SIGNAL(triggered()), w, SLOT(foablakKeretTg()));
    menu->addAction(foablakAction);
    menu->addMenu(w->nyelvMenu);
    menu->addMenu(w->cedictMenu);
    menu->exec(pt);
    delete menu;
}

void SaTextEdit::showBetuszinDialog() {
    if (this->idx == 7) {
        this->setStilus(w->cedictStilusSt);
    }
    colorDialog->setCurrentColor(this->textColor());
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setBetuSzinDialog(QColor)));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setHatterSzinDialog(QColor)));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            w, SLOT(setFeliratSzin(QColor)));
    colorDialog->connect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setBetuSzinDialog(QColor)));
    w->releaseMouse();
    colorDialog->show();
}

void SaTextEdit::showHatterszinDialog() {
    if (this->idx == 7) {
        this->setStilus(w->cedictStilusSt);
    }
    colorDialog->setCurrentColor(this->textBackgroundColor());
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setBetuSzinDialog(QColor)));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setHatterSzinDialog(QColor)));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            w, SLOT(setFeliratSzin(QColor)));
    colorDialog->connect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setHatterSzinDialog(QColor)));
    w->releaseMouse();
    colorDialog->show();
}

void SaTextEdit::showFeliratszinDialog() {
    colorDialog->setCurrentColor(w->ocr->videoFeliratSzin);
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setBetuSzin(QColor)));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setHatterSzin(QColor)));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            w, SLOT(setFeliratSzin(QColor)));
    colorDialog->connect(colorDialog, SIGNAL(colorSelected(QColor)),
            w, SLOT(setFeliratSzin(QColor)));
    w->releaseMouse();
    colorDialog->show();
}

void SaTextEdit::showFontDialog() {
    fontDialog->setCurrentFont(this->font());
    fontDialog->connect(fontDialog, SIGNAL(currentFontChanged(QFont)),
            this, SLOT(setSaFontDialog(QFont)));
    w->releaseMouse();
    fontDialog->show();
}

void SaTextEdit::setBetuSzin(QColor col) {
//    qWarning("setBetuSzin, objectName: " + this->objectName().toLatin1() + ", col.red: %d", col.toRgb().red());
    this->betuSzin = col;
    this->hide();
    this->setTextColor(col);
    this->saSetStyleSheet("color", "rgba(" +
            QString::number(col.red()) + ", " +
            QString::number(col.green()) + ", " +
            QString::number(col.blue()) + ", 100%)");
    //kijelölés mindig legyen feltűnő:
    QPalette p = this->palette();
    p.setColor(QPalette::HighlightedText, this->hatterSzin);
    p.setColor(QPalette::Highlight, this->betuSzin);
    this->setPalette(p);

    if (this->idx == 5) {   //Beállítások az 5. (idő) ablakhoz:
        QString st = this->getStilus();
        QStringList stL = st.split("|");
        idoC = stL[1] + "," + stL[2] + "," + stL[3] + ",100%";   //pl "55,55,51,100%"
        idoBGC1 = stL[4] + "," + stL[5] + "," + stL[6] + ",100%";   //pl "55,55,51,100%"
        int cR = stL[4].toInt() + (stL[1].toInt() - stL[4].toInt()) * SaTextEdit5BCArany;
        int cG = stL[5].toInt() + (stL[2].toInt() - stL[5].toInt()) * SaTextEdit5BCArany;
        int cB = stL[6].toInt() + (stL[3].toInt() - stL[6].toInt()) * SaTextEdit5BCArany;
        idoBGC2 = QString::number(cR) + "," + QString::number(cG) +
                "," + QString::number(cB) + ",100%";   //pl "55,55,51,100%"
    }

    this->setPlainText(this->toPlainText());
    this->show();
}

void SaTextEdit::setBetuSzinDialog(QColor col) {
    setBetuSzin(col);
    if (this->idx == 7) {
        w->cedictStilusSt = this->getStilus();
    }
}

void SaTextEdit::setHatterSzin(QColor col) {
    //qWarning("hatterszín:");
    this->hatterSzin = col;
    this->hide();
    //Ez a karakterek háttérszíne:
    this->setTextBackgroundColor(col);
    //Ez a SaTextEdit háttérszíne:
    QPalette pal = this->palette();
    pal.setColor(QPalette::Base, col);
    this->setPalette(pal);
    //Ez a StyleSheet beállítás:
    this->saSetStyleSheet("border", "0px solid blue");
    this->saSetStyleSheet("background", "rgba(" +
            QString::number(col.red()) + ", " +
            QString::number(col.green()) + ", " +
            QString::number(col.blue()) + ", 100%)");
    //kijelölés mindig legyen feltűnő:
    QPalette p = this->palette();
    p.setColor(QPalette::HighlightedText, this->hatterSzin);
    p.setColor(QPalette::Highlight, this->betuSzin);
    this->setPalette(p);

    if (this->idx == 5) {   //Beállítások az 5. (idő) ablakhoz:
        QString st = this->getStilus();
        QStringList stL = st.split("|");
        idoC = stL[1] + "," + stL[2] + "," + stL[3] + ",100%";   //pl "55,55,51,100%"
        idoBGC1 = stL[4] + "," + stL[5] + "," + stL[6] + ",100%";   //pl "55,55,51,100%"
        int cR = stL[4].toInt() + (stL[1].toInt() - stL[4].toInt()) * SaTextEdit5BCArany;
        int cG = stL[5].toInt() + (stL[2].toInt() - stL[5].toInt()) * SaTextEdit5BCArany;
        int cB = stL[6].toInt() + (stL[3].toInt() - stL[6].toInt()) * SaTextEdit5BCArany;
        idoBGC2 = QString::number(cR) + "," + QString::number(cG) +
                "," + QString::number(cB) + ",100%";   //pl "55,55,51,100%"
    }
    this->setPlainText(this->toPlainText());
    if (this->lathato) {
        this->show();
    }
}

void SaTextEdit::setHatterSzinDialog(QColor col) {
    setHatterSzin(col);
    if (this->idx == 7) {
        w->cedictStilusSt = this->getStilus();
    }
}

void SaTextEdit::setSaFont(QFont font) {
    //qWarning("SaTextEdit::setSaFont:");
    this->hide();
    this->setFont(font);
    this->setPlainText(this->toPlainText());
    this->show();
    alapMod();
}

void SaTextEdit::setSaFontDialog(QFont font) {
    setSaFont(font);
    if (this->idx == 7) {
        w->cedictStilusSt = this->getStilus();
    }
}

void SaTextEdit::mousePressEvent(QMouseEvent *e) {
    int idx = this->idx;
    if (idx == 0 || idx == 3 || idx == 9) {
        w->utolsoKijelolesFokusz = this;
    }
    if (ablakMozgatas) {
        //qWarning("SaTextEdit::mousePressEvent_1: objectName():" + this->objectName().toLatin1());
        w->mousePressEvent(e);
    } else if (e->button() == Qt::LeftButton) {
//        qWarning("SaTextEdit::mousePressEvent_2: objectName():" + this->objectName().toLatin1());
        if (this->objectName() == "W_09 (OCR)") {
            isOCRmaradjonLathato = true;
        }
        if (imgSzerkesztes->isVisible()) {
            this->setReadOnly(false);
            if (this->objectName().startsWith("W_") &&
                    this->objectName() <= "W_04 (2)") {
                w->utolsoSzerkesztesFokusz = this;
            }
        }
        int pos = (this->cursorForPosition(e->pos())).position();
        if (e->modifiers() == Qt::NoModifier ) {
            this->selTorol();
            this->selList.append(QPoint(pos, pos));
            QTextCursor cur = this->textCursor();
            cur.setPosition(pos, QTextCursor::MoveAnchor);
            this->setTextCursor(cur);
        } else if (e->modifiers() == Qt::ShiftModifier) {
            if (this->selList.size() == 0) {
                this->selList.append(QPoint(pos, pos));
            } else {
                while (this->selList.size() > 0 && pos < this->selList.last().x()) {
                    this->selList.removeLast();
                }
                if (pos < this->selList.last().x()) {
                    this->selList.last().setX(pos);
                } else {
                    this->selList.last().setY(pos);
                }
            }
        } else if (e->modifiers() == Qt::ControlModifier) {
            while (this->selList.size() > 0 && pos < this->selList.last().x()) {
                this->selList.removeLast();
            }
            this->selList.append(QPoint(pos, pos));
        }

        this->setFocus();
        w->utolsoKijelolesFokusz = this;
        if (this->objectName() != "W_07 (Cedict)") {
            w->cedictAblakState = 0;
            w->teL[7]->setLathato(false);
        }
        //QTextEdit::mousePressEvent(e);
    } else if (e->button() == Qt::RightButton) {
        if (this->objectName() == "W_09 (OCR)") {
            isOCRmaradjonLathato = true;
        }
        w->mousePressEvent(e);
    }
}

void SaTextEdit::mouseDoubleClickEvent(QMouseEvent *e) {
//    qWarning("SaTextEdit::mouseDoubleClickEvent: ");
    this->selTorol();
    QTextEdit::mouseDoubleClickEvent(e);
    w->mySelectionChanged(this);
}

void SaTextEdit::mouseReleaseEvent(QMouseEvent *e) {
//    qWarning("SaTextEdit::mouseReleaseEvent: ");
    QTextEdit::mouseReleaseEvent(e);
}

void SaTextEdit::mouseMoveEvent(QMouseEvent *e) {
    //qWarning("SaTextEdit::mouseMoveEvent_1: objectName():" + this->objectName().toLatin1());
    if (ablakMozgatas) {
        //qWarning("SaTextEdit::mouseMoveEvent_2: objectName():" + this->objectName().toLatin1());
        w->mouseMoveEvent(e);
    } else {

        int pos = (this->cursorForPosition(e->pos())).position();
//        qWarning("SaTextEdit::mouseMoveEvent_3: cur pos: %d, objectName(): " +
//                this->objectName().toLatin1(), pos);
        if (this->selList.size() == 0) {
            this->selList.append(QPoint(pos, pos));
        } else {
            while (this->selList.size() > 0 && pos < this->selList.last().x()) {
                this->selList.removeLast();
            }
            if (this->selList.size() == 0) {
                this->selList.append(QPoint(pos, pos));
            } else {
                this->selList.last().setY(pos);
            }
        }
        this->selBeallitas();

        isOCRmaradjonLathato = true;
        w->mySelectionChanged(this);
        this->setFocus();
        //szerkesztéskor az egérrel történő kijelölés változás nem emitál automatikusan
        //      selectionChanged -et
    }
}

void SaTextEdit::saSetStyleSheet(QString par, QString value) {
    QString st = this->styleSheet();
    st.replace(QRegExp(par + ":[^;]*;"), par + ": " + value + ";");
//    qWarning("st: " + st.toLatin1());
    this->setStyleSheet(st);
}

void SaTextEdit::selTorol() {
    this->selList.clear();
    QList<QTextEdit::ExtraSelection> extraSelections;
    this->setExtraSelections(extraSelections);
    if (!w->utolsoKijelolesFokusz) {
        w->utolsoKijelolesFokusz = this;
    }
    w->kurzorBeallitas();
}

QString SaTextEdit::selBeallitas() {
    const QString SZEPA1 = QString::fromUtf8("÷1");    //kínai szavakat választ el
    QString st = "";
    if (this->selList.size() > 0) {
        QList<QTextEdit::ExtraSelection> extraSelections;
        for (int i=0; i<this->selList.size(); i++) {
            QTextEdit::ExtraSelection selection;
            selection.cursor = this->textCursor();
            selection.format.setBackground(this->betuSzin);
            selection.format.setForeground(this->hatterSzin);
            selection.cursor.setPosition(this->selList[i].x(), QTextCursor::MoveAnchor);
            selection.cursor.setPosition(this->selList[i].y(), QTextCursor::KeepAnchor);
            extraSelections.append(selection);
        }
        this->setExtraSelections(extraSelections);
        QTextCursor cur = this->textCursor();
        cur.setPosition(this->selList.last().y(), QTextCursor::MoveAnchor);
        this->setTextCursor(cur);
        for(int i=0; i<this->extraSelections().size(); i++) {
            if (i > 0) {
                st = st + (w->isKijelolesUjSelSepa ? SZEPA1 : "");
            }
            st = st + this->toPlainText().mid(
                    this->selList[i].x(),
                    this->selList[i].y() - this->selList[i].x());
        }
    } else {
        QList<QTextEdit::ExtraSelection> extraSelections;
        this->setExtraSelections(extraSelections);
        st = this->textCursor().selectedText();
    }
    st = st.trimmed();
    this->setFocus();
    return st;
}

void SaTextEdit::cedictKeresSlot() {
    w->cedictKerdez(this->selBeallitas());
}
