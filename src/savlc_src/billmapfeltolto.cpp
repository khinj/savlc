﻿#include <QtGui>


void billMapFeltolto(QMap<QString, long> *billMap) {
    billMap->insert(QString("ESCAPE"), Qt::Key_Escape);   //0x01000000 | 16777216 |
    billMap->insert(QString("F1"), Qt::Key_F1);   //0x01000030 | 16777264 |
    billMap->insert(QString("F2"), Qt::Key_F2);   //0x01000031 | 16777265 |
    billMap->insert(QString("F3"), Qt::Key_F3);   //0x01000032 | 16777266 |
    billMap->insert(QString("F4"), Qt::Key_F4);   //0x01000033 | 16777267 |
    billMap->insert(QString("F5"), Qt::Key_F5);   //0x01000034 | 16777268 |
    billMap->insert(QString("F6"), Qt::Key_F6);   //0x01000035 | 16777269 |
    billMap->insert(QString("F7"), Qt::Key_F7);   //0x01000036 | 16777270 |
    billMap->insert(QString("F8"), Qt::Key_F8);   //0x01000037 | 16777271 |
    billMap->insert(QString("F9"), Qt::Key_F9);   //0x01000038 | 16777272 |
    billMap->insert(QString("F10"), Qt::Key_F10);   //0x01000039 | 16777273 |
    billMap->insert(QString("F11"), Qt::Key_F11);   //0x0100003a | 16777274 |
    billMap->insert(QString("F12"), Qt::Key_F12);   //0x0100003b | 16777275 |
    billMap->insert(QString("PRINT"), Qt::Key_Print);   //0x01000009 | 16777225 |
    billMap->insert(QString("SCROLLLOCK"), Qt::Key_ScrollLock);   //0x01000026 | 16777254 |
    billMap->insert(QString("PAUSE"), Qt::Key_Pause);   //0x01000008 | 16777224 | The Pause/Break key (Note: Not anything to do with pausing media)
    billMap->insert(QString("0"), Qt::Key_0);   //0x30 | 48 |
    billMap->insert(QString("1"), Qt::Key_1);   //0x31 | 49 |
    billMap->insert(QString("2"), Qt::Key_2);   //0x32 | 50 |
    billMap->insert(QString("3"), Qt::Key_3);   //0x33 | 51 |
    billMap->insert(QString("4"), Qt::Key_4);   //0x34 | 52 |
    billMap->insert(QString("5"), Qt::Key_5);   //0x35 | 53 |
    billMap->insert(QString("6"), Qt::Key_6);   //0x36 | 54 |
    billMap->insert(QString("7"), Qt::Key_7);   //0x37 | 55 |
    billMap->insert(QString("8"), Qt::Key_8);   //0x38 | 56 |
    billMap->insert(QString("9"), Qt::Key_9);   //0x39 | 57 |
    billMap->insert(QString("ö"), Qt::Key_Odiaeresis);   //0x0d6 | 214 | ö
    billMap->insert(QString("ü"), Qt::Key_Udiaeresis);   //0x0dc | 220 | ü
    billMap->insert(QString("ó"), Qt::Key_Oacute);   //0x0d3 | 211 | ó
    billMap->insert(QString("BACKSPACE"), Qt::Key_Backspace);   //0x01000003 | 16777219 |
    billMap->insert(QString("INSERT"), Qt::Key_Insert);   //0x01000006 | 16777222 |
    billMap->insert(QString("HOME"), Qt::Key_Home);   //0x01000010 | 16777232 |
    billMap->insert(QString("PAGEUP"), Qt::Key_PageUp);   //0x01000016 | 16777238 |
    billMap->insert(QString("NUMLOCK"), Qt::Key_NumLock);   //0x01000025 | 16777253 |
    billMap->insert(QString("SLASH"), Qt::Key_Slash);   //0x2f | 47 | /
    billMap->insert(QString("ASTERISK"), Qt::Key_Asterisk);   //0x2a | 42 | *
    billMap->insert(QString("MINUS"), Qt::Key_Minus);   //0x2d | 45 |
//    billMap->insert(QString("TAB"), Qt::Key_Tab);   //0x01000001 | 16777217 |     //ez nem jó
    billMap->insert(QString("TAB"), Qt::Key_Tab + 1);   //0x01000002 | 16777218 |        //ez a jó
    billMap->insert(QString("a"), Qt::Key_A);   //0x41 | 65 |
    billMap->insert(QString("b"), Qt::Key_B);   //0x42 | 66 |
    billMap->insert(QString("c"), Qt::Key_C);   //0x43 | 67 |
    billMap->insert(QString("d"), Qt::Key_D);   //0x44 | 68 |
    billMap->insert(QString("e"), Qt::Key_E);   //0x45 | 69 |
    billMap->insert(QString("f"), Qt::Key_F);   //0x46 | 70 |
    billMap->insert(QString("g"), Qt::Key_G);   //0x47 | 71 |
    billMap->insert(QString("h"), Qt::Key_H);   //0x48 | 72 |
    billMap->insert(QString("i"), Qt::Key_I);   //0x49 | 73 |
    billMap->insert(QString("j"), Qt::Key_J);   //0x4a | 74 |
    billMap->insert(QString("k"), Qt::Key_K);   //0x4b | 75 |
    billMap->insert(QString("l"), Qt::Key_L);   //0x4c | 76 |
    billMap->insert(QString("m"), Qt::Key_M);   //0x4d | 77 |
    billMap->insert(QString("n"), Qt::Key_N);   //0x4e | 78 |
    billMap->insert(QString("o"), Qt::Key_O);   //0x4f | 79 |
    billMap->insert(QString("p"), Qt::Key_P);   //0x50 | 80 |
    billMap->insert(QString("q"), Qt::Key_Q);   //0x51 | 81 |
    billMap->insert(QString("r"), Qt::Key_R);   //0x52 | 82 |
    billMap->insert(QString("s"), Qt::Key_S);   //0x53 | 83 |
    billMap->insert(QString("t"), Qt::Key_T);   //0x54 | 84 |
    billMap->insert(QString("u"), Qt::Key_U);   //0x55 | 85 |
    billMap->insert(QString("v"), Qt::Key_V);   //0x56 | 86 |
    billMap->insert(QString("w"), Qt::Key_W);   //0x57 | 87 |
    billMap->insert(QString("x"), Qt::Key_X);   //0x58 | 88 |
    billMap->insert(QString("y"), Qt::Key_Y);   //0x59 | 89 |
    billMap->insert(QString("z"), Qt::Key_Z);   //0x5a | 90 |
    billMap->insert(QString("ő"), 336);   // | 336 | ő
    billMap->insert(QString("ú"), 218);   // | 218 | ú
    billMap->insert(QString("RETURN"), Qt::Key_Return);   //0x01000004 | 16777220 |
    billMap->insert(QString("DELETE"), Qt::Key_Delete);   //0x01000007 | 16777223 |
    billMap->insert(QString("END"), Qt::Key_End);   //0x01000011 | 16777233 |
    billMap->insert(QString("PAGEDOWN"), Qt::Key_PageDown);   //0x01000017 | 16777239 |
    billMap->insert(QString("PLUS"), Qt::Key_Plus);   //0x2b | 43 |
    billMap->insert(QString("CAPSLOCK"), Qt::Key_CapsLock);   //0x01000024 | 16777252 |
    billMap->insert(QString("é"), 201);   // | 201 | é
    billMap->insert(QString("á"), 193);   // | 193 | á
    billMap->insert(QString("ű"), 368);   // | 368 | ű
    billMap->insert(QString("í"), 205);   // | 205 | í
    billMap->insert(QString(","), Qt::Key_Comma);   //0x2c | 44 |
    billMap->insert(QString("."), Qt::Key_Period);   //0x2e | 46 |
    billMap->insert(QString("UP"), Qt::Key_Up);   //0x01000013 | 16777235 |
    billMap->insert(QString("ENTER"), Qt::Key_Enter);   //0x01000005 | 16777221 | Typically located on the keypad.
    billMap->insert(QString("META"), Qt::Key_Meta);   //0x01000022 | 16777250 | On Mac OS X, this corresponds to the Control keys. On Windows keyboards, this key is mapped to the Windows key.
    billMap->insert(QString("SPACE"), Qt::Key_Space);   //0x20 | 32 |
    billMap->insert(QString("JOBBEGÉRGOMB"), 16777301);   // | 16777301 | Jobb egérgomb
    billMap->insert(QString("LEFT"), Qt::Key_Left);   //0x01000012 | 16777234 |
    billMap->insert(QString("DOWN"), Qt::Key_Down);   //0x01000015 | 16777237 |
    billMap->insert(QString("RIGHT"), Qt::Key_Right);   //0x01000014 | 16777236 |
    billMap->insert(QString("!"), Qt::Key_Exclam);   //0x21 | 33 | !
    billMap->insert(QString(""""), Qt::Key_QuoteDbl);   //0x22 | 34 | "
    billMap->insert(QString("#"), Qt::Key_NumberSign);   //0x23 | 35 | #
    billMap->insert(QString("$"), Qt::Key_Dollar);   //0x24 | 36 | $
    billMap->insert(QString("%"), Qt::Key_Percent);   //0x25 | 37 | %
    billMap->insert(QString("&"), Qt::Key_Ampersand);   //0x26 | 38 | &
    billMap->insert(QString("'"), Qt::Key_Apostrophe);   //0x27 | 39 | '
    billMap->insert(QString("("), Qt::Key_ParenLeft);   //0x28 | 40 | (
    billMap->insert(QString(")"), Qt::Key_ParenRight);   //0x29 | 41 | )
    billMap->insert(QString(":"), Qt::Key_Colon);   //0x3a | 58 | :
    billMap->insert(QString(";"), Qt::Key_Semicolon);   //0x3b | 59 | ;
    billMap->insert(QString("<"), Qt::Key_Less);   //0x3c | 60 | <
    billMap->insert(QString("="), Qt::Key_Equal);   //0x3d | 61 | =
    billMap->insert(QString(">"), Qt::Key_Greater);   //0x3e | 62 | >
    billMap->insert(QString("@"), Qt::Key_At);   //0x40 | 64 | @
    billMap->insert(QString("["), Qt::Key_BracketLeft);   //0x5b | 91 | [
    billMap->insert(QString("\\"), Qt::Key_Backslash);   //0x5c | 92 | Backslash
    billMap->insert(QString("]"), Qt::Key_BracketRight);   //0x5d | 93 | ]
    billMap->insert(QString("^"), Qt::Key_AsciiCircum);   //0x5e | 94 | ^
    billMap->insert(QString("_"), Qt::Key_Underscore);   //0x5f | 95 | _
    billMap->insert(QString("{"), Qt::Key_BraceLeft);   //0x7b | 123 | {
    billMap->insert(QString("|"), Qt::Key_Bar);   //0x7c | 124 | |
    billMap->insert(QString("}"), Qt::Key_BraceRight);   //0x7d | 125 | }
    billMap->insert(QString("Ł"), Qt::Key_sterling);   //0x0a3 | 163 | Ł
    billMap->insert(QString("Shift"), Qt::ShiftModifier);   //0x02000000 | 33554432 | A Shift key on the keyboard is pressed.
    billMap->insert(QString("Ctrl"), Qt::ControlModifier);   //0x04000000 | 67108864 | A Ctrl key on the keyboard is pressed.
    billMap->insert(QString("Alt"), Qt::AltModifier);   //0x08000000 | 134217728 | An Alt key on the keyboard is pressed.
    billMap->insert(QString("Meta"), Qt::MetaModifier);   //0x10000000 | 268435456 | A Meta key on the keyboard is pressed.
    billMap->insert(QString("KP"), Qt::KeypadModifier);   //0x20000000 | 536870912 | A keypad button is pressed.
}
