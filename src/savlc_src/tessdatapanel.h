#ifndef TESSDATAPANEL_H
#define TESSDATAPANEL_H

#include <QDialog>

namespace Ui {
class TessdataPanel;
}

class TessdataPanel : public QDialog
{
    Q_OBJECT
    
public:
    explicit TessdataPanel(QWidget *parent = 0);
    ~TessdataPanel();
    Ui::TessdataPanel *ui;
};

#endif // TESSDATAPANEL_H
