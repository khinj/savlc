﻿//http://doc.qt.io/archives/qt-4.8/qt-network-fortuneserver-example.html

//"g:\Beszed\Mandarin\Film\OCRTeszt\Teszt.mp4"
//"g:\Beszed\Mandarin\Film\OCRTeszt\Teszt.mp4.bkm"
//"g:\Beszed\Mandarin\Film\GuCh\GuCh_070 - 078\GuCh_073_After_exams.bkm"
//"g:\beszed\mandarin\Kínai nyelvkönyv magyaroknak\4_09_BC_lecke.mp3"
//"f:\Ideigl\i\L is not B.flv"
//"f:\Zene\Egyéb\Perfume - レーザービーム_sa.mp4"

#include "samainwindow.h"
#include "savideo.h"
#include "satextedit.h"
#include "felirat.h"
#include "sadefs.h"
#include <iostream>
#include <windows.h>

QString exePath, exeName;
extern void billMapFeltolto(QMap<QString, long> *billMap);
extern bool isVege;
void fajlmegnyitasElokeszites(bool isStart);
void billEvent(int e);
void kilepes();
void masolas();
void alapMod();
void sugoBeallitas();
double idoStToDouble(QString st);
QString idoDoubleToSt(double d);
double mesterIdo, videoHossz;
double tA, tB;   //-1.0, ha nincs meghatározva
int hangEro;
QString cedictDict;
double feliratKesleltetes, hangKesleltetes, vilagossag, elesseg, telitettseg, gamma;
int szinHo;
QStringList aspectRatioTomb;
int aspectRatioIdx;
QString idoC, idoBGC1, idoBGC2;   //idő ablak betűszíne és 2 háttérszíne
QString billTriggerSt;
double SaTextEdit5BCArany;        //betűszín és a 2. háttérszín aránya
QString sebessegek;
int sebessegIdx;
QString lepesek;
int pauseMod; //0: nincs pause, 1: van pause, frissíteni kell az időablakot,
        //2: van pause, nem kell frissíteni az időablakot
int feliratPause;   /*0: nincs felirat pause, 1: van felirat pause*/
int autoFeliratPause;   //0: feliratPause nem állítódik be AB szakasz kijelöléskor, egyébként igen
double feliratPauseIdo;
double Ugras_rovid, Ugras_kozepes, Ugras_hosszu;
long BILL_kilepes, BILL_start_stop, BILL_start_stop2, BILL_rovid_elore, BILL_rovid_hatra, BILL_kozepes_elore, BILL_kozepes_hatra, BILL_hosszu_elore, BILL_hosszu_hatra, BILL_hangero_fel, BILL_hangero_le, BILL_nagy_hangero_fel, BILL_nagy_hangero_le, BILL_tempo_gyorsabb, BILL_tempo_normal, BILL_tempo_lassabb, BILL_tempo_lassabb2, BILL_kijelzes, BILL_ABszakasz, BILL_ABszakasz2, BILL_ABszakasz3, BILL_ABszakasz4, BILL_ABszakasz5, BILL_bkmToggle, BILL_bkmCommandToggle, BILL_bkm0, BILL_bkmKov, BILL_bkmElozo, BILL_SaTextEditToggle0, BILL_SaTextEditToggle1, BILL_SaTextEditToggle2, BILL_SaTextEditToggle3, BILL_SaTextEditToggle4, BILL_SaTextEditToggle5, BILL_SaTextEditToggle6, BILL_SaTextEditToggle6B, BILL_feliratToggle, BILL_felirat0Valt, BILL_felirat1Valt, BILL_feliratBetolt, BILL_AblakElrendezesValtas, BILL_TgAlap, BILL_TgFeliratKesleltetes, BILL_TgHangKesleltetes, BILL_TgVilagossag, BILL_TgElesseg, BILL_TgTelitettseg, BILL_TgGamma, BILL_TgSzinHo, BILL_TgKeparany, BILL_egyKockaElore, BILL_IttJarok, BILL_TeljesKepernyoTg, BILL_AblakmozgatasTg, BILL_PotiToggle, BILL_feliratPauseTg, BILL_autoFeliratPauseTg, BILL_OCRTesztPadTg, BILL_trigger, BILL_fajlMegnyitas, BILL_UjFeliratMentes, BILL_FeliratKezdopont, BILL_FeliratVegpont, BILL_FeliratVegpont2, BILL_FeliratAppend, BILL_FeliratKezdopont2, BILL_FeliratKezdopont3, BILL_kijelolesVissza, BILL_kijelolesElore, BILL_kijelolesKiterjeszt, BILL_kijelolesUjSel, BILL_kijelolesUjSelSzepaTg, BILL_kijelolesTorol, BILL_SzerkesztesBeMentes, BILL_snapshot, BILL_cedictAblakTg, BILL_editAblakValtas;
enum TgE {AlapTg, FeliratKesleltetesTg, VilagossagTg, ElessegTg, TelitettsegTg, KeparanyTg, HangKesleltetesTg, GammaTg, SzinHoTg} Tg;

QList<double> bkmTimeL;
QList<QString> bkmCommentL;
QList<QString> bkmAllCommandsL;
QList<QString> bkmCurrentCommandL;
bool bkmForceNew = false;
bool bkmForceNewFn();
qint64 saTimerStartMs = -1;
bool saTimerMs(long ms);
long saTimerMsDT;
bool bkmCommandOn = false;  //displays C when on
int bkmIdx, bkmN;
QString bkmCurrentCommand;
int bkmCurrentCommandParameter = -1;
int bkmSleepMs;
bool bkmIsSleepOn = false;  // C or c
QString bkmFileName, bkmFileSt, bkmFajlStOriginal;
void beep(int ms);
void manageBkmCommands();
QString bkmPrefix1 = "# BKM commands e.g.:\n";
QString bkmPrefix2 = "# $ beep 1000| sleep 1000| play| sleepPlayToTime 1.1 00:00:19,437| sleepPlayToNextBKM 1.1| sleepPlayToBKM 1.1 1| playToTimeSleep 1.1 00:00:19,437| playToNextBKMSleep 1.1| playToBKMSleep 1.1 1| goToNextBKM| goToBKM 1\n\n";
void bkmClearCommands();
void bkmUpdateFileSt();

double ittJarokPos;
QString sugoSt;
QString felirat0 = "0";
QString openedFilePath, openedFileName, openedFileExtension, videoFile;
QString iniFileName;
QString iniFileSt;
QList<Felirat*> feliratok;
int N_feliratok;
void bkmIniBeolvas();
void iniBeolvasB();
void kezdetiBeallitasok();
void feliratKezel(bool saLoopHivjaMeg);
QString qRectToqString(QRect r);
QRect qStringToqRect(QString st);
QString getParameter(QString *forras, QString parNev);
void setParameter(QString *forras, QString parNev, QString ertek, bool torlesHaUres = false);
void saLoop();
QString fileToQString(QString fajlNev);
void billKezel(QKeyEvent *);
void qStToFile(QString fajlNev, QString st, bool withBOM = false);
bool globalVisible;
QRect foablakRect;
QStringList argL;
int aktualisAblakElrendezes = 0;
void ablakElrendezes(int ujAe, bool kellMentes);
QApplication *app;
void testFv();
bool ablakMozgatas;
void ablakMozgatasTg();
SaMainWindow *w;
QMap<QString, long> *billMap;
int BillKodFv(QString billSt);
QLabel *msgBox;
QTime msgBoxIdoS;
int msgBoxIdo;
void showMsgBox(QString text, QColor bgColor, int msec);
QLabel *imgSzerkesztes;
QLabel *imgMozgatas;
void changePos(double pos);
QString billBuffer;      //azért, hogy pl. a nyilak működjenek Hotkey -el
bool foglalt;
bool kellRepaint;   //azért, hogy a satexedit elrejtése rendesen működjön (takaro)
QWidget *takaroAlso, *takaroJobb;   //azért, mert a player-ben a képernyőfelbontáson kívüli rész
                                        //nem jól rajzólódik ki
QString SaMainWindow::nyelv = "";
QString SaMainWindow::nyelvSt = "";
QString SaMainWindow::cedictSzotar = "FvChToEnNyers|";
bool isOCRmaradjonLathato;
SaTextEdit* kijelolesiAblak();
bool isFileExists(QString st);
double getIttJarokTimeFromIni();
void setIttJarokTimeInIni(double t);
void delIttJarokTimeInIni();
void iniMent(bool isWindowArrangementNeeded);
bool lehettAElott = false;
double min_B_A;
void saveSubtitle(int par);
double ujFeliratHossz;
void feliratPauseSetting(bool isAB);
int startDelay;     //millisecond
int xms_millisec;     //felirat kezdőpont xms_millisec-kel előrébb
int xms_millisec2;    //új felirat hozzáadásakorfelirat kezdőpont xms_millisec2-vel előrébb


#define TRIGST "r"

int main(int argc, char *argv[]) {
    foglalt = true;
    app = new QApplication(argc, argv);
    fajlmegnyitasElokeszites(true);

    //OCR init:
    if (w->ocr->tess.Init(exePath.toLatin1() +
                "/../tessdata", w->ocr->tessdataSt.toLatin1())) {
        qWarning("OCRTesseract: Could not initialize tesseract.");
    }

    sugoBeallitas();
    w->teL[6]->setLathato(false);  //SaSúgó ablak elrejtése
    w->teL[7]->setLathato(false);  //Cedict ablak elrejtése
    w->teL[8]->setLathato(false);  //Súgó ablak elrejtése
    w->teL[9]->setLathato(false);  //OCR ablak elrejtése

    foglalt = false;
    w->saMainWindowMouseMoveEventFoglalt = false;

    return app->exec();
}

void saLoop() {
    static bool kellPause = true;
    mesterIdo = w->player->getPos();
    if (pauseMod == 0) {
        if (lehettAElott) {
            if (mesterIdo - tA > 0.0) {
                lehettAElott = false;
            }
        } else {
            if (mesterIdo - tA < -0.05) {
                w->changePos(tA);
                bkmForceNew = true;
            }
        }
        if (tB > -0.9 && tB - mesterIdo < 0.05 && bkmCurrentCommand == "") {
            w->changePos(tA);
            bkmForceNew = true;
        }
        w->potiFoglalt = true;
        w->poti->setSliderPosition(mesterIdo/videoHossz*100);
        w->potiFoglalt = false;
    }
    if (imgSzerkesztes->isHidden()) {
        int bkmIdxNew = w->keresIdx(bkmTimeL, mesterIdo + 0.1);

//        qWarning("\nbkmCommandOn:%d, bkmAllCommandsL[bkmIdxNew].size(): %d, bkmCurrentCommandL.size(): %d, bkmCurrentCommand: "
//                 + bkmCurrentCommand.toUtf8()
//                 + ", BKMNew: %d, BKM: %d, bkmForceNew: %d, pauseMod: %d"
//                 , bkmCommandOn, bkmAllCommandsL[bkmIdxNew].size(), bkmCurrentCommandL.size()
//                 , bkmIdxNew + 1, bkmIdx + 1, bkmForceNew, pauseMod);

        // New bookmark:
        if ( bkmCommandOn
                && bkmIdxNew > -1
                && bkmAllCommandsL[bkmIdxNew].size() != 0
                && bkmCurrentCommandL.size() == 0
                && bkmCurrentCommand == ""
                && (bkmForceNew || (bkmIdxNew != bkmIdx && pauseMod == 0)) ) {    // bkmIdx change should be managed:
            bkmIdx = bkmIdxNew;
            msgBox->hide();     //pl. bkm törlés/hozáadás kiírás elrejtése
            bkmCurrentCommandL = bkmAllCommandsL[bkmIdx].split("|");
            bkmCurrentCommandParameter = -1;
            w->player->pause();
            feliratKezel(false);
        } else {                                            // no need bkmIdx change to be managed:
            bkmIdx = bkmIdxNew;
            feliratKezel(true);
        }
        bkmForceNew = false;

//        qWarning("(1) BKM: %d, bkmCurrentCommandL.size(): %d, bkmCurrentCommand: " + bkmCurrentCommand.toUtf8() +
//                 ", bkmCurrentCommandParameter: %d, bkmSleepMs: %d, saTimerMsDT: %d"
//                 , bkmIdx + 1, bkmCurrentCommandL.size(), bkmCurrentCommandParameter, bkmSleepMs, saTimerMsDT);

        // Check once:
        if (bkmCurrentCommandL.size() != 0 && bkmCurrentCommandParameter == -1) {
            bkmCurrentCommand = bkmCurrentCommandL[0].trimmed();
            bkmCurrentCommandL.removeFirst();

//            qWarning("(2) BKM: %d, bkmCurrentCommandL.size(): %d, bkmCurrentCommand: " + bkmCurrentCommand.toUtf8() +
//                     ", bkmCurrentCommandParameter: %d, bkmSleepMs: %d, saTimerMsDT: %d"
//                     , bkmIdx + 1, bkmCurrentCommandL.size(), bkmCurrentCommandParameter, bkmSleepMs, saTimerMsDT);

            QString bkmCurrentCommand0 = bkmCurrentCommand.split(" ")[0];
            if (bkmCurrentCommand0 == "beep") {                      // beep ms:
                bkmCurrentCommandParameter = bkmCurrentCommand.split(" ")[1].toInt();
                if (pauseMod != 1) {
                    w->player->pause();
                }
            } else if (bkmCurrentCommand0 == "sleep") {              // sleep ms:
                bkmCurrentCommandParameter = 0;     // should not be -1
                bkmSleepMs = bkmCurrentCommand.split(" ")[1].toInt();
                bkmIsSleepOn = true;
                if (pauseMod != 1) {
                    w->player->pause();
                }
            } else if (bkmCurrentCommand0 == "play") {               // play:
                if (pauseMod != 0) {
                    w->player->play();
                }
                bkmCurrentCommand = "";
            } else if (bkmCurrentCommand0 == "sleepPlayToTime") {         // sleepPlayToTime f "hh:mm:ss,xxx":
                bkmCurrentCommandParameter = idoStToDouble(bkmCurrentCommand.split(" ")[2]) * 1000;
                double f = bkmCurrentCommand.split(" ")[1].toDouble();
                bkmSleepMs = (bkmCurrentCommandParameter - mesterIdo * 1000) * f;
                bkmIsSleepOn = true;
            } else if (bkmCurrentCommand0 == "sleepPlayToBKM" || bkmCurrentCommand0 == "sleepPlayToNextBKM") { // sleepPlayTo(Next)BKM f (n):
                int n;
                if (bkmCurrentCommand0 == "sleepPlayToBKM") {
                    n = bkmCurrentCommand.split(" ")[2].toInt();
                } else {
                    n = bkmIdx + 1 + 1;
                }
                bkmCurrentCommandParameter = n - 1;
                if (bkmCurrentCommandParameter > bkmN - 1) {
                    showMsgBox(w->satr("sleepPlayTo(Next)BKM error (n is too big)!")
                               + " (n: " + QString::number(n) + ")", QColor(200, 100, 100), 10000);
                    bkmCurrentCommand = "";
                    bkmCurrentCommandParameter = -1;
                } else if (bkmCurrentCommandParameter < bkmIdx + 1) {
                    showMsgBox(w->satr("sleepPlayTo(Next)BKM error (n is too small)!")
                               + " (n: " + QString::number(n) + ")", QColor(200, 100, 100), 10000);
                    bkmCurrentCommand = "";
                    bkmCurrentCommandParameter = -1;
                } else if (pauseMod != 0) {
                    double f = bkmCurrentCommand.split(" ")[1].toDouble();
                    bkmSleepMs = (bkmTimeL[bkmCurrentCommandParameter] - mesterIdo) * 1000 * f;
                    bkmIsSleepOn = true;
                }
            } else if (bkmCurrentCommand0 == "playToTimeSleep") {         // playToTimeSleep f "hh:mm:ss,xxx":
                bkmCurrentCommandParameter = idoStToDouble(bkmCurrentCommand.split(" ")[2]) * 1000;
                double f = bkmCurrentCommand.split(" ")[1].toDouble();
                bkmSleepMs = (bkmCurrentCommandParameter - mesterIdo * 1000) * f;
            } else if (bkmCurrentCommand0 == "playToBKMSleep" || bkmCurrentCommand0 == "playToNextBKMSleep") { // playTo(Next)BKMSleep f (n):
                int n;
                if (bkmCurrentCommand0 == "playToBKMSleep") {
                    n = bkmCurrentCommand.split(" ")[2].toInt();
                } else {
                    n = bkmIdx + 1 + 1;
                }
                bkmCurrentCommandParameter = n - 1;
                if (bkmCurrentCommandParameter > bkmN - 1) {
                    showMsgBox(w->satr("playTo(Next)BKMSleep error (n is too big)!")
                               + " (n: " + QString::number(n) + ")", QColor(200, 100, 100), 10000);
                    bkmCurrentCommand = "";
                    bkmCurrentCommandParameter = -1;
                } else if (bkmCurrentCommandParameter < bkmIdx + 1) {
                    showMsgBox(w->satr("playTo(Next)BKMSleep error (n is too small)!")
                               + " (n: " + QString::number(n) + ")", QColor(200, 100, 100), 10000);
                    bkmCurrentCommand = "";
                    bkmCurrentCommandParameter = -1;
                } else if (pauseMod != 0) {
                    double f = bkmCurrentCommand.split(" ")[1].toDouble();
                    bkmSleepMs = (bkmTimeL[bkmCurrentCommandParameter] - mesterIdo) * 1000 * f;
                }

            } else if (bkmCurrentCommand0 == "goToNextBKM") {            // goToNextBKM:
                if (bkmIdx > bkmN - 2) {
                    showMsgBox(w->satr("goToNextBKM error!"), QColor(200, 100, 100), 10000);
                    bkmCurrentCommand = "";
                    bkmCurrentCommandParameter = -1;
                } else {
                    w->player->setPos(bkmTimeL[bkmIdx + 1]);
                    bkmCurrentCommand = "";
                    bkmCurrentCommandParameter = -1;
                    bkmForceNew = true;
                }
            } else if (bkmCurrentCommand0 == "goToBKM") {            // goToBKM n:
                bkmCurrentCommandParameter = bkmCurrentCommand.split(" ")[1].toInt();
                if (bkmCurrentCommandParameter > bkmN) {
                    showMsgBox(w->satr("goToBKM error!"), QColor(200, 100, 100), 10000);
                    bkmCurrentCommand = "";
                    bkmCurrentCommandParameter = -1;
                } else {
                    w->player->setPos(bkmTimeL[bkmCurrentCommandParameter - 1]);
                    bkmCurrentCommand = "";
                    bkmCurrentCommandParameter = -1;
                    bkmForceNew = true;
                }
            }
            feliratKezel(false);
        }

        // Continouos check:
//        qWarning("(3) BKM: %d, bkmCurrentCommandL.size(): %d, bkmCurrentCommand: " + bkmCurrentCommand.toUtf8() +
//                 ", bkmCurrentCommandParameter: %d, bkmSleepMs: %d, saTimerMsDT: %d"
//                 , bkmIdx + 1, bkmCurrentCommandL.size(), bkmCurrentCommandParameter, bkmSleepMs, saTimerMsDT);

        QString bkmCurrentCommand0 = bkmCurrentCommand.split(" ")[0];
        if (bkmCurrentCommand0 == "beep") {                      // beep ms:
            w->player->setPos(bkmTimeL[bkmIdx]);
            if (saTimerMs(100)) {
                feliratKezel(false);    // Waiting for feliratKezel refresh
            } else {
                beep(bkmCurrentCommandParameter);
                bkmCurrentCommand = "";
                bkmCurrentCommandParameter = -1;
            }
        } else if (bkmCurrentCommand0 == "sleep") {              // sleep ms:
            if (!saTimerMs(bkmSleepMs)) {
                bkmCurrentCommand = "";
                bkmCurrentCommandParameter = -1;
                bkmIsSleepOn = false;
                msgBox->hide();
                feliratKezel(false);
            } else {
                showMsgBox(w->satr("Sleep") + ": " + QString::number(int(saTimerMsDT/1000+1)), QColor(200, 200, 100), 10000);
            }
        } else if (bkmCurrentCommand0 == "sleepPlayToTime") {                  // sleepPlayToTime f "hh:mm:ss,xxx":
            if (!saTimerMs(bkmSleepMs)) {
                if (pauseMod != 0) {
                    saTimerStartMs = -2;
                    bkmIsSleepOn = false;
                    msgBox->hide();
                    w->player->play();
                }
                if (mesterIdo * 1000 > bkmCurrentCommandParameter) {
                    w->player->pause();
                    saTimerStartMs = -1;
                    bkmCurrentCommand = "";
                    bkmCurrentCommandParameter = -1;
                    feliratKezel(false);
                }
            } else {
                showMsgBox(w->satr("Sleep") + ": " + QString::number(int(saTimerMsDT/1000+1)), QColor(200, 200, 100), 10000);
            }
        } else if (bkmCurrentCommand0 == "sleepPlayToBKM" || bkmCurrentCommand0 == "sleepPlayToNextBKM") { // sleepPlayTo(Next)BKM f (n):
            if (!saTimerMs(bkmSleepMs)) {
                if (pauseMod != 0) {
                    saTimerStartMs = -2;
                    bkmIsSleepOn = false;
                    msgBox->hide();
                    w->player->play();
                }
                if (bkmIdx == bkmCurrentCommandParameter) {
                    w->player->pause();
                    saTimerStartMs = -1;
                    bkmCurrentCommand = "";
                    bkmCurrentCommandParameter = -1;
                    bkmForceNew = true;
                    feliratKezel(false);
                }
            } else {
                showMsgBox(w->satr("Sleep") + ": " + QString::number(int(saTimerMsDT/1000+1)), QColor(200, 200, 100), 10000);
            }
        } else if (bkmCurrentCommand0 == "playToTimeSleep") {                  // playToTimeSleep f "hh:mm:ss,xxx":
            if (mesterIdo * 1000 <= bkmCurrentCommandParameter) {
                if (pauseMod != 0) {
                    bkmIsSleepOn = false;
                    w->player->play();
                }
            } else {
                if (pauseMod == 0) {
                    w->player->pause();
                    bkmIsSleepOn = true;
                    msgBox->hide();
                    feliratKezel(false);
                }
                if (!saTimerMs(bkmSleepMs)) {
                    saTimerStartMs = -1;
                    bkmCurrentCommand = "";
                    bkmCurrentCommandParameter = -1;
                    feliratKezel(false);
                } else {
                    showMsgBox(w->satr("Sleep") + ": " + QString::number(int(saTimerMsDT/1000+1)), QColor(200, 200, 100), 10000);
                }
            }
        } else if (bkmCurrentCommand0 == "playToBKMSleep" || bkmCurrentCommand0 == "playToNextBKMSleep") { // playTo(Next)BKMSleep f (n):
            if (bkmIdx < bkmCurrentCommandParameter) {
                if (pauseMod != 0) {
                    bkmIsSleepOn = false;
                    w->player->play();
                }
            } else {
                if (pauseMod == 0) {
                    w->player->pause();
                    bkmIsSleepOn = true;
                    feliratKezel(false);
                }
                if (!saTimerMs(bkmSleepMs)) {
                    saTimerStartMs = -1;
                    bkmCurrentCommand = "";
                    bkmCurrentCommandParameter = -1;
                    msgBox->hide();
                    feliratKezel(false);
                    bkmForceNew = true;
                } else {
                    showMsgBox(w->satr("Sleep") + ": " + QString::number(int(saTimerMsDT/1000+1)), QColor(200, 200, 100), 10000);
                }
            }
        }
    }

    if (kellRepaint) {
        if (takaroAlso && takaroJobb) {
            QRect rec = QApplication::desktop()->screenGeometry();
            if (imgMozgatas->isVisible()) {
                takaroAlso->setGeometry(8, rec.height(), w->player->width()-16,
                        w->player->height()-rec.height()-8);
                takaroAlso->raise();
                takaroAlso->show();
                takaroJobb->setGeometry(rec.width(), 8, w->player->width()-rec.width()-8,
                        w->player->height()-16);
                takaroJobb->raise();
                takaroJobb->show();
            } else {
                takaroAlso->setGeometry(0, rec.height(), w->player->width(), w->player->height()-rec.height());
                takaroAlso->raise();
                takaroAlso->show();
                takaroJobb->setGeometry(rec.width(), 0, w->player->width()-rec.width(), w->player->height());
                takaroJobb->raise();
                takaroJobb->show();
            }
        }
    }
    if (msgBoxIdoS.elapsed() > msgBoxIdo) {
        msgBox->hide();
    }


    //qWarning("te->cursor().shape(): %d", te->cursor().shape());
}

void clearA() {
    tA = -1.0;
}

void clearB() {
    tB = -1.0;
    if (autoFeliratPause == 1) {
        feliratPause = 0;   //AB szakasz megszűnésekor megszüntetjük a felírat rögzítést
    }
}

void clearAB() {
    clearA();
    clearB();
}

void feliratPauseSetting(bool isAB) {
    if (!isAB) {
        double t = mesterIdo - feliratKesleltetes;
        int iAkt = feliratok[0]->keres(t);
        if (feliratok[0]->t1[iAkt] <= t && t <= feliratok[0]->t2[iAkt]) {
            feliratPause = 1;
            feliratPauseIdo = t;
        }
    } else if (feliratok.size() > 0) {
        double t = (tB + tA)/2;
        int iAkt = feliratok[0]->keres(t);
        if (tA <= feliratok[0]->t2[iAkt] ||
                (iAkt < feliratok[0]->t1.size()-1 && feliratok[0]->t1[iAkt+1] < tB)) {
            if (iAkt < feliratok[0]->t1.size()-1 &&
                    qAbs(mesterIdo - feliratKesleltetes - feliratok[0]->t1[iAkt+1]) <
                            qAbs(mesterIdo - feliratKesleltetes - feliratok[0]->t2[iAkt])) {
                iAkt++;
            }
            feliratPause = 1;
            feliratPauseIdo = (feliratok[0]->t1.at(iAkt) + feliratok[0]->t2.at(iAkt)) / 2;
        }
    }
}


void setA() {
    double pos;
    if (isVege) {
        pos = videoHossz - 0.4;
    } else {
        pos = w->player->getPos();
    }
    tA = pos;
    if (tB - tA > 0.1) {
        if (autoFeliratPause != 0) {
            feliratPauseSetting(true);
        }
    } else {
        clearB();
    }
}

void setB(bool shouldClearAB) {
    if (tA > -0.9) {
        double pos;
        if (isVege) {
            pos = videoHossz - 0.4;
        } else {
            pos = w->player->getPos();
            if (!shouldClearAB && pos - tA < min_B_A) {
                pos = qMin(tA + min_B_A, videoHossz - 0.4);
            }
        }
        tB = pos;
        if (tB - tA > 0.1) {
            if (autoFeliratPause != 0) {
                feliratPauseSetting(true);
            }
        } else {
            clearAB();
        }
    }
}

void setABszakasz() {
    //Normál AB szakasz beállítás
    if (tA < -0.9) {
        setA();
    } else if (tB < -0.9){
        setB(true);
    } else {
        clearAB();
    }
    //qWarning("setABszakasz: tA = %f, tB = %f", tA, tB);
}

void setABszakasz2() {
    //Ha van tA, akkor tB-t beállítja az aktuális pozícióba, ha nincs, akkor tA-t állítja be az aktuális pozícióba
    if (tA > -0.9) {
        setB(false);
    } else {
        setA();
    }
    //qWarning("setABszakasz2: tA = %f, tB = %f, tB-tA = %f", tA, tB, tB-tA);
}

void setABszakasz3() {
    //Ha nincs tA, akkor beállítja tA-t. Ha van tA, akkor tB-t invertálja
    if (tA < -0.9) {
        setA();
    } else {
        if (tB < -0.9) {
            setB(true);
        } else {
            clearB();
        }
    }
//    qWarning("setABszakasz3: tA = %f, tB = %f", tA, tB);
}

void setABszakasz4() {
    //Lényeg: tA <- tB
    double pos;
    if (isVege && tA > -0.9) {
        pos = videoHossz - 0.4;
    } else {
        pos = w->player->getPos();
    }
    if (tA < -0.9) {
        tA = pos;
    } else if (tA > -0.9 && tB > -0.9) {
        lehettAElott = true;
        feliratPause = 0;
        tA = tB;
        tB = -1.0;
    } else if (tB < -0.9 && pos-tA > 0.1) {
        tB = pos;
        if (autoFeliratPause != 0) {
            feliratPauseSetting(true);
        }
    } else {
        tA = -1.0;
        tB = -1.0;
        feliratPause = 0;   //AB szakasz megszűnésekor mindig megszüntetjük a felírat rögzítést
    }
//    qWarning("setABszakasz4: tA = %f, tB = %f", tA, tB);
}

void setABszakasz5() {
    //tA beállítása az aktuális pozícióra
    setA();
//    qWarning("setABszakasz5: tA = %f, tB = %f", tA, tB);
}

void ablakMozgatasTg() {
    if (!imgSzerkesztes->isVisible()) {
        ablakMozgatas = !ablakMozgatas;
        if (ablakMozgatas) {    //belépés ablakmozgatásba:
            w->setMouseTracking(true);
            imgMozgatas->raise();
            imgMozgatas->move(w->player->pos().x() + 30, w->player->pos().x() + 30);
            imgMozgatas->show();
            w->setKeretStyle(SaMainWindow::mozgat);
            w->setEgerKurzor(QCursor(Qt::OpenHandCursor));
            for (int i=0; i<w->teL.size(); i++) {
                w->teL[i]->setEnabled(false);
            }
            w->grabMouse();
        } else {                //kilépés ablakmozgatásból:
            w->setMouseTracking(false);
            w->mFr = NULL;
            w->eredetiGlobalPos = QPoint(-1, -1);
            imgMozgatas->hide();
            w->player->lower();
            w->poti->raise();
            w->hScrollBar->raise();
            w->vScrollBar->raise();
            w->setKeretStyle(SaMainWindow::semmi);
            w->setEgerKurzor(QCursor(Qt::ArrowCursor));
            for (int i=0; i<w->teL.size(); i++) {
                w->teL[i]->setEnabled(true);
            }
            w->releaseMouse();
            bool isFull = w->isFullScreen();
            if (w->isFullScreen()) {
                w->player->tgFullscreen();
            }
            kellRepaint = true;
            iniMent(true);
            if (isFull) {
                w->player->tgFullscreen();
            }
        }
        w->player->resizeEvent(0);
        w->poti->resizeEvent(0);
        w->scrollBarsRefresh();
    }
}

QString fileToQString(QString fajlNev) {
    //Beolvassa a fájlt QString-be Utf-8 kódolással, CR (carrige return) karaktereket kiveszi

    QString st = "";
    QFile fajl(fajlNev);
    if (!fajl.open(QIODevice::ReadOnly)) {
        qWarning("\nHIBA: fajlToQString, ez nincs meg: " + fajlNev.toUtf8());
    } else {
        QByteArray ba = fajl.readAll();
        fajl.close();
        st = QString::fromUtf8(ba);

        st = st.trimmed();
        st = st + "\n";     //muindenképpen legyen a végén új sor jel
        st.replace("\r", "");    //kocsi vissza kiszedése
    }
    //qWarning("fajlToQString, st = " + st.toUtf8());
    return st;
}

void qStToFile(QString fajlNev, QString st, bool withBOM) {
    //st-t kiírja Utf-8 kódolással

    QFile fajl(fajlNev);
    if (!fajl.open(QIODevice::WriteOnly | QIODevice::Text)) {
        qWarning("HIBA: QStringToFajl, ez nem írható ki: " + fajlNev.toUtf8());
        exit(233);
    }
    QByteArray ba;
    if (withBOM) {
        ba.resize(3);
        ba[0] = 0xef;
        ba[1] = 0xbb;
        ba[2] = 0xbf;
    }
    ba.append(st.toUtf8());
    fajl.write(ba);
    fajl.close();
}

void bkmIniBeolvas() {
    iniFileName = "";
    //billMap feltöltés:
    billMap = new QMap<QString, long>;
    billMapFeltolto(billMap);

    //*.bkm kezelése:
    bkmFileName = "";
    bkmFileSt = "";
    QString utolsoPar = argL.last(); //utolsó paraméter (fájlnév)
    QFileInfo fi(utolsoPar);
    openedFilePath = fi.absolutePath() + "/";
    openedFileName = fi.completeBaseName();
    openedFileExtension = fi.suffix();
    if (openedFileExtension == "bkm") {   //*.bkm-et nyitottak meg:
        bkmFileName = utolsoPar;
    } else if (isFileExists(openedFilePath + openedFileName + ".bkm")) {
        //nem *.bkm-et nyitottak meg, de létezik a megfelelő *.bkm:
        bkmFileName = openedFilePath + openedFileName + ".bkm";
    }


    if (bkmFileName !="") {   // a bkmFajl létezik:
        bkmFileSt = fileToQString(bkmFileName);
        if (bkmFileSt.startsWith(bkmPrefix1)) {
            if (!bkmFileSt.startsWith(bkmPrefix1 + bkmPrefix2)) {
                int pos = bkmFileSt.indexOf("\n\n$") + 2;
                bkmFileSt = bkmPrefix1 + bkmPrefix2 + bkmFileSt.mid(pos);
            }
        } else {
            bkmFileSt = bkmPrefix1 + bkmPrefix2 + bkmFileSt;
        }
        videoFile = openedFilePath + getParameter(&bkmFileSt, "VIDEOFAJL");
        if (!isFileExists(videoFile)) {
            QMessageBox::warning(w, "SaVLC",
                w->satr("A video fájl: %1, hivatkozva %2 -ben, nem létezik.").arg(videoFile).arg(bkmFileName),
                QMessageBox::Cancel);
            exit(1);
        }
        iniFileName = getParameter(&bkmFileSt, "INI");
        if (!QFileInfo(iniFileName).isAbsolute()) {
            iniFileName = openedFilePath + iniFileName;
        }
        if (!isFileExists(iniFileName)) {
            QMessageBox::warning(w, "SaVLC",
                w->satr("Az ini fájl: %1, hivatkozva %2 -ben, nem létezik.").arg(iniFileName).arg(bkmFileName),
                QMessageBox::Cancel);
            exit(1);
        }
        int i = 0;
        if (bkmFileSt.indexOf("$IttJarok=") == -1) {
            int pos = bkmFileSt.indexOf("$BKM");
            if (pos == -1) {
                bkmFileSt = bkmFileSt + "$IttJarok=\n";
            } else {
                bkmFileSt = bkmFileSt.mid(0, pos) + "$IttJarok=\n" + bkmFileSt.mid(pos);
            }
        }

        QStringList bkmFajlStL = bkmFileSt.split("\n");
        for (int i = 0; i < bkmFajlStL.size(); i++) {
            QString st = bkmFajlStL[i].trimmed();
            if (st.startsWith("$BKM")) {
                while (st.contains("  ")) {
                    st.replace("  ", " ");
                }
                QStringList stL = st.split("#")[0].split("=")[1].split("$");
                bkmTimeL.append(idoStToDouble(stL[0].trimmed()));
                if (stL.size() == 1) {
                    bkmAllCommandsL.append("");
                } else {
                    bkmAllCommandsL.append("" + stL[1].trimmed());
                }
                if (st.contains("#")) {
                    bkmCommentL.append(st.split("#")[1].trimmed());
                } else {
                    bkmCommentL.append("");
                }
                bkmN++;
                bkmFajlStL.removeAt(i);
                i--;
            }
        }
        bkmFileSt = bkmFajlStL.join("\n");
        bkmUpdateFileSt();
    } else {              //a bkmFájl nem létezik:
        videoFile = utolsoPar;
        bkmFileSt = bkmPrefix1 + bkmPrefix2 + "$VIDEOFAJL=" + QFileInfo(videoFile).fileName() + "\n" +
                "$INI=" + QFileInfo(videoFile).completeBaseName() + ".ini\n" +
                "$IttJarok=\n";
    }
    bkmFajlStOriginal = bkmFileSt;


    //iniFajl kezelése, ha nincs meghatározva *.bkm-ben:
    if (iniFileName == "") {
        QString userName = qgetenv("USER");
        if (userName.isEmpty())
            userName = qgetenv("USERNAME");
        iniFileName = exePath + "../config/" + exeName + "_" + userName + ".ini";
    }

    QString iniFileTemplateName = exePath + "../config/SaVLC.ini.template";

    iniFileSt = fileToQString(iniFileTemplateName);

    QString iniFileSt2 = fileToQString(iniFileName);
    QStringList iniFileStL = iniFileSt.split("\n");
    for (int i=0; i < iniFileStL.length(); i++) {
        if (iniFileStL[i].startsWith("$")) {
            int pos1 = iniFileStL[i].indexOf("=");
            QString parameter = iniFileStL[i].mid(1, pos1-1);
            QString value =getParameter(&iniFileSt2, parameter);
            if (value != "") {
                setParameter(&iniFileSt, parameter, value);
            }
        }
    }
    qStToFile(iniFileName, iniFileSt);

    ujFeliratHossz = getParameter(&iniFileSt, "UjFeliratHossz").toDouble();

//    qWarning("iniFileName = " + iniFileName.toUtf8());
    iniBeolvasB();
    qWarning("utolsoPar: " + utolsoPar.toUtf8());
    qWarning("iniFileName: " + iniFileName.toUtf8());
    qWarning("videoFile: " + videoFile.toUtf8());
    qWarning("bkmFajl: " + bkmFileName.toUtf8());
}

void iniBeolvasB() {
    //bill. beállítások:
    //BILL_autoFeliratPauseTg = BillKodFv(getParameter(&iniFajlSt, "BILL_autoFeliratPauseTg"));

    BILL_kilepes = BillKodFv(getParameter(&iniFileSt, "BILL_kilepes"));
    BILL_start_stop = BillKodFv(getParameter(&iniFileSt, "BILL_start_stop"));
    BILL_start_stop2 = BillKodFv(getParameter(&iniFileSt, "BILL_start_stop2"));
    BILL_rovid_elore = BillKodFv(getParameter(&iniFileSt, "BILL_rovid_elore"));
    BILL_rovid_hatra = BillKodFv(getParameter(&iniFileSt, "BILL_rovid_hatra"));
    BILL_kozepes_elore = BillKodFv(getParameter(&iniFileSt, "BILL_kozepes_elore"));
    BILL_kozepes_hatra = BillKodFv(getParameter(&iniFileSt, "BILL_kozepes_hatra"));
    BILL_hosszu_elore = BillKodFv(getParameter(&iniFileSt, "BILL_hosszu_elore"));
    BILL_hosszu_hatra = BillKodFv(getParameter(&iniFileSt, "BILL_hosszu_hatra"));
    BILL_hangero_fel = BillKodFv(getParameter(&iniFileSt, "BILL_hangero_fel"));
    BILL_hangero_le = BillKodFv(getParameter(&iniFileSt, "BILL_hangero_le"));
    BILL_nagy_hangero_fel = BillKodFv(getParameter(&iniFileSt, "BILL_nagy_hangero_fel"));
    BILL_nagy_hangero_le = BillKodFv(getParameter(&iniFileSt, "BILL_nagy_hangero_le"));
    BILL_tempo_gyorsabb = BillKodFv(getParameter(&iniFileSt, "BILL_tempo_gyorsabb"));
    BILL_tempo_normal = BillKodFv(getParameter(&iniFileSt, "BILL_tempo_normal"));
    BILL_tempo_lassabb = BillKodFv(getParameter(&iniFileSt, "BILL_tempo_lassabb"));
    BILL_tempo_lassabb2 = BillKodFv(getParameter(&iniFileSt, "BILL_tempo_lassabb2"));
    BILL_kijelzes = BillKodFv(getParameter(&iniFileSt, "BILL_kijelzes"));
    BILL_ABszakasz = BillKodFv(getParameter(&iniFileSt, "BILL_ABszakasz"));
    BILL_ABszakasz2 = BillKodFv(getParameter(&iniFileSt, "BILL_ABszakasz2"));
    BILL_ABszakasz3 = BillKodFv(getParameter(&iniFileSt, "BILL_ABszakasz3"));
    BILL_ABszakasz4 = BillKodFv(getParameter(&iniFileSt, "BILL_ABszakasz4"));
    BILL_ABszakasz5 = BillKodFv(getParameter(&iniFileSt, "BILL_ABszakasz5"));
    BILL_bkmToggle = BillKodFv(getParameter(&iniFileSt, "BILL_bkmToggle"));
    BILL_bkmCommandToggle = BillKodFv(getParameter(&iniFileSt, "BILL_bkmCommandToggle"));
    BILL_bkm0 = BillKodFv(getParameter(&iniFileSt, "BILL_bkm0"));
    BILL_bkmKov = BillKodFv(getParameter(&iniFileSt, "BILL_bkmKov"));
    BILL_bkmElozo = BillKodFv(getParameter(&iniFileSt, "BILL_bkmElozo"));
    BILL_SaTextEditToggle0 = BillKodFv(getParameter(&iniFileSt, "BILL_SaTextEditToggle0"));
    BILL_SaTextEditToggle1 = BillKodFv(getParameter(&iniFileSt, "BILL_SaTextEditToggle1"));
    BILL_SaTextEditToggle2 = BillKodFv(getParameter(&iniFileSt, "BILL_SaTextEditToggle2"));
    BILL_SaTextEditToggle3 = BillKodFv(getParameter(&iniFileSt, "BILL_SaTextEditToggle3"));
    BILL_SaTextEditToggle4 = BillKodFv(getParameter(&iniFileSt, "BILL_SaTextEditToggle4"));
    BILL_SaTextEditToggle5 = BillKodFv(getParameter(&iniFileSt, "BILL_SaTextEditToggle5"));
    BILL_SaTextEditToggle6 = BillKodFv(getParameter(&iniFileSt, "BILL_SaTextEditToggle6"));
    BILL_SaTextEditToggle6B = BillKodFv(getParameter(&iniFileSt, "BILL_SaTextEditToggle6B"));
    BILL_feliratToggle = BillKodFv(getParameter(&iniFileSt, "BILL_feliratToggle"));
    BILL_felirat0Valt = BillKodFv(getParameter(&iniFileSt, "BILL_felirat0Valt"));
    BILL_felirat1Valt = BillKodFv(getParameter(&iniFileSt, "BILL_felirat1Valt"));
    BILL_AblakElrendezesValtas = BillKodFv(getParameter(&iniFileSt, "BILL_AblakElrendezesValtas"));
    BILL_egyKockaElore = BillKodFv(getParameter(&iniFileSt, "BILL_egyKockaElore"));
    BILL_TeljesKepernyoTg = BillKodFv(getParameter(&iniFileSt, "BILL_TeljesKepernyoTg"));
    BILL_IttJarok = BillKodFv(getParameter(&iniFileSt, "BILL_IttJarok"));
    BILL_AblakmozgatasTg = BillKodFv(getParameter(&iniFileSt, "BILL_AblakmozgatasTg"));
    BILL_PotiToggle = BillKodFv(getParameter(&iniFileSt, "BILL_PotiToggle"));
    BILL_feliratPauseTg = BillKodFv(getParameter(&iniFileSt, "BILL_feliratPauseTg"));
    BILL_autoFeliratPauseTg = BillKodFv(getParameter(&iniFileSt, "BILL_autoFeliratPauseTg"));
    BILL_OCRTesztPadTg = BillKodFv(getParameter(&iniFileSt, "BILL_OCRTesztPadToggle"));
    BILL_cedictAblakTg = BillKodFv(getParameter(&iniFileSt, "BILL_cedictAblakTg"));
    BILL_editAblakValtas = BillKodFv(getParameter(&iniFileSt, "BILL_editAblakValtas"));
    BILL_trigger = BillKodFv(getParameter(&iniFileSt, "BILL_trigger"));
    BILL_fajlMegnyitas = BillKodFv(getParameter(&iniFileSt, "BILL_fajlMegnyitas"));
    //qWarning("BILL_trigger = %d", BILL_trigger);

    BILL_TgAlap = BillKodFv(getParameter(&iniFileSt, "BILL_TgAlap"));
    BILL_TgFeliratKesleltetes = BillKodFv(getParameter(&iniFileSt, "BILL_TgFeliratKesleltetes"));
    BILL_TgHangKesleltetes = BillKodFv(getParameter(&iniFileSt, "BILL_TgHangKesleltetes"));
    BILL_TgVilagossag = BillKodFv(getParameter(&iniFileSt, "BILL_TgVilagossag"));
    BILL_TgTelitettseg = BillKodFv(getParameter(&iniFileSt, "BILL_TgTelitettseg"));
    BILL_TgElesseg = BillKodFv(getParameter(&iniFileSt, "BILL_TgElesseg"));
    BILL_TgGamma = BillKodFv(getParameter(&iniFileSt, "BILL_TgGamma"));
    BILL_TgSzinHo = BillKodFv(getParameter(&iniFileSt, "BILL_TgSzinHo"));
    BILL_TgKeparany = BillKodFv(getParameter(&iniFileSt, "BILL_TgKeparany"));
    BILL_feliratBetolt = BillKodFv(getParameter(&iniFileSt, "BILL_feliratBetolt"));
    BILL_UjFeliratMentes = BillKodFv(getParameter(&iniFileSt, "BILL_UjFeliratMentes"));
    BILL_FeliratKezdopont = BillKodFv(getParameter(&iniFileSt, "BILL_FeliratKezdopont"));
    BILL_FeliratVegpont = BillKodFv(getParameter(&iniFileSt, "BILL_FeliratVegpont"));
    BILL_FeliratVegpont2 = BillKodFv(getParameter(&iniFileSt, "BILL_FeliratVegpont2"));
    BILL_FeliratAppend = BillKodFv(getParameter(&iniFileSt, "BILL_FeliratAppend"));
    BILL_FeliratKezdopont2 = BillKodFv(getParameter(&iniFileSt, "BILL_FeliratKezdopont2"));
    BILL_FeliratKezdopont3 = BillKodFv(getParameter(&iniFileSt, "BILL_FeliratKezdopont3"));
    BILL_SzerkesztesBeMentes = BillKodFv(getParameter(&iniFileSt, "BILL_SzerkesztesBeMentes"));
    BILL_kijelolesVissza = BillKodFv(getParameter(&iniFileSt, "BILL_kijelolesVissza"));
    BILL_kijelolesElore = BillKodFv(getParameter(&iniFileSt, "BILL_kijelolesElore"));
    BILL_kijelolesKiterjeszt = BillKodFv(getParameter(&iniFileSt, "BILL_kijelolesKiterjeszt"));
    BILL_kijelolesUjSel = BillKodFv(getParameter(&iniFileSt, "BILL_kijelolesUjSel"));
    BILL_kijelolesUjSelSzepaTg = BillKodFv(getParameter(&iniFileSt, "BILL_kijelolesUjSelSzepaTg"));
    BILL_snapshot = BillKodFv(getParameter(&iniFileSt, "BILL_snapshot"));

    BILL_kijelolesTorol = BillKodFv(getParameter(&iniFileSt, "BILL_kijelolesTorol"));

    vilagossag = getParameter(&iniFileSt, "Vilagossag").toFloat();
    elesseg = getParameter(&iniFileSt, "Elesseg").toFloat();
    telitettseg = getParameter(&iniFileSt, "Telitettseg").toFloat();
    gamma = getParameter(&iniFileSt, "Gamma").toFloat();
    szinHo = getParameter(&iniFileSt, "szinHo").toInt();
    hangKesleltetes = getParameter(&iniFileSt, "Hangkesleltetes").toFloat();
    feliratKesleltetes = getParameter(&iniFileSt, "Feliratkesleltetes").toFloat();
    aspectRatioIdx = getParameter(&iniFileSt, "KeparanyIdx").toInt();

    bkmCommandOn = getParameter(&iniFileSt, "bkmCommandOn") == "0" ? false : true ;
    hangEro = getParameter(&iniFileSt, "Hangero").toInt();
    cedictDict = getParameter(&iniFileSt, "cedictDict");
    xms_millisec = getParameter(&iniFileSt, "xms_millisec").toInt();
    xms_millisec2 = getParameter(&iniFileSt, "xms_millisec2").toInt();
    Ugras_rovid = getParameter(&iniFileSt, "Ugras_rovid").toInt();
    Ugras_kozepes = getParameter(&iniFileSt, "Ugras_kozepes").toInt();
    Ugras_hosszu = getParameter(&iniFileSt, "Ugras_hosszu").toInt();
    SaTextEdit5BCArany = getParameter(&iniFileSt, "SaTextEdit5BCArany").toDouble();
    autoFeliratPause = getParameter(&iniFileSt, "autoFeliratPause").toInt();
    SaMainWindow::cedictSzotar = getParameter(&iniFileSt, "cedictSzotar");
    min_B_A = getParameter(&iniFileSt, "min_B-A").toDouble();

    w->tesztPad->ui->pot1->setValue(getParameter(&iniFileSt, "ocr_pot1").toInt());
    w->tesztPad->ui->pot2->setValue(getParameter(&iniFileSt, "ocr_pot2").toInt());
    w->tesztPad->ui->pot3->setValue(getParameter(&iniFileSt, "ocr_pot3").toInt());
    w->tesztPad->ui->pot4->setValue(getParameter(&iniFileSt, "ocr_pot4").toInt());
    w->tesztPad->ui->pot5->setValue(getParameter(&iniFileSt, "ocr_pot5").toInt());
    w->tesztPad->ui->pot6->setValue(getParameter(&iniFileSt, "ocr_pot6").toInt());
    w->tesztPad->ui->pot7->setValue(getParameter(&iniFileSt, "ocr_pot7").toInt());
    w->tesztPad->ui->pot8->setValue(getParameter(&iniFileSt, "ocr_pot8").toInt());
    w->tesztPad->ui->pot9->setValue(getParameter(&iniFileSt, "ocr_pot9").toInt());
    w->tesztPad->ui->pot10->setValue(getParameter(&iniFileSt, "ocr_pot10").toInt());
    w->tesztPad->ui->checkBox->setChecked(false);
    w->ocr->tessdataSt = getParameter(&iniFileSt, "tessdata");

    aktualisAblakElrendezes = getParameter(&iniFileSt, "AktAblakElrendezes").toInt();

    if (bkmFileName == "") {
        ittJarokPos = getIttJarokTimeFromIni();
    } else {
        ittJarokPos = idoStToDouble(getParameter(&bkmFileSt, "IttJarok"));
    }

    startDelay = getParameter(&iniFileSt, "StartDelay").toInt();
}

int BillKodFv(QString billSt) {
    //billMap alapján billentyűkódot ad vissza, vagy ha nincs, akkor -1 -et
    QStringList stL = billSt.split("+");
    int eR = 0;
    int a;
    for(int i=0; i<stL.size(); i++) {
        //qWarning("BillKodFv: stL[%d] = " + stL[i].toUtf8() + ", billSt = " + billSt.toUtf8(), i);
        a = billMap->value(stL[i], -1);
        if (a == -1) {
            eR = -1;
            break;
        } else {
            eR += a;
        }
    }
    return eR;
}

void billKezel(QKeyEvent *e) {
//    qWarning("billKezel: e->modifiers() = 0x%08x, e->key() = 0x%08x (" + e->text().toLatin1() +
//            "), BILL_autoFeliratPauseTg: %d, app->queryKeyboardModifiers() = 0x%08x",
//            (int) e->modifiers()&0xffffffff, e->key(),
//            BILL_autoFeliratPauseTg,
//            (int) app->queryKeyboardModifiers()&0xffffffff);

    int eModifiers = (int) e->modifiers()&0xffffffff;    //azért, hogy Sogou-val is működjön a a lassítás
    int eKey = e->key();                                 //azért, hogy Sogou-val is működjön a a lassítás
    long bill = 0;
    if (!foglalt) {
        foglalt = true;
        msgBox->hide();     //pl. bkm törlés/hozáadás kiírás elrejtése

        /* //azért, hogy pl. a nyilak működjenek Hotkey -el */
        if (e->matches(QKeySequence::Copy) || e->matches(QKeySequence::SelectAll)) {
            if (imgSzerkesztes->isVisible() && w->utolsoSzerkesztesFokusz) {
                w->utolsoSzerkesztesFokusz->keyPressEvent(e);
            } else {
                w->utolsoKijelolesFokusz->keyPressEvent(e);
            }
        } else if (e->text() == QString::fromUtf8("÷") && billBuffer == "") {
            //this method is needed, because AHK does not send Numpad keys propery
            billBuffer = QString::fromUtf8("÷");
        } else if (billBuffer == QString::fromUtf8("÷")) {
            billBuffer = e->text().toLower();
            if (billBuffer == "0") {
                bill = BILL_nagy_hangero_le;
            } else if (billBuffer == "1") {
                 bill = BILL_nagy_hangero_fel;
            } else if (billBuffer == "2") {
                 bill = BILL_ABszakasz4;
            } else if (billBuffer == "3") {
                 bill = BILL_rovid_hatra;
            } else if (billBuffer == "4") {
                 bill = BILL_start_stop;
            } else if (billBuffer == "5") {
                 bill = BILL_rovid_elore;
            } else if (billBuffer == "6") {
                 bill = BILL_ABszakasz5;
            } else if (billBuffer == "7") {
                 bill = BILL_tempo_gyorsabb;
            } else if (billBuffer == "8") {
                 bill = BILL_tempo_normal;
            } else if (billBuffer == "9") {
                 bill = BILL_ABszakasz2;
            } else if (billBuffer == "a") {
                 bill = BILL_ABszakasz;
            } else if (billBuffer == "b") {
                 bill = BILL_tempo_lassabb;
            } else if (billBuffer == "c") {
                 bill = BILL_ABszakasz3;
            } else if (billBuffer == "d") {
                 bill = BILL_kijelolesTorol;
            } else if (billBuffer == "e") {
                bill = BILL_hangero_le;
            } else if (billBuffer == "f") {
                bill = BILL_hangero_fel;
            }
            billBuffer = "";
        } else {                    //nem ÷
            billBuffer = "";
            bill = e->modifiers() + e->key();
        }

        if (bkmCurrentCommand != ""
                && (bill == BILL_start_stop || bill == BILL_start_stop2
                    || bill == BILL_ABszakasz || bill == BILL_ABszakasz2
                    || bill == BILL_ABszakasz3 || bill == BILL_ABszakasz4 || bill == BILL_ABszakasz5
                    || bill == BILL_bkm0 || bill == BILL_bkmCommandToggle || bill == BILL_bkmElozo
                    || bill == BILL_bkmKov || bill == BILL_bkmToggle
                    || bill == BILL_fajlMegnyitas || bill == BILL_kilepes
                    || bill == BILL_egyKockaElore || bill == BILL_rovid_elore || bill == BILL_rovid_hatra
                    || bill == BILL_kozepes_elore || bill == BILL_kozepes_hatra || bill == BILL_hosszu_elore || bill == BILL_hosszu_hatra)) {
            bkmClearCommands();
            w->player->pause();
            bill = -1;
        }

        double incr;
        if (bill == BILL_kilepes) {
            w->close();
        } else if (bill == BILL_trigger) {
            //qWarning("BILL_trigger");
            if (billTriggerSt == "") {
                billTriggerSt = TRIGST;
                //qWarning("billTriggerSt = " + billTriggerSt.toUtf8());
            } else {
                billTriggerSt = billTriggerSt.mid(1,2) + ":" +
                        billTriggerSt.mid(3,2) + ":" +
                        billTriggerSt.mid(5,2) + "," +
                        billTriggerSt.mid(7,3);
                //qWarning("billTriggerSt = " + billTriggerSt.toUtf8());
                double pos = idoStToDouble(billTriggerSt);
                billTriggerSt = "";
                //qWarning("pos = %f", pos);
                tA = -1.0;
                tB = -1.0;
                feliratPause = 0;   //AB szakasz megszűnésekor mindig megszüntetjük a felírat rögzítést
                if (pos > 0.0) {
                    w->changePos(pos);
                }
                if (pauseMod != 0) {
                    w->player->tgPlayPause();
                }
            }
        } else if (billTriggerSt.left(1) == TRIGST) {
            billTriggerSt = billTriggerSt + e->text();
            //qWarning("billTriggerSt = " + billTriggerSt.toUtf8());
        } else if (bill == BILL_start_stop) {
            if (Tg == AlapTg) {
                w->player->tgPlayPause();
                bkmForceNew = bkmForceNewFn();
            } else if (Tg == FeliratKesleltetesTg) {
                feliratKesleltetes = 0.0;
            } else if (Tg == HangKesleltetesTg) {
                hangKesleltetes = 0.0;
                w->player->setHangKesleltetes(hangKesleltetes);
            } else if (Tg == VilagossagTg) {
                vilagossag = 1.0;
                w->player->setVilagossag(vilagossag);
            } else if (Tg == ElessegTg) {
                elesseg = 1.0;
                w->player->setElesseg(elesseg);
            } else if (Tg == TelitettsegTg) {
                if (telitettseg == 1.0) {
                    telitettseg = 0.0;
                } else {
                    telitettseg = 1.0;
                }
                w->player->setTelitettseg(telitettseg);
            } else if (Tg == GammaTg) {
                gamma = 1.0;
                w->player->setGamma(gamma);
            } else if (Tg == SzinHoTg) {
                if (szinHo == 0) {
                    szinHo = 180;
                } else {
                    szinHo = 0;
                }
                w->player->setSzinHo(szinHo);
            } else if (Tg == KeparanyTg) {
                aspectRatioIdx = 0;
                w->player->setAspectRatio();
            }
        } else if (bill == BILL_start_stop2) {
            alapMod();
            w->player->tgPlayPause();
            bkmForceNew = bkmForceNewFn();
        } else if (bill == BILL_hangero_fel) {
            alapMod();
            hangEro = qMin( hangEro + lepesek.split("|")[7].toInt(), 254);
            w->player->changeVolume(hangEro);
        } else if (bill == BILL_hangero_le) {
            alapMod();
            hangEro = qMax( hangEro - lepesek.split("|")[7].toInt(), 0);
            w->player->changeVolume(hangEro);
        } else if (bill == BILL_nagy_hangero_fel) {
            alapMod();
            hangEro = qMin( hangEro + lepesek.split("|")[8].toInt(), 254);
            w->player->changeVolume(hangEro);
        } else if (bill == BILL_nagy_hangero_le) {
            alapMod();
            hangEro = qMax( hangEro - lepesek.split("|")[8].toInt(), 0);
            w->player->changeVolume(hangEro);
        } else if (bill == BILL_egyKockaElore) {
            if (imgSzerkesztes && imgSzerkesztes->isHidden()) {
                alapMod();
                w->player->setNextFrame();
                mesterIdo = w->player->getPos();
            }
        } else if (bill == BILL_feliratPauseTg) {
            alapMod();
            if (feliratPause == 0) {
                feliratPauseSetting(false);
            } else {
                feliratPause = 0;
            }
        } else if (bill == BILL_autoFeliratPauseTg) {
            alapMod();
            autoFeliratPause = autoFeliratPause == 0 ? 1 : 0;
        } else if (bill == BILL_OCRTesztPadTg) {
            alapMod();
            if (w->tesztPad->isVisible()) {
                w->tesztPad->hide();
            } else {
                w->tesztPad->show();
                w->tesztPad->raise();
                w->tesztPad->setFocus();
                w->ocr->optimalizal();
            }
        } else if (bill == BILL_cedictAblakTg) {
            w->cedictAblakState = w->cedictAblakState > 0 ? 0 : 1;
            w->teL[7]->setLathato(w->cedictAblakState > 0);
        } else if (bill == BILL_editAblakValtas) {
            if (imgSzerkesztes->isVisible()) {
                int idx = 0;
                if (w->utolsoSzerkesztesFokusz != NULL) {
                    idx = w->utolsoSzerkesztesFokusz->idx;
                }
                do {
                    idx++;
                    if (idx > 4) {
                        idx = 0;
                    }
                } while (!w->teL[idx]->getLathato());
                w->utolsoSzerkesztesFokusz = w->teL[idx];
                w->utolsoKijelolesFokusz = w->teL[idx];
                w->teL[idx]->setFocus();
                w->teL[idx]->setReadOnly(false);
            }
        } else if (bill == BILL_TeljesKepernyoTg) {
            w->player->tgFullscreen();
            alapMod();
        } else if (bill == BILL_fajlMegnyitas) {
            alapMod();
            argL.removeLast();
            fajlmegnyitasElokeszites(false);
        } else if (bill == BILL_ABszakasz) {
            alapMod();
            setABszakasz();
        } else if (bill == BILL_ABszakasz2) {
            alapMod();
            setABszakasz2();
        } else if (bill == BILL_ABszakasz3) {
            alapMod();
            setABszakasz3();
        } else if (bill == BILL_ABszakasz4) {
            alapMod();
            setABszakasz4();
        } else if (bill == BILL_ABszakasz5) {
            alapMod();
            setABszakasz5();
        } else if (bill == BILL_bkmToggle) {
            alapMod();
            billEvent(10);
        } else if (bill == BILL_bkmCommandToggle) {
            alapMod();
            billEvent(17);
        } else if (bill == BILL_bkm0) {
            alapMod();
            if (bkmN > 0) {
                bkmIdx = 0;
                w->changePos(bkmTimeL[bkmIdx]);
                bkmForceNew = true;
            }
        } else if (bill == BILL_bkmElozo) {
            if (bkmN > 0) {
                alapMod();
                if (bkmIdx == -1) {
                    bkmIdx = bkmN - 1;
                } else if (mesterIdo > bkmTimeL[bkmIdx] + 1.0) {
                } else {
                    if (bkmIdx > 0) {
                        bkmIdx = bkmIdx - 1;
                    } else {
                        bkmIdx = bkmN - 1;
                    }
                }
                w->changePos(bkmTimeL[bkmIdx]);
                bkmForceNew = true;
            }
        } else if (bill == BILL_bkmKov) {
            if (bkmN > 0){
                alapMod();
                if (bkmIdx == -1) {
                    bkmIdx = 0;
                } else {
                    if (bkmIdx < bkmN-1) {
                        bkmIdx = bkmIdx + 1;
                    } else {
                        bkmIdx = 0;
                    }
                }
                w->changePos(bkmTimeL[bkmIdx]);
                bkmForceNew = true;
            }
        } else if (bill == BILL_felirat0Valt) {
            if (Tg == AlapTg) {
                billEvent(-2);  //0. felirat váltás
            } else {
                alapMod();
            }
        } else if (bill == BILL_felirat1Valt) {
            alapMod();
            billEvent(-3);  //1. felirat váltás
        } else if (bill == BILL_feliratBetolt) {
            alapMod();
            billEvent(-5);  //Felirat betöltés
        } else if (bill == BILL_feliratToggle) {
            if (Tg == AlapTg) {
                alapMod();
                billEvent(-1);  //toggle globalVisible
            } else {
                Tg = static_cast<TgE>((Tg + 1) % (SzinHoTg + 1));
                if (Tg == AlapTg) {
                    Tg = static_cast<TgE>(Tg + 1);
                }
            }
        } else if (bill == BILL_SaTextEditToggle0) {
            alapMod();
            billEvent(0);
        } else if (bill == BILL_SaTextEditToggle1) {
            alapMod();
            billEvent(1);
        } else if (bill == BILL_SaTextEditToggle2) {
            alapMod();
            billEvent(2);
        } else if (bill == BILL_SaTextEditToggle3) {
            alapMod();
            billEvent(3);
        } else if (bill == BILL_SaTextEditToggle4) {
            alapMod();
            billEvent(4);
        } else if (bill == BILL_SaTextEditToggle5) {  //idő
            alapMod();
            billEvent(5);
        } else if (bill == BILL_SaTextEditToggle6) {  //Súgó
            alapMod();
            billEvent(9);
        } else if (bill == BILL_SaTextEditToggle6B) {  //Súgó config fájllal és bkm-kal
            alapMod();
            billEvent(6);
        } else if (bill == BILL_PotiToggle) {  //Poti
            alapMod();
            billEvent(8);
        } else if (bill == BILL_UjFeliratMentes) {  //Új felirat mentés (amikor OCR aktív)
            alapMod();
            saveSubtitle(1);
        } else if (bill == BILL_FeliratKezdopont) {  //Felirat kezdőpont
            alapMod();
            saveSubtitle(2);
        } else if (bill == BILL_FeliratVegpont) {  //Felirat végpont
            alapMod();
            saveSubtitle(3);
        } else if (bill == BILL_FeliratVegpont2) {  //Előző felirat végét az aktuális elejéhez igazítja
            alapMod();
            saveSubtitle(4);
        } else if (bill == BILL_FeliratAppend) {  //OCR hozzáfűzése az aktuális felirathoz
            alapMod();
            saveSubtitle(5);
        } else if (bill == BILL_FeliratKezdopont2) {  //Felirat kezdőpont xms millisec-kel korábbra
            alapMod();
            saveSubtitle(6);
        } else if (bill == BILL_FeliratKezdopont3) {  //Felirat kezdőpont xms millisec-kel későbbre
            alapMod();
            saveSubtitle(7);
        } else if (bill == BILL_kijelolesVissza) {
            isOCRmaradjonLathato = true;
            alapMod();
            isOCRmaradjonLathato = false;
            billEvent(12);
        } else if (bill == BILL_kijelolesElore) {
            isOCRmaradjonLathato = true;
            alapMod();
            isOCRmaradjonLathato = false;
            billEvent(13);
        } else if (bill == BILL_kijelolesKiterjeszt) {
            isOCRmaradjonLathato = true;
            alapMod();
            isOCRmaradjonLathato = false;
            billEvent(14);
        } else if (bill == BILL_kijelolesUjSel) {
            isOCRmaradjonLathato = true;
            alapMod();
            isOCRmaradjonLathato = false;
            billEvent(15);
        } else if (bill == BILL_kijelolesUjSelSzepaTg) {
            w->isKijelolesUjSelSepa = !w->isKijelolesUjSelSepa;
            w->mySelectionChanged(w->utolsoKijelolesFokusz);
        } else if (bill == BILL_snapshot) {
            //Successful only if normal characters are there in the path+filename
            QString st = openedFilePath + openedFileName + ".png";
            w->player->takeSnapshot(st);
            showMsgBox(w->satr("Pillanatkép: ") + st,
                    QColor(100, 100, 200), 5000);
        } else if (bill == BILL_kijelolesTorol) {
            bool w7 =w->teL[7]->getLathato();
            bool w9 =w->teL[9]->getLathato();
            isOCRmaradjonLathato = true;
            alapMod();
            isOCRmaradjonLathato = false;
            billEvent(16);
            if (w->teL[9]->toPlainText().length() > 0) {
                if (!w7 && !w9) {
                    w->teL[7]->setLathato(true);
                    w->teL[9]->setLathato(true);
                    w->mySelectionChanged(w->teL[9]);
                    w->player->setFocus();
                } else if (w7 && w9) {
                    w->teL[7]->setLathato(false);
                } else {
                    w->teL[7]->setLathato(false);
                    w->teL[9]->setLathato(false);
                }
            } else {
                w->teL[7]->setLathato(!w->teL[7]->getLathato());
            }
        } else if (bill == BILL_SzerkesztesBeMentes) {  //Felirat szerkesztés be/mentés:
            alapMod();
            billEvent(11);
        } else if (bill == BILL_AblakElrendezesValtas) {  //Ablak elrendezés váltás
            alapMod();
            billEvent(7);
        } else if (bill == BILL_tempo_gyorsabb) {
            if (sebessegIdx < sebessegek.split("|").size() - 1) {
                ++sebessegIdx;
            } else {
                sebessegIdx = 0;
            }
            alapMod();
            w->player->setSebesseg(sebessegek.split("|")[sebessegIdx].toDouble());
        } else if (bill == BILL_tempo_lassabb || bill == BILL_tempo_lassabb2 ||
                   (eModifiers == 0x20000000 && eKey == 0x0000002e)) {
            if (0 < sebessegIdx) {
                --sebessegIdx;
            } else {
                sebessegIdx = sebessegek.split("|").size() - 1;
            }
            alapMod();
            w->player->setSebesseg(sebessegek.split("|")[sebessegIdx].toDouble());
        } else if (bill == BILL_tempo_normal) {
            alapMod();
            sebessegIdx = sebessegek.split("|").indexOf("1");
            w->player->setSebesseg(sebessegek.split("|")[sebessegIdx].toDouble());
        } else if (bill == BILL_TgAlap) {
            if (ablakMozgatas) {
                ablakMozgatasTg();
            }
            if (imgSzerkesztes->isVisible()) {
                w->feliratMentKilep();
            }
            w->teL[6]->setLathato(false);
            w->teL[7]->setLathato(false);
            w->teL[8]->setLathato(false);
            isOCRmaradjonLathato = false;
            w->setFocus();
            alapMod();
        } else if (bill == BILL_TgFeliratKesleltetes) {
            if (Tg == FeliratKesleltetesTg) {
                alapMod();
            } else {
                Tg = FeliratKesleltetesTg;
            }
        } else if (bill == BILL_TgHangKesleltetes) {
            if (Tg == HangKesleltetesTg) {
                alapMod();
            } else {
                Tg = HangKesleltetesTg;
            }
        } else if (bill == BILL_TgVilagossag) {
            if (Tg == VilagossagTg) {
                alapMod();
            } else {
                Tg = VilagossagTg;
            }
        } else if (bill == BILL_TgElesseg) {
            if (Tg == ElessegTg) {
                alapMod();
            } else {
                Tg = ElessegTg;
            }
        } else if (bill == BILL_TgTelitettseg) {
            if (Tg == TelitettsegTg) {
                alapMod();
            } else {
                Tg = TelitettsegTg;
            }
        } else if (bill == BILL_TgGamma) {
            if (Tg == GammaTg) {
                alapMod();
            } else {
                Tg = GammaTg;
            }
        } else if (bill == BILL_TgSzinHo) {
            if (Tg == SzinHoTg) {
                alapMod();
            } else {
                Tg = SzinHoTg;
            }
        } else if (bill == BILL_TgKeparany) {
            if (Tg == KeparanyTg) {
                alapMod();
            } else {
                Tg = KeparanyTg;
            }
        } else if (bill == BILL_egyKockaElore) {  // Step to next frame
            alapMod();
            w->player->setNextFrame();
        } else if (bill == BILL_IttJarok) {
            alapMod();
            if (ittJarokPos < 0.0) { // -1.0 -> time
                billEvent(-6);
            } else {                 // time -> -1.0
                incr = ittJarokPos-mesterIdo;
                billEvent(-6);
                goto do_seek;
            }
        } else if (bill == BILL_rovid_hatra) {
            if (Tg == AlapTg) {
                alapMod();
                incr = -Ugras_rovid;
                goto do_seek;
            } else if (Tg == FeliratKesleltetesTg) {
                feliratKesleltetes = qMax(-100.0, feliratKesleltetes - lepesek.split("|")[0].toDouble());
                if (qAbs(feliratKesleltetes-0.0)<0.001) {    //"-0.0" helyett "0.0" legyen
                    feliratKesleltetes = 0.0;
                }
            } else if (Tg == HangKesleltetesTg) {
                hangKesleltetes = qMax(-100.0, hangKesleltetes - lepesek.split("|")[1].toDouble());
                if (qAbs(hangKesleltetes-0.0)<0.001) {    //"-0.0" helyett "0.0" legyen
                    hangKesleltetes = 0.0;
                }
                w->player->setHangKesleltetes(hangKesleltetes);
            } else if (Tg == VilagossagTg) {
                vilagossag = qMax(0.0, vilagossag - lepesek.split("|")[2].toDouble());
                if (qAbs(vilagossag-0.0)<0.001) {    //"-0.0" helyett "0.0" legyen
                    vilagossag = 0.0;
                }
                w->player->setVilagossag(vilagossag);
            } else if (Tg == ElessegTg) {
                elesseg = qMax(-10.0, elesseg - lepesek.split("|")[3].toDouble());
                if (qAbs(elesseg-0.0)<0.001) {    //"-0.0" helyett "0.0" legyen
                    elesseg = 0.0;
                }
                w->player->setElesseg(elesseg);
            } else if (Tg == TelitettsegTg) {
                telitettseg = qMax(-20.0, telitettseg - lepesek.split("|")[4].toDouble());
                if (qAbs(telitettseg-0.0)<0.001) {    //"-0.0" helyett "0.0" legyen
                    telitettseg = 0.0;
                }
                w->player->setTelitettseg(telitettseg);
            } else if (Tg == GammaTg) {
                gamma = qMax(0.1, gamma - lepesek.split("|")[5].toDouble());
                if (qAbs(gamma-0.0)<0.001) {    //"-0.0" helyett "0.0" legyen
                    gamma = 0.0;
                }
                w->player->setGamma(gamma);
            } else if (Tg == SzinHoTg) {
                szinHo -= lepesek.split("|")[6].toInt();
                if (szinHo<0) {
                    szinHo += 360;
                }
                w->player->setSzinHo(szinHo);
            } else if (Tg == KeparanyTg) {
                aspectRatioIdx--;
                w->player->setAspectRatio();
            }
        } else if (bill == BILL_rovid_elore) {
            if (Tg == AlapTg) {
                incr = Ugras_rovid;
                goto do_seek;
            } else if (Tg == FeliratKesleltetesTg) {
                feliratKesleltetes = qMin(100.0, feliratKesleltetes + lepesek.split("|")[0].toDouble());
                if (qAbs(feliratKesleltetes-0.0)<0.001) {    //"-0.0" helyett "0.0" legyen
                    feliratKesleltetes = 0.0;
                }
            } else if (Tg == HangKesleltetesTg) {
                hangKesleltetes = qMin(100.0, hangKesleltetes + lepesek.split("|")[1].toDouble());
                if (qAbs(hangKesleltetes-0.0)<0.001) {    //"-0.0" helyett "0.0" legyen
                    hangKesleltetes = 0.0;
                }
                w->player->setHangKesleltetes(hangKesleltetes);
            } else if (Tg == VilagossagTg) {
                vilagossag = qMin(2.0, vilagossag + lepesek.split("|")[2].toDouble());
                if (qAbs(vilagossag-0.0)<0.001) {    //"-0.0" helyett "0.0" legyen
                    vilagossag = 0.0;
                }
                w->player->setVilagossag(vilagossag);
            } else if (Tg == ElessegTg) {
                elesseg = qMin(10.0, elesseg + lepesek.split("|")[3].toDouble());
                if (qAbs(elesseg-0.0)<0.001) {    //"-0.0" helyett "0.0" legyen
                    elesseg = 0.0;
                }
                w->player->setElesseg(elesseg);
            } else if (Tg == TelitettsegTg) {
                telitettseg = qMin(20.0, telitettseg + lepesek.split("|")[4].toDouble());
                if (qAbs(telitettseg-0.0)<0.001) {    //"-0.0" helyett "0.0" legyen
                    telitettseg = 0.0;
                }
                w->player->setTelitettseg(telitettseg);
            } else if (Tg == GammaTg) {
                gamma = qMin(10.0, gamma + lepesek.split("|")[5].toDouble());
                if (qAbs(gamma-0.0)<0.001) {    //"-0.0" helyett "0.0" legyen
                    gamma = 0.0;
                }
                w->player->setGamma(gamma);
            } else if (Tg == SzinHoTg) {
                szinHo += lepesek.split("|")[6].toInt();
                szinHo %= 360;
                w->player->setSzinHo(szinHo);
            } else if (Tg == KeparanyTg) {
                aspectRatioIdx++;
                w->player->setAspectRatio();
            }
        } else if (bill == BILL_kozepes_hatra) {
            alapMod();
            incr = -Ugras_kozepes;
            goto do_seek;
        } else if (bill == BILL_kozepes_elore) {
            alapMod();
            incr = Ugras_kozepes;
            goto do_seek;
        } else if (bill == BILL_hosszu_hatra) {
            alapMod();
            incr = -Ugras_hosszu;
            goto do_seek;
        } else if (bill == BILL_hosszu_elore) {
            alapMod();
            incr = Ugras_hosszu;
    do_seek:
            w->changePos(mesterIdo + incr);
        } else if (bill == BILL_AblakmozgatasTg) {
            ablakMozgatasTg();
        }
        if (bill != BILL_kilepes) {
        }
    }
    feliratKezel(false);
    foglalt = false;
}

void billEvent(int e) {
    if (e == -6) {   //Itt járok
        if (ittJarokPos < 0.0) { // -1.0 -> time
            ittJarokPos = mesterIdo;
        } else {
            ittJarokPos = -1.0;
        }
        w->saveIttJarokPos();
        iniMent(false);
    } else if (e == -5) {   //Felirat betöltés
        for (int i=0; i<N_feliratok; i++) {
            //qWarning("Felirat betöltés, i = %d", i);
            feliratok[i]->beolvas();
        }
    } else if (e == -4) {   //Viedo ablak aktiválva
        for (int i=0; i<7; i++) {
            if (!w->teL[i]->isHidden()) {
                w->teL[i]->activateWindow();
            }
        }
    } else if (e == -3) {   //1. felirat váltás
        int i = w->teL[4]->feliratIdx;
        i++;
        if (i>=N_feliratok) {
            i = 0;
        }
        w->teL[4]->feliratIdx = i;
    } else if (e == -2) {   //0. felirat váltás
        int i = w->teL[0]->feliratIdx;
        i++;
        if (i>=N_feliratok) {
            i = 0;
        }
        w->teL[0]->feliratIdx = i;
        w->teL[1]->feliratIdx = i;
        w->teL[2]->feliratIdx = i;
        w->teL[3]->feliratIdx = i;
    } else if (e == -1) {  //toggle globalVisible
        globalVisible = !globalVisible;
        for(int i=0; i<5; i++) {
            w->teL[i]->setLathato(w->teL[i]->getLathato());
        }
        w->teL[7]->setLathato(w->teL[7]->getLathato());
    } else if (e == 6) {    //Súgó config fájllal és bkm-kal
        w->teL[6]->setText2(iniFileName + ":\n\n" + iniFileSt);
        w->teL[6]->setLathato(!w->teL[6]->getLathato());
        w->teL[6]->setFocus();
    } else if (e == 7) {   //Ablak elrendezés váltás
        ablakElrendezes(aktualisAblakElrendezes+1, true);
    } else if (e == 8) {   //Poti
        w->poti->setVisible(w->poti->isHidden());
    } else if (e == 9) {   //Súgó
        QString st = "";
        st = st + "\t" + w->satr("Feliratfájlok") + ":\n";
        if (N_feliratok > 1) {
            st = st + feliratok[1]->abszolutFajlPath + "\n";
        }
        if (N_feliratok > 0) {
            st = st + feliratok[0]->abszolutFajlPath + "\n";
        }
        if (N_feliratok > 0) {
            st = st + "\n";
        } else {
            st = st + "-\n\n";
        }
        st = st + "\t" + w->satr("Könyvjelzők") + ":\n";
        if (bkmTimeL.size() == 0) {
            st = st + "-\n";
        } else {
            for (int i=0; i<bkmTimeL.size(); i++) {
                st = st + idoDoubleToSt(bkmTimeL[i]) + "\n";
            }
        }
        QString st2 = w->satr("A konfiguráció módosításához szerkezd a xx fájlt.");
        st2 = st2.replace("xx", iniFileName);
        st = st + "\n" + st2 + "\n";
        st = st + "\n\t" + w->satr("Billentyűk") + ":\n" + sugoSt;
        w->teL[8]->setText2(st);
        if (!w->teL[8]->getLathato()) {
            w->teL[8]->setLathato(true);
            w->teL[8]->setFocus();
        } else {
            w->teL[8]->setLathato(false);
            w->utolsoKijelolesFokusz->setFocus();
        }
    } else if (e == 10) {   //bkmToggle
        //qWarning("bkmToggle");
        double t = mesterIdo;
        for (int i=0; i<bkmN; i++) {
            if (qAbs(t - bkmTimeL[i]) < 1.5) {  //bkm törlés:
                QString st = w->satr("Könyvjelző:") + " " + QString::number(i+1) + "/" +
                        QString::number(bkmN) + "\n" +
                        w->satr("törlés") + ": " + idoDoubleToSt(bkmTimeL[i]);
                bkmTimeL.removeAt(i);
                bkmAllCommandsL.removeAt(i);
                bkmCommentL.removeAt(i);
                bkmN--;
                if (bkmN == 0) {
                    bkmIdx = -1;
                }
                t = -1.0;
                showMsgBox(st, QColor(200, 100, 100), 5000);
                break;
            }
        }
        if (t > 0) {            //bkm hozzáadás
            if (bkmN == 0) {
                bkmTimeL.append(t);
                bkmAllCommandsL.append("");
                bkmCommentL.append("");
                bkmIdx = 0;
            } else {
                for (int i=0; i<bkmN; i++) {
                    if (bkmTimeL[i] > t) {
                        bkmTimeL.insert(i, t);
                        bkmAllCommandsL.insert(i, "");
                        bkmCommentL.insert(i, "");
                        bkmIdx = i;
                        t = -1.0;
                        break;
                    }
                }
                if (t > 0) {
                    bkmTimeL.append(t);
                    bkmAllCommandsL.append("");
                    bkmCommentL.append("");
                    bkmIdx = bkmN;
                }
            }
            bkmN++;
            QString st = w->satr("Könyvjelző:") + " " + QString::number(bkmIdx+1) + "/" +
                        QString::number(bkmN) + "\n" + w->satr("hozzáadás") + ": " + idoDoubleToSt(mesterIdo);
            showMsgBox(st, QColor(100, 200, 100), 5000);
            if (bkmFileName == "") {
                bkmFileName = openedFilePath + openedFileName + ".bkm";
                st = openedFilePath + openedFileName + ".ini";
                if (iniFileName != st) {
                    delIttJarokTimeInIni();
                    QFile::copy(iniFileName, st);
                    iniFileName = st;
                }
                setParameter(&iniFileSt, "IttJarok", "", false);
                iniMent(false);
                if (ittJarokPos > 0.0) {
                    setParameter(&bkmFileSt, "IttJarok", idoDoubleToSt(ittJarokPos), false);
                }
            }
        }
        bkmUpdateFileSt();
    } else if (e == 11) {   //felirat szerkesztés toggle
        if (!ablakMozgatas && !w->isFullScreen()) {
            if (imgSzerkesztes->isHidden()) {           //Felirat szerkesztés be:
                w->cursorStartEndCh = QPoint(w->teL[0]->textCursor().selectionStart(),
                        w->teL[0]->textCursor().selectionEnd());
                        //azért, hogy a Cedict ablak ne tűnjön el felíratszerkesztés után
                imgSzerkesztes->raise();
                imgSzerkesztes->move(w->poti->x() + w->poti->width() - imgSzerkesztes->width()-3,
                        w->poti->y() + 3);
                imgSzerkesztes->show();
                w->setKeretStyle(SaMainWindow::szerkeszt);
                int idx = 0;
                if (w->utolsoSzerkesztesFokusz == NULL) {
                    do {
                        idx++;
                        if (idx > 4) {
                            idx = 0;
                        }
                    } while (!w->teL[idx]->isVisible() && idx != 0);
                } else {
                    idx = w->utolsoSzerkesztesFokusz->idx;
                }
                if (w->teL[idx]->isVisible()) {
                    w->utolsoSzerkesztesFokusz = w->teL[idx];
                    w->utolsoSzerkesztesFokusz->setReadOnly(false);
                    w->utolsoKijelolesFokusz = w->utolsoSzerkesztesFokusz;
                    w->kurzorBeallitas();
                }
            } else {                                    //Felirat szerkesztés mentés:
                w->kellFeliratMentes = true;
                w->feliratMentMent();
                w->kellFeliratMentes=false;
            }
        }
    } else if (e == 12) {   //BILL_kijelolesVissza
        SaTextEdit* ablak = kijelolesiAblak();
        if (ablak) {
            ablak->setFocus();
            if (ablak->selList.size() == 0) {
                QTextCursor cur = ablak->textCursor();
                ablak->selList.append(QPoint(cur.selectionStart(), cur.selectionEnd()));
            }
            int pos1 = ablak->selList.last().x();
            int pos2 = ablak->selList.last().y();
            int hossz = ablak->toPlainText().length();
            if (pos2-pos1 > 1) {
                ablak->selList.last().setX(pos2-1);
                ablak->selList.last().setY(pos2);
            } else if (pos2 == pos1) {
                if (pos1 > 0) {
                    ablak->selList.last().setX(pos1-1);
                    ablak->selList.last().setY(pos2);
                } else {
                    ablak->selList.last().setX(hossz-1);
                    ablak->selList.last().setY(hossz);
                }
            } else {
                if (pos2 > 1) {
                    ablak->selList.last().setX(pos2-2);
                    ablak->selList.last().setY(pos2-1);
                } else if (ablak->selList.size() == 1){
                    ablak->selList.last().setX(hossz-1);
                    ablak->selList.last().setY(hossz);
                }
            }
            int n = ablak->selList.size();
            if (n > 1 &&
                    ablak->selList[n-2].y() > ablak->selList[n-1].x()) {
                ablak->selList.removeLast();
                ablak->selList.last().setY(ablak->selList.last().y() + 1);
            }
            w->mySelectionChanged(ablak);
        }
    } else if (e == 13) {   //BILL_kijelolesElore
        SaTextEdit* ablak = kijelolesiAblak();
        if (ablak) {
            ablak->setFocus();
            if (ablak->selList.size() == 0) {
                QTextCursor cur = ablak->textCursor();
                ablak->selList.append(QPoint(cur.selectionStart(), cur.selectionEnd()));
            }
            int pos1 = ablak->selList.last().x();
            int pos2 = ablak->selList.last().y();
            int hossz = ablak->toPlainText().length();
            if (pos2 < hossz) {
                if (pos1 == pos2) {
                    ablak->selList.last().setX(pos1);
                    ablak->selList.last().setY(pos1+1);
                } else {
                    ablak->selList.last().setX(pos1+1);
                    ablak->selList.last().setY(pos1+2);
                }
            } else  if (ablak->selList.size() == 1) {
                ablak->selList.last().setX(0);
                ablak->selList.last().setY(1);
            }
            w->mySelectionChanged(ablak);
        }
    } else if (e == 14) {   //BILL_kijelolesKiterjeszt
        SaTextEdit* ablak = kijelolesiAblak();
        if (ablak) {
            ablak->setFocus();
            if (ablak->selList.size() == 0) {
                QTextCursor cur = ablak->textCursor();
                ablak->selList.append(QPoint(cur.selectionStart(), cur.selectionEnd()));
            }
            int pos1 = ablak->selList.last().x();
            int pos2 = ablak->selList.last().y();
//            qWarning("size: %d, pos1: %d, pos2: %d", ablak->selList.size(), pos1, pos2);
            if (ablak->selList.size() == 1 && pos1 == 0 && pos2 == 0) {
                if (ablak->toPlainText().length() == 0) {
                    w->teL[7]->setLathato(false);
                } else {
                    pos1 = 0;
                    pos2 = ablak->toPlainText().length();
                }
            } else if (pos2 == ablak->toPlainText().length()) {
                if (pos1 > 0) {
                    pos1 = 0;
                    ablak->selList.clear();
                    ablak->selList.append(QPoint(pos1, pos2));
                } else {
                    pos1 = pos2;
                    w->teL[7]->setLathato(false);
                }
            } else if (pos2 < ablak->toPlainText().length()) {
                pos2++;
            } else if (pos1 ==0 && pos2 == 0) {
            }
            ablak->selList.last().setX(pos1);
            ablak->selList.last().setY(pos2);
            w->mySelectionChanged(ablak);
        }
    } else if (e == 15) {   //BILL_kijelolesUjSel
        SaTextEdit* ablak = kijelolesiAblak();
        if (ablak) {
            ablak->setFocus();
            if (ablak->selList.size() == 0) {
                QTextCursor cur = ablak->textCursor();
                ablak->selList.append(QPoint(cur.selectionStart(), cur.selectionEnd()));
            }
            int pos1 = ablak->selList.last().x();
            int pos2 = ablak->selList.last().y();
            if (pos2 < ablak->toPlainText().length()) {
                pos1 = pos2;
                pos2 = pos2 + 1;
                ablak->selList.append(QPoint(pos1, pos2));
            }
            w->mySelectionChanged(ablak);
        }
    } else if (e == 16) {   //BILL_kijelolesTörlés
        SaTextEdit* ablak = kijelolesiAblak();
        if (ablak && ablak != w->teL[9]) {
            if (ablak->selList.count() > 0) {
                int pos1 = ablak->selList.last().y();
                ablak->selList.clear();
                ablak->selList.append(QPoint(pos1, pos1));
            }
            if (w->cedictAblakState == 1) {
                w->cedictAblakState = 2;
            }
            w->mySelectionChanged(ablak);
        }
    } else if (e == 17) {   //BILL_bkmCommandToggle
        bkmCommandOn = !bkmCommandOn;
    } else {    //Felírat ablakok és Idő ablak toggle:
        w->teL[e]->setLathato(!w->teL[e]->getLathato());
    }
}


void saveSubtitle(int par) {        //Új felirat mentés (amikor OCR aktív)::
//par=1: új felírat hozzáadása
//par=2: a felirat kezdete az aktuális időpont
//par=3: a felirat vége az aktuális időpont
//par=4: előző végét az új elejéhez igazítja
//par=5: OCR hozzáfűzése az aktuális felirathoz
//par=6: felirat kezdőpont xms millisec-kel korábbra
//par=7: felirat kezdőpont xms millisec-kel későbbre
//    qWarning("saveSubtitle_1");
    imgSzerkesztes->show();
    if (par == 1) {           //Új felirat mentés (amikor OCR aktív):
        if (w->teL[9]->toPlainText() != "") {
            double t1 = qMax(0.0, mesterIdo - xms_millisec2/1000.0);
            double t2 = qMin(videoHossz, mesterIdo + ujFeliratHossz);
            if (N_feliratok == 0) {                //új feliratfájl:
                QString subtitleFajl = QFileInfo(videoFile).completeBaseName() + "_1_chp.srt";
                QString idoSor = "";
                idoSor = idoDoubleToSt(t1) + " --> " + idoDoubleToSt(t2) + "\n";
                QString subtitleSt = "1\n" +
                        idoSor +
                        w->teL[9]->toPlainText() + "\n"
                        "x\n"
                        "x\n"
                        "\n";
                qStToFile(QFileInfo(videoFile).path() + "/" + subtitleFajl, subtitleSt, true);
                Sleeper::msleep(600);
                kezdetiBeallitasok();
                w->teL[0]->feliratIdx = 0;
                w->teL[1]->feliratIdx = 0;
                w->teL[2]->feliratIdx = 0;
                w->teL[3]->feliratIdx = 0;
                w->teL[0]->sorIdx = 0;
                w->teL[1]->sorIdx = 1;
                w->teL[2]->sorIdx = 2;
                w->teL[3]->sorIdx = 3;
                showMsgBox(w->satr("Új feliratfájl mentve") + ": " + subtitleFajl, QColor(150, 150, 255), 5000);
            } else {                                //nem új feliratfájl:
                int idx;
                if (mesterIdo < feliratok[0]->t1[feliratok[0]->iAkt]) {
                    idx = feliratok[0]->iAkt;
                    feliratok[0]->t1[feliratok[0]->iAkt] = qMax(feliratok[0]->t1[feliratok[0]->iAkt], mesterIdo + 2.0);
                    showMsgBox(w->satr("Új felirat mentve mint első felirat."), QColor(200, 150, 200), 5000);
                } else {
                    idx = feliratok[0]->iAkt + 1;
                    feliratok[0]->t2[feliratok[0]->iAkt] = qMin(feliratok[0]->t2[feliratok[0]->iAkt], mesterIdo);
                    showMsgBox(w->satr("Új felirat mentve."), QColor(200, 150, 200), 5000);
                }
                feliratok[0]->L[0].insert(idx, w->teL[9]->toPlainText());
                feliratok[0]->text[0] = w->teL[9]->toPlainText();
                feliratok[0]->L[1].insert(idx, "x");
                feliratok[0]->text[1] = "x";
                feliratok[0]->L[2].insert(idx, "x");
                feliratok[0]->text[2] = "x";
                feliratok[0]->L[3].insert(idx, w->teL[9]->toPlainText() + "\nx\nx");
                feliratok[0]->text[3] = w->teL[9]->toPlainText() + "\nx\nx";
                feliratok[0]->t1.insert(idx, t1);
                feliratok[0]->t2.insert(idx, t2);
                w->subtitleFileUpdate(0);
                feliratok[0]->iAkt = idx;
            }
        }
    } else if (par == 2) {                  //a felirat kezdete az aktuális időpont:
        if (0 < feliratok[0]->iAkt) {
            feliratok[0]->t2[feliratok[0]->iAkt - 1] = qMin(feliratok[0]->t2[feliratok[0]->iAkt - 1], mesterIdo);
        }
        double t2 = mesterIdo + feliratok[0]->t2[feliratok[0]->iAkt] - feliratok[0]->t1[feliratok[0]->iAkt];
        if (feliratok[0]->iAkt < feliratok[0]->t2.size() - 1) {
            t2 = qMin(t2, feliratok[0]->t1[feliratok[0]->iAkt + 1]);
        }
        feliratok[0]->t1[feliratok[0]->iAkt] = mesterIdo;
        feliratok[0]->t2[feliratok[0]->iAkt] = t2;
        w->subtitleFileUpdate(0);
        showMsgBox(w->satr("Felirat kezdőpont mentve."), QColor(80, 160, 80), 5000);
    } else if (par == 3) {                  //a felirat vége az aktuális időpont:
        if (feliratok[0]->iAkt < feliratok[0]->t2.size() - 1) {
            feliratok[0]->t1[feliratok[0]->iAkt + 1] = qMax(feliratok[0]->t1[feliratok[0]->iAkt + 1], mesterIdo);
        }
        feliratok[0]->t2[feliratok[0]->iAkt] = mesterIdo;
        w->subtitleFileUpdate(0);
        showMsgBox(w->satr("Felirat végpont mentve."), QColor(100, 200, 100), 5000);

    } else if (par == 4) {           //Előző felirat végét az aktuális elejéhez igazítja:
        if (w->teL[0]->toPlainText() != "") {
            if (feliratok[0]->iAkt > 0) {
                feliratok[0]->t2[feliratok[0]->iAkt - 1] = feliratok[0]->t1[feliratok[0]->iAkt];
                showMsgBox(w->satr("Előző felirat kezdőpontja az aktuálishoz igazítva."), QColor(150, 200, 200), 5000);
            }
        }
    } else if (par == 5) {                  //OCR hozzáfűzése az aktuális felirathoz:
        if (w->teL[9]->toPlainText() != "") {
            int idx = feliratok[0]->iAkt;
            QString st = feliratok[0]->L[0][idx] + QString::fromUtf8("，") + w->teL[9]->toPlainText();
//            qWarning("idx: %d, st: " + st.toUtf8(), idx);
            feliratok[0]->L[0][idx] = st;
            feliratok[0]->text[0] = st;
            feliratok[0]->L[3][idx] = st + "\nx\nx";
            feliratok[0]->text[3] = st + "\nx\nx";
            w->subtitleFileUpdate(0);
            showMsgBox(w->satr("OCR hozzáfűzve az aktuális felirathoz."), QColor(200, 200, 100), 5000);
        }
    } else if (par == 6 || par == 7) {                  //Felirat kezdőpont xms millisec-kel korábbra/későbbre:
        if (w->teL[0]->toPlainText() != "") {
            double t1;
            QString msg;
            QColor msgColor;
            if (par == 6) {
                t1 = feliratok[0]->t1[feliratok[0]->iAkt] - xms_millisec/1000.0;
                msg = w->satr("Felirat kezdőpont ") + QString::number(xms_millisec) + w->satr(" ms-al korábbra tolva.");
                msgColor = QColor(80, 160, 80);
            } else {
                t1 = feliratok[0]->t1[feliratok[0]->iAkt] + xms_millisec/1000.0;
                msg = w->satr("Felirat kezdőpont ") + QString::number(xms_millisec) + w->satr(" ms-al későbbre tolva.");
                msgColor = QColor(160, 160, 40);
            }
            if (0 < feliratok[0]->iAkt) {
                feliratok[0]->t2[feliratok[0]->iAkt - 1] = qMin(feliratok[0]->t2[feliratok[0]->iAkt - 1], t1);
            }
            feliratok[0]->t1[feliratok[0]->iAkt] = t1;
            w->subtitleFileUpdate(0);
            showMsgBox(msg , msgColor, 5000);
        }
    }
    imgSzerkesztes->hide();
}

void testFv() {
    qWarning("testFv");
}

void feliratKezel(bool saLoopHivjaMeg) {
//    qWarning("feliratKezel_1, feliratPause = %d, feliratPauseIdo = %f", feliratPause, feliratPauseIdo);
    if (imgSzerkesztes && imgSzerkesztes->isHidden()) {
        for(int i=0; i < N_feliratok; i++) {
            feliratok[i]->kezel(mesterIdo-feliratKesleltetes, feliratPause, feliratPauseIdo);
        }

        if (N_feliratok > 0) {
            for(int i=0; i < 4; i++) {
                //te->setText2(feliratok[0]->text);
                w->teL[i]->setText2(feliratok[w->teL[i]->feliratIdx]->text[w->teL[i]->sorIdx]);
            }
        }
        if (N_feliratok > 1) {
            w->teL[4]->setText2(feliratok[w->teL[4]->feliratIdx]->text[w->teL[4]->sorIdx]);
        }
    }

    if (Tg == AlapTg) {
        if (pauseMod != 2 || !saLoopHivjaMeg) {
            QString sStA1 = "<span style=\"color: rgba(" + idoC + "); background: rgba(" + idoBGC1 + ");\">";
            QString sStA2 = "<span style=\"color: rgba(" + idoC + "); background: rgba(" + idoBGC2 + ");\">";
            QString sStB = "</span>";

            QTime t = QTime(0, 0, 0).addMSecs((int) (mesterIdo*1000));
            QString st = sStA1;
            if (pauseMod == 0) {
                st = st + t.toString("hh:mm:ss,000");
            } else {
                st = st + t.toString("hh:mm:ss,zzz");
            }
            t = QTime(0, 0, 0).addMSecs((int) (videoHossz*1000));
            st = st + " / " + t.toString("hh:mm:ss,zzz");

            st = st + ", " + sStB + sStA2 + "h:" + QString::number(hangEro) + sStB + sStA1;

            st = st + ", [" + QString::number(aktualisAblakElrendezes) +
                (w->isKijelolesUjSelSepa ? "|" : ".") + "]";

            st = st + ", " + sStB + sStA2 + QString::number(bkmIdx+1) + "/" + QString::number(bkmN);

            if (bkmCommandOn) {
                if (bkmIsSleepOn) {
                    st = st + "c";
                } else {
                    st = st + "C";
                }
            }

            st = st + sStB + sStA1;

            st = st + ", " + QString::number(sebessegek.split("|")[sebessegIdx].toDouble(), 'f', 2) + "x";

            if (autoFeliratPause == 0) {
                st = st + ", " + sStB + sStA2 + "-";
            } else {
                st = st + ", " + sStB + sStA2 + "A-";
            }
            if (feliratPause == 0) {
                st = st + " ";
            } else {
                st = st + "P";
            }
            if (tB > -0.9) {
                st = st + ", AB";
            } else if (tA > -0.9) {
                st = st + ", A";
            }
            st = st + sStB + sStA1;

            if (ittJarokPos >= 0.0) {
                st = st + ", *i" + sStB;
            }

    //        qWarning("st = " + st.toUtf8());
            w->teL[5]->setText2(st);    //pauseMode > 0 esetén csak 1-szer frissüljön
                        //az idő ablak, máskülönben ciklikus frissítésnél a kijelölés problémás
        }

        if (pauseMod == 1) {
            pauseMod = 2;    //pauseMode > 0 esetén csak 1-szer frissüljön
                    //az idő ablak, máskülönben ciklikus frissítésnél a kijelölés problémás
        }
    } else if (Tg == FeliratKesleltetesTg) {
        w->teL[5]->setText2(w->satr("Feliratkésleltetés") + ": " + QString::number(feliratKesleltetes, 'f', 2));
    } else if (Tg == HangKesleltetesTg) {
        w->teL[5]->setText2(w->satr("Hangkésleltetés") + ": " + QString::number(hangKesleltetes, 'f', 2));
    } else if (Tg == VilagossagTg) {
        w->teL[5]->setText2(w->satr("Világosság") + ": " + QString::number(vilagossag, 'f', 2));
    } else if (Tg == ElessegTg) {
        w->teL[5]->setText2(w->satr("Élesség") + ": " + QString::number(elesseg, 'f', 2));
    } else if (Tg == TelitettsegTg) {
        w->teL[5]->setText2(w->satr("Telítettség") + ": " + QString::number(telitettseg, 'f', 2));
    } else if (Tg == GammaTg) {
        w->teL[5]->setText2(w->satr("Gamma") + ": " + QString::number(gamma, 'f', 2));
    } else if (Tg == SzinHoTg) {
        w->teL[5]->setText2(w->satr("Színhő") + ": " + QString::number(szinHo, 'f', 0));
    } else if (Tg == KeparanyTg) {
        w->teL[5]->setText2(w->satr("Képarány") + ": " + aspectRatioTomb[aspectRatioIdx]);
    }
}

void kezdetiBeallitasok()
{
    //"path\fajl 01*.srt"-re szűrés szóköz miatt sehogy sem működik, ezért a fájlnevekben nem lehet szóköz

    QDir feliratDir = QDir(QFileInfo(videoFile).absolutePath(), "*.srt");  //pl a "A Title*.str" szűrő szóköz miatt sajnos nem működik

    QFileInfoList list_srt = feliratDir.entryInfoList();

    //qWarning("stA1 = " + stA1.toUtf8());
    //qWarning("videoName = " + videoName.toUtf8());
    for (int i = 0; i < list_srt.size(); i++) {
        QFileInfo fileInfo = list_srt[i];
        if (fileInfo.fileName().startsWith(QFileInfo(videoFile).completeBaseName())) {
            feliratok.append( new Felirat);
            feliratok.last()->abszolutFajlPath = fileInfo.absoluteFilePath();
            feliratok.last()->beolvas();
        }
    }
    N_feliratok = feliratok.size();
    qWarning("N_feliratok: %d", N_feliratok);
}

void ablakElrendezes(int ujAe, bool kellMentes) {
    QString st;
    if (kellMentes) {
        bool isMax = w->isMaximized();
        if (isMax) {
            w->showNormal();
        }
        st = qRectToqString(w->geometry()).toUtf8();
        if (w->windowFlags() & (Qt::FramelessWindowHint)) {
            st = st + ",0";
        } else {
            st = st + ",1";
        }
        st= st + "," + QString::number(w->palette().background().color().red());
        st= st + "," + QString::number(w->palette().background().color().green());
        st= st + "," + QString::number(w->palette().background().color().blue());
        st= st + "," + QString::number(isMax);
        setParameter(&iniFileSt, "foablak_" + QString::number(aktualisAblakElrendezes), st);

        st = qRectToqString(w->player->geometry()).toUtf8();
        setParameter(&iniFileSt, "VideoAblak_" + QString::number(aktualisAblakElrendezes),
                     st.toUtf8());

        for (int i=0; i<w->teL.size(); i++) {
            QString idxSt = QString::number(i);
            setParameter(&iniFileSt, "SaTextEdit" + idxSt + "_" +
                    QString::number(aktualisAblakElrendezes),
                    qRectToqString(w->teL[i]->geometry()).toUtf8());
            setParameter(&iniFileSt, "SaTextEditStilus" + idxSt + "_" +
                    QString::number(aktualisAblakElrendezes),
                    w->teL[i]->getStilus());
        }
        setParameter(&iniFileSt, "SaTextEditStilus7_" +
                QString::number(aktualisAblakElrendezes),
                w->cedictStilusSt);
        setParameter(&iniFileSt, "Poti_" + QString::number(aktualisAblakElrendezes),
                     qRectToqString(w->poti->geometry()).toUtf8());
        setParameter(&iniFileSt, "PotiStilus_" + QString::number(aktualisAblakElrendezes),
                     w->poti->getStilus());
    }

    aktualisAblakElrendezes = ujAe;
    st = getParameter(&iniFileSt, "foablak_" + QString::number(aktualisAblakElrendezes));
    if (st.length() == 0) {
        aktualisAblakElrendezes = 0;
        st = getParameter(&iniFileSt, "foablak_" + QString::number(aktualisAblakElrendezes));
    }
    setParameter(&iniFileSt, "AktAblakElrendezes", QString::number(aktualisAblakElrendezes));
    QStringList stL = st.split(",");
    bool isMax = (stL.last().toInt() == 1);
    stL.removeLast();
    QColor col(0, 0, 0);
    col.setBlue(stL.last().toInt());
    stL.removeLast();
    col.setGreen(stL.last().toInt());
    stL.removeLast();
    col.setRed(stL.last().toInt());
    stL.removeLast();
    QPalette pal = w->palette();
    pal.setColor(QPalette::Background, col);
    w->hide();
    w->setAutoFillBackground(true);
    w->setPalette(pal);
    if ( stL.last().compare("1")==0 ) {
        w->setWindowFlags(Qt::Window);
    } else {
        w->setWindowFlags(Qt::FramelessWindowHint);
    }
    stL.removeLast();
    st = stL.join(",");
    foablakRect = qStringToqRect(st);
    w->setGeometry(foablakRect);
    w->show();
    if (isMax) {
        w->showMaximized();
    }

    st = getParameter(&iniFileSt, "VideoAblak_" + QString::number(aktualisAblakElrendezes));
    w->player->setGeometry(qStringToqRect(st));

    for(int i=0; i < w->teL.size(); i++) {
        QString st = getParameter(&iniFileSt, "SaTextEditStilus" +
                QString::number(i).trimmed() +
                "_" + QString::number(aktualisAblakElrendezes));
        w->teL[i]->setStilus(st);
        if (i == 5) {   //Beállítások az 5. (idő) ablakhoz:
            QStringList stL = st.split("|");
            idoC = stL[1] + "," + stL[2] + "," + stL[3] + ",100%";   //pl "55,55,51,100%"
            idoBGC1 = stL[4] + "," + stL[5] + "," + stL[6] + ",100%";   //pl "55,55,51,100%"
            int cR = stL[4].toInt() + (stL[1].toInt() - stL[4].toInt()) * SaTextEdit5BCArany;
            int cG = stL[5].toInt() + (stL[2].toInt() - stL[5].toInt()) * SaTextEdit5BCArany;
            int cB = stL[6].toInt() + (stL[3].toInt() - stL[6].toInt()) * SaTextEdit5BCArany;
            idoBGC2 = QString::number(cR) + "," + QString::number(cG) +
                    "," + QString::number(cB) + ",100%";   //pl "55,55,51,100%"
        }
        st = getParameter(&iniFileSt, "SaTextEdit" +
                QString::number(i).trimmed() +
                "_" + QString::number(aktualisAblakElrendezes));
        //qWarning("geo[%d] = " + st.toUtf8(), i);
        w->teL[i]->setGeometry(qStringToqRect(st));
        //w->teL[i]->setGeometry(1100, i*200+50, 800, 150);
        w->teL[i]->setLathato(w->teL[i]->getLathato());

    }
    w->cedictStilusSt = w->teL[7]->getStilus();

    st = getParameter(&iniFileSt, "PotiStilus_" + QString::number(aktualisAblakElrendezes));
    //qWarning("PotiStilus_ = " + st.toUtf8());
    w->poti->setStilus(st);
    st = getParameter(&iniFileSt, "Poti_"+ QString::number(aktualisAblakElrendezes));
    //qWarning("geo = " + st.toUtf8());
    w->poti->setGeometry(qStringToqRect(st));
    w->poti->raise();
    w->hScrollBar->raise();
    w->vScrollBar->raise();
    //w->teL[i]->setGeometry(1100, i*200+50, 800, 150);
}

QString qRectToqString(QRect r) {
    QString st = QString::number(r.x());
    st = st + "," + QString::number(r.y());
    st = st + "," + QString::number(r.width());
    st = st + "," + QString::number(r.height());
    return st;
}

QRect qStringToqRect(QString st) {
    QStringList stL = st.split(",");
    QRect r(stL[0].toInt(), stL[1].toInt(), stL[2].toInt(), stL[3].toInt());
    return r;
}

QString getParameter(QString *forras, QString parNev) {
    QString eR = "";
    QRegExp rx("\\n\\$" + parNev + "=([^\\n#]*)[\\n#]+");
    if (rx.indexIn("\n" + *forras) != -1) {
        eR = rx.cap(1);
        eR = eR.split("#")[0];
        eR = eR.trimmed();
        return eR;
    } else {
        return "";
    }
}

void setParameter(QString *forras, QString parNev, QString ertek, bool torlesHaUres) {
    if (!torlesHaUres && ertek == getParameter(forras, parNev)) {
        return;
    }
    int pos = forras->indexOf("\n$" + parNev + "=");
    if (pos > -1) {
        if (torlesHaUres && ertek == "") {
            int pos2 = forras->indexOf("\n", pos + 1);
            forras->replace(pos, pos2 - pos, "");
        } else {
            int pos2 = pos + 1;
            while (forras->mid(pos2, 1) != "\n" && forras->mid(pos2, 1) != "#") {
                pos2++;
            }
            if (forras->mid(pos2, 1) == "#") {
                while (forras->mid(pos2 - 1, 1) == " ") {
                    pos2--;
                }
            }
            forras->replace(pos, pos2 - pos, "\n$" + parNev + "=" + ertek);
        }
    } else {
        forras->append("$" + parNev + "=" + ertek + "\n");
    }
}

void kilepes() {
    if (w->isFullScreen()) {
        w->player->tgFullscreen();
    }
    alapMod();
    feliratKezel(false);
    w->saveIttJarokPos();
    iniMent(true);

    delete(imgMozgatas);
    imgMozgatas = 0;
    delete(imgSzerkesztes);
    imgMozgatas = 0;
    delete(msgBox);
    msgBox = 0;
    delete(takaroAlso);
    takaroAlso = 0;
    delete(takaroJobb);
    takaroJobb = 0;
}

void alapMod() {
    Tg = AlapTg;  //sa: e nélkül pl. a DOWN billentyűt nem érzékeli
    msgBox->hide();
    w->player->lower();
    w->poti->raise();
    w->hScrollBar->raise();
    w->vScrollBar->raise();
    w->scrollBarsRefresh();
//    qWarning("alapMod:");
    if (w->teL[9]->getLathato() && !isOCRmaradjonLathato) {
        w->teL[9]->setLathato(false);  //OCR elrejtés
    }

    if (w->teL[7]->isVisible()) {   //poti ne takarja el a Cedict ablakot
        w->teL[7]->raise();
    }
    if (w->tesztPad->isVisible()) {
        w->tesztPad->hide();
    }

    w->setFocus();
}

double idoStToDouble(QString st) {
    QRegExp rx1("^(\\d\\d):(\\d\\d):(\\d\\d),(\\d\\d\\d)$");
    int pos = rx1.indexIn(st);
    if ( pos != -1 ) {
        double d = rx1.cap(1).toInt()*3600 + rx1.cap(2).toInt()*60 + rx1.cap(3).toInt() +
                rx1.cap(4).toInt()/1000.0;
        return d;
    } else {
        return -1.0;
    }
}

QString idoDoubleToSt(double d0) {
    if (d0 < 0.0) {
        return "-1.0";
    } else {
        QTime t = QTime(0, 0, 0).addMSecs((int) (d0*1000));
        return t.toString("hh:mm:ss,zzz");
    }
}

void fajlmegnyitasElokeszites(bool isStart) {
    if (isStart) {
        argL = app->arguments();
    }
    QFileInfo fajlInfo = QFileInfo(argL[0]);
    exePath = fajlInfo.path()+"/";
    exeName = fajlInfo.completeBaseName();
    if (w) {
        for(int i=0; i<w->teL.size(); i++) {    //9 db ablak
            delete(w->teL[i]);
        }
        w->teL.clear();
        if (w->player) {
            w->player->finish();
        }
    } else {
        w = new SaMainWindow;
        w->poti = new SaPoti(Qt::Horizontal, w);
        w->player = new SaVideo(w);
        w->player->show();
        w->show();
    }

    pauseMod = 0;
    billTriggerSt = "";
    isOCRmaradjonLathato = false;

    ablakMozgatas = false;
    feliratPause = 0;
    ittJarokPos = -1.0;
    bkmIdx = -1;
    bkmN = 0;
    bkmTimeL.clear();
    bkmAllCommandsL.clear();
    bkmCommentL.clear();
    globalVisible = true;
    N_feliratok = 0;
    billBuffer = "";
    feliratok.clear();
    if (!isStart) {
        setParameter(&iniFileSt, "Nyelv", SaMainWindow::nyelv, false);
        iniMent(true);
    }
    if (argL.size() == 1) {
        QString fileOpen;
        if (openedFilePath.length() == 0) {
            fileOpen = QFileDialog::getOpenFileName(0, exeName +
                    " - " + SaMainWindow::satr("Médiafájl megnyitása"));
        } else {
            fileOpen = QFileDialog::getOpenFileName(0, exeName +
                    " - " + SaMainWindow::satr("Médiafájl megnyitása"), openedFilePath);
        }
        argL.append(fileOpen + "");
    }

    QStringList nevL;
    //ablaklista (ablak lista, felirat lista):
    nevL << QString::fromUtf8("W_00 (1/ch)")     <<
            QString::fromUtf8("W_01 (1/pin)")    <<
            QString::fromUtf8("W_02 (1/ford)")   <<
            QString::fromUtf8("W_03 (1)")        <<
            QString::fromUtf8("W_04 (2)")        <<
            QString::fromUtf8("W_05 (idő)")      <<
            QString::fromUtf8("W_06 (SaSúgó)")   <<
            QString::fromUtf8("W_07 (Cedict)")   <<
            QString::fromUtf8("W_08 (Súgó)")     <<
            QString::fromUtf8("W_09 (OCR)");
            //van még w->poti ("W_10 (Poti)") és w->player ("W_11 (Player)") is, de nem a w->teL-ben!
    for(int i=0; i<nevL.count(); i++) {
        w->teL.append(new SaTextEdit(w));
        w->teL[i]->setObjectName(nevL[i]);
        w->teL[i]->idx = i;
    }

    bkmIniBeolvas();
    kezdetiBeallitasok();
    w->nyelvValtas(getParameter(&iniFileSt, "Nyelv"));
    w->cedictMenuSetChecked(cedictDict);

    if (isStart) {
        QImage myImage;
        myImage.load(exePath + "../resources/" + "szerkesztes.jpg");
        myImage = myImage.scaled(53, 53, Qt::KeepAspectRatio);
        imgSzerkesztes = new QLabel(w);
        imgSzerkesztes->setObjectName("imgSzerkesztes");
        imgSzerkesztes->hide();
        imgSzerkesztes->setPixmap(QPixmap::fromImage(myImage));
        imgSzerkesztes->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
        imgSzerkesztes->setWindowModality(Qt::NonModal);
        imgSzerkesztes->setFocusPolicy( Qt::NoFocus);
        imgSzerkesztes->move(w->width()-75, 25);
        imgSzerkesztes->resize(25, 25);

        myImage.load(exePath + "../resources/" + "mozgatas.png");
        myImage = myImage.scaled(53, 53, Qt::KeepAspectRatio);
        imgMozgatas = new QLabel(w);
        imgMozgatas->setObjectName("imgMozgatas");
        imgMozgatas->hide();
        imgMozgatas->setPixmap(QPixmap::fromImage(myImage));
        imgMozgatas->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
        imgMozgatas->setWindowModality(Qt::NonModal);
        imgMozgatas->setFocusPolicy( Qt::NoFocus);
        imgMozgatas->move(w->width()-75, 25);
        imgMozgatas->resize(50, 50);

        msgBox = new QLabel(w);
        QFont font = msgBox->font();
        font.setPointSize(30);
        msgBox->setFont(font);
        msgBox->setWindowFlags(Qt::FramelessWindowHint);
        msgBox->setFixedSize(800, 120);

        QObject::connect(w->poti, SIGNAL(sliderMoved(int)), w, SLOT(changePoti(int)));
        QObject::connect(w->tesztPad->ui->pot1, SIGNAL(valueChanged(int)), w, SLOT(optimalizal(int)));
        QObject::connect(w->tesztPad->ui->pot2, SIGNAL(valueChanged(int)), w, SLOT(optimalizal(int)));
        QObject::connect(w->tesztPad->ui->pot3, SIGNAL(valueChanged(int)), w, SLOT(optimalizal(int)));
        QObject::connect(w->tesztPad->ui->pot4, SIGNAL(valueChanged(int)), w, SLOT(optimalizal(int)));
        QObject::connect(w->tesztPad->ui->pot5, SIGNAL(valueChanged(int)), w, SLOT(optimalizal(int)));
        QObject::connect(w->tesztPad->ui->pot6, SIGNAL(valueChanged(int)), w, SLOT(optimalizal(int)));
        QObject::connect(w->tesztPad->ui->pot7, SIGNAL(valueChanged(int)), w, SLOT(optimalizal(int)));
        QObject::connect(w->tesztPad->ui->pot8, SIGNAL(valueChanged(int)), w, SLOT(optimalizal(int)));
        QObject::connect(w->tesztPad->ui->pot9, SIGNAL(valueChanged(int)), w, SLOT(optimalizal(int)));
        QObject::connect(w->tesztPad->ui->pot10, SIGNAL(valueChanged(int)), w, SLOT(optimalizal(int)));
        QObject::connect(w->tesztPad->ui->pushButton, SIGNAL(clicked()), w, SLOT(felismer()));
        QObject::connect(w->tesztPad->ui->pushButton_4, SIGNAL(clicked()), w, SLOT(felismer2()));
    }


    w->teL[9]->setReadOnly(false);
    w->teL[9]->setToolTip(w->satr("Összefűzéshez írj + -t."));

    QObject::connect(w->teL[0], SIGNAL(szerkesztesSignal(int)), w, SLOT(szerkesztesSlot(int)));
    QObject::connect(w->teL[1], SIGNAL(szerkesztesSignal(int)), w, SLOT(szerkesztesSlot(int)));
    QObject::connect(w->teL[2], SIGNAL(szerkesztesSignal(int)), w, SLOT(szerkesztesSlot(int)));
    QObject::connect(w->teL[3], SIGNAL(szerkesztesSignal(int)), w, SLOT(szerkesztesSlot(int)));

    ablakElrendezes(aktualisAblakElrendezes, false);

    if (N_feliratok>0) {
        w->teL[0]->feliratIdx = 0;
        w->teL[1]->feliratIdx = 0;
        w->teL[2]->feliratIdx = 0;
        w->teL[3]->feliratIdx = 0;
        w->teL[0]->sorIdx = 0;
        w->teL[1]->sorIdx = 1;
        w->teL[2]->sorIdx = 2;
        w->teL[3]->sorIdx = 3;
    }
    if (N_feliratok>1) {
        w->teL[4]->feliratIdx = 1;
        w->teL[4]->sorIdx = 3;
    }

    w->pipeNev = getParameter(&iniFileSt, "pipeNev");

    if (isFileExists(openedFilePath + "SaVLC.ocr")) {   // a SaVLC.ocr fájl létezik:
        QString st = fileToQString(openedFilePath + "SaVLC.ocr");
        w->optFoglalt = true;
        w->tesztPad->ui->pot1->setValue(getParameter(&st, "ocr_pot1").toInt());
        w->tesztPad->ui->pot2->setValue(getParameter(&st, "ocr_pot2").toInt());
        w->tesztPad->ui->pot3->setValue(getParameter(&st, "ocr_pot3").toInt());
        w->tesztPad->ui->pot4->setValue(getParameter(&st, "ocr_pot4").toInt());
        w->tesztPad->ui->pot5->setValue(getParameter(&st, "ocr_pot5").toInt());
        w->tesztPad->ui->pot6->setValue(getParameter(&st, "ocr_pot6").toInt());
        w->tesztPad->ui->pot7->setValue(getParameter(&st, "ocr_pot7").toInt());
        w->tesztPad->ui->pot8->setValue(getParameter(&st, "ocr_pot8").toInt());
        w->tesztPad->ui->pot9->setValue(getParameter(&st, "ocr_pot9").toInt());
        w->tesztPad->ui->pot10->setValue(getParameter(&st, "ocr_pot10").toInt());
        w->tesztPad->ui->checkBox->setChecked(true);
        w->ocr->tessdataSt = getParameter(&st, "tessdata");
        w->optFoglalt = false;
    }

    QString st = getParameter(&iniFileSt, "FeliratSzin");
    w->isKijelolesUjSelSepa = (getParameter(&iniFileSt, "isKijelolesUjSelSepa") == "1");
    QStringList stL = st.split("|");
    takaroAlso = new QWidget(w->player);
    takaroAlso->setStyleSheet("border: 0px solid black; background: black;");
    takaroJobb = new QWidget(w->player);
    takaroJobb->setStyleSheet("border: 0px solid black; background: black;");

    ablakElrendezes(aktualisAblakElrendezes, false);

    w->ocr->videoFeliratSzin = QColor(stL[0].toInt(), stL[1].toInt(), stL[2].toInt());
    w->tesztPad->setColor(QColor(stL[0].toInt(), stL[1].toInt(), stL[2].toInt()));

    if (bkmFileName != "") {
        st = getParameter(&bkmFileSt, "ABszakaszIdok");
    } else {
        st = getParameter(&iniFileSt, "ABszakaszIdok");
    }
    if (st == "") {
        st = "-1.0|-1.0";
    }
    stL = st.split("|");
    if (stL[0].contains(":")) {
        tA = idoStToDouble(stL[0]);
    } else {
        tA = stL[0].toDouble();
    }
    if (stL[1].contains(":")) {
        tB = idoStToDouble(stL[1]);
    } else {
        tB = stL[1].toDouble();
    }

    if (tA > 0.0 && tB > 0.0 && autoFeliratPause != 0) {
        feliratPauseSetting(true);
    }

    sebessegek = getParameter(&iniFileSt, "Speeds");
    sebessegIdx = sebessegek.split("|").indexOf("1");

    lepesek = getParameter(&iniFileSt, "Steps");

    alapMod();

    //qWarning("argL.last(): " + argL.last().toLatin1());
    if (argL.last() != "") {
        w->timerS->start(2000);
    }
}

void sugoBeallitas() {
    //Súgó beállítás:
    sugoSt = "";
    QStringList iniFajlStList = iniFileSt.split("\n");
    for (int i=0; i<iniFajlStList.length(); i++)                                             {
        if (iniFajlStList[i] == "") {
            sugoSt += "\n";
        } else if (iniFajlStList[i].startsWith("$BILL_")) {
            sugoSt += w->satr(iniFajlStList[i-1].replace("#", "").replace(":", "").toUtf8()) + ": ";
            int pos1 = iniFajlStList[i].indexOf("=");
            int pos2 = iniFajlStList[i].indexOf("#");
            QString value = iniFajlStList[i].mid(pos1+1, pos2-pos1-1);
            sugoSt += value + "\n";
        }
    }
    sugoSt = sugoSt.replace("KP+", "KeyPad ");
    sugoSt = sugoSt.replace("\n\n\n", "\n\n");
    sugoSt = sugoSt.replace("\n\n\n", "\n\n");
    sugoSt = sugoSt.replace("\n\n\n", "\n\n");
    sugoSt = sugoSt.replace("\n\n\n", "\n\n");
}

void changePos(double pos) {
    w->changePos(pos*videoHossz);
}

SaTextEdit* kijelolesiAblak() {
    SaTextEdit *ablak = NULL;
    int idx = -1;
    if (w->utolsoKijelolesFokusz) {
        idx = w->utolsoKijelolesFokusz->idx;
    }
    if ((idx == 0 || idx == 3 || idx == 9) && w->teL[idx]->getLathato()) {
        ablak = w->utolsoKijelolesFokusz;
    } else if (w->teL[0]->isVisible() && w->teL[0]->toPlainText() != "") {
        ablak = w->teL[0];
    } else if (w->teL[3]->isVisible() && w->teL[3]->toPlainText() != "") {
        ablak = w->teL[3];
    } else if (w->teL[9]->isVisible() && w->teL[9]->toPlainText() != "") {
        ablak = w->teL[9];
    }
    if (ablak && ablak->idx == 9) {
        isOCRmaradjonLathato = true;
    }
    return ablak;
}

bool isFileExists(QString st) {
    QFileInfo fi(st);
    if (fi.exists() && fi.isFile()) {
        return true;
    } else {
        return false;
    }
}

double getIttJarokTimeFromIni() {
    QString st = getParameter(&iniFileSt, "IttJarok");
    double t = -1.0;
    if (st != "") {
        QStringList stL = st.split("|");
        for (int i=0; i<stL.size(); i++) {
            if (stL[i].startsWith(videoFile, Qt::CaseInsensitive)) {
                t = idoStToDouble(stL[i].split("@")[1]);
                break;
            }
        }
    }
    return t;
}

void setIttJarokTimeInIni(double t) {
    QString st1 = videoFile + "@" + idoDoubleToSt(t);
    delIttJarokTimeInIni();
    QString st2 = getParameter(&iniFileSt, "IttJarok");
    if (st2 == "") {
        setParameter(&iniFileSt, "IttJarok", st1, false);
    } else {
        setParameter(&iniFileSt, "IttJarok", st2 + "|" + st1, false);
    }
    iniMent(false);
}

void delIttJarokTimeInIni() {
    QString st = getParameter(&iniFileSt, "IttJarok");
    if (st != "") {
        QStringList stL = st.split("|");
        for (int i = 0; i < stL.size(); i++) {
            if (stL[i].startsWith(videoFile, Qt::CaseInsensitive)) {
                stL.removeAt(i);
            }
        }
        st = stL.join("|");
        setParameter(&iniFileSt, "IttJarok", st, false);
        iniMent(false);
    }
}

void showMsgBox(QString text, QColor bgColor, int msec) {
    QPalette p = msgBox->palette();
    p.setColor(QPalette::Active, QPalette::Window, bgColor);
    QString st = "background: rgba(" + QString::number(bgColor.red()) + ", " +
            QString::number(bgColor.green()) + ", " +
            QString::number(bgColor.blue()) + ", 100%);";
    msgBox->setStyleSheet(st);
    msgBox->setPalette(p);
    msgBox->setWordWrap(true);
    msgBox->setText(text);
    msgBox->raise();
    msgBox->move(w->player->pos().x() + 7, w->player->pos().y() + w->player->height() / 2 - msgBox->height() / 2);
    msgBox->show();
    msgBoxIdo = msec;
    msgBoxIdoS.start();
    w->setFocus();
}

void iniMent(bool isWindowArrangementNeeded) {
    if (isWindowArrangementNeeded) {
        ablakElrendezes(aktualisAblakElrendezes, true);
    }
    setParameter(&iniFileSt, "bkmCommandOn", bkmCommandOn ? QString::number(1).toUtf8() : QString::number(0).toUtf8());
    setParameter(&iniFileSt, "Hangero", QString::number(hangEro).toUtf8());
    setParameter(&iniFileSt, "cedictDict", cedictDict);
    setParameter(&iniFileSt, "Vilagossag", QString::number(vilagossag).toUtf8());
    setParameter(&iniFileSt, "Elesseg", QString::number(elesseg).toUtf8());
    setParameter(&iniFileSt, "Telitettseg", QString::number(telitettseg).toUtf8());
    setParameter(&iniFileSt, "Gamma", QString::number(gamma).toUtf8());
    setParameter(&iniFileSt, "szinHo", QString::number(szinHo).toUtf8());
    setParameter(&iniFileSt, "Hangkesleltetes", QString::number(hangKesleltetes).toUtf8());
    setParameter(&iniFileSt, "Feliratkesleltetes", QString::number(feliratKesleltetes).toUtf8());
    setParameter(&iniFileSt, "KeparanyIdx", QString::number(aspectRatioIdx).toUtf8());
    setParameter(&iniFileSt, "autoFeliratPause", QString::number(autoFeliratPause).toUtf8());
    setParameter(&iniFileSt, "Nyelv", SaMainWindow::nyelv);
    setParameter(&iniFileSt, "cedictSzotar", SaMainWindow::cedictSzotar);
    if (w->tesztPad->ui->checkBox->checkState() == Qt::Unchecked) {
        setParameter(&iniFileSt, "ocr_pot1", QString::number(w->tesztPad->ui->pot1->value()).toUtf8());
        setParameter(&iniFileSt, "ocr_pot2", QString::number(w->tesztPad->ui->pot2->value()).toUtf8());
        setParameter(&iniFileSt, "ocr_pot3", QString::number(w->tesztPad->ui->pot3->value()).toUtf8());
        setParameter(&iniFileSt, "ocr_pot4", QString::number(w->tesztPad->ui->pot4->value()).toUtf8());
        setParameter(&iniFileSt, "ocr_pot5", QString::number(w->tesztPad->ui->pot5->value()).toUtf8());
        setParameter(&iniFileSt, "ocr_pot6", QString::number(w->tesztPad->ui->pot6->value()).toUtf8());
        setParameter(&iniFileSt, "ocr_pot7", QString::number(w->tesztPad->ui->pot7->value()).toUtf8());
        setParameter(&iniFileSt, "ocr_pot8", QString::number(w->tesztPad->ui->pot8->value()).toUtf8());
        setParameter(&iniFileSt, "ocr_pot9", QString::number(w->tesztPad->ui->pot9->value()).toUtf8());
        setParameter(&iniFileSt, "ocr_pot10",
                QString::number(w->tesztPad->ui->pot10->value()).toUtf8());
        setParameter(&iniFileSt, "tessdata", w->ocr->tessdataSt);
    } else {
        QString st = "#(" + openedFilePath + openedFileName + "." + openedFileExtension + ")\n";
        setParameter(&st, "ocr_pot1", QString::number(w->tesztPad->ui->pot1->value()).toUtf8());
        setParameter(&st, "ocr_pot2", QString::number(w->tesztPad->ui->pot2->value()).toUtf8());
        setParameter(&st, "ocr_pot3", QString::number(w->tesztPad->ui->pot3->value()).toUtf8());
        setParameter(&st, "ocr_pot4", QString::number(w->tesztPad->ui->pot4->value()).toUtf8());
        setParameter(&st, "ocr_pot5", QString::number(w->tesztPad->ui->pot5->value()).toUtf8());
        setParameter(&st, "ocr_pot6", QString::number(w->tesztPad->ui->pot6->value()).toUtf8());
        setParameter(&st, "ocr_pot7", QString::number(w->tesztPad->ui->pot7->value()).toUtf8());
        setParameter(&st, "ocr_pot8", QString::number(w->tesztPad->ui->pot8->value()).toUtf8());
        setParameter(&st, "ocr_pot9", QString::number(w->tesztPad->ui->pot9->value()).toUtf8());
        setParameter(&st, "ocr_pot10",
                QString::number(w->tesztPad->ui->pot10->value()).toUtf8());
        setParameter(&st, "tessdata", w->ocr->tessdataSt);
        qStToFile(openedFilePath + "SaVLC.ocr", st);
    }
    QString st = QString::number(w->ocr->videoFeliratSzin.red()) + "|" +
            QString::number(w->ocr->videoFeliratSzin.green()) + "|" +
            QString::number(w->ocr->videoFeliratSzin.blue()) + "|";
    setParameter(&iniFileSt, "FeliratSzin", st);
    setParameter(&iniFileSt, "isKijelolesUjSelSepa", (w->isKijelolesUjSelSepa ? "1" : "0"));
    setParameter(&iniFileSt, "Speeds", sebessegek);
    setParameter(&iniFileSt, "Steps", lepesek);

    if (bkmFileName != "") {
        setParameter(&bkmFileSt, "ABszakaszIdok",
                idoDoubleToSt(tA).toUtf8() + "|" + idoDoubleToSt(tB).toUtf8());
        if (bkmFajlStOriginal != bkmFileSt) {
            qStToFile(bkmFileName, bkmFileSt);
            bkmFajlStOriginal = bkmFileSt;
        }
    } else {
        setParameter(&iniFileSt, "ABszakaszIdok",
                idoDoubleToSt(tA).toUtf8() + "|" + idoDoubleToSt(tB).toUtf8());
    }

    qStToFile(iniFileName, iniFileSt, false);
}

void beep(int ms) {
    Beep(523, ms); // 523 hertz (C5) for 500 milliseconds
}

bool saTimerMs(long ms) {
    if (saTimerStartMs == -1) {
        saTimerStartMs = QDateTime::currentMSecsSinceEpoch ();
        return true;
    } else if (saTimerStartMs != -2) {
        saTimerMsDT = ms - QDateTime::currentMSecsSinceEpoch () + saTimerStartMs;
        if (0 < saTimerMsDT) {
            return true;
        } else {
            saTimerStartMs = -1;
            return false;
        }
    }
}

void bkmClearCommands() {
    bkmCurrentCommandL.clear();
    bkmCurrentCommand = "";
    bkmCurrentCommandParameter = -1;
    saTimerStartMs = -1;
    saTimerMsDT = -1;
    bkmIsSleepOn = false;
}

bool bkmForceNewFn() {
    if (bkmN == 0) {
        return false;
    } else if (bkmIdx == -1) {
        return abs(mesterIdo - bkmTimeL[0]) < 0.1;
    } else {
        return abs(mesterIdo - bkmTimeL[bkmIdx]) < 0.1;
    }
}

void bkmUpdateFileSt() {
    int pos1 = bkmFileSt.indexOf("\n$BKM");
    while (pos1 > -1) {
        int pos2 = pos2 = bkmFileSt.indexOf("\n", pos1 + 1);
        if (pos2 == -1) {
            bkmFileSt = bkmFileSt.left(pos1);
        } else {
            bkmFileSt = bkmFileSt.left(pos1) + bkmFileSt.mid(pos2);
        }
        pos1 = bkmFileSt.indexOf("\n$BKM", pos1);
    }

    for (int i =0; i < bkmTimeL.size(); i++) {
        QString st = "$BKM" + QString::number(i + 1).rightJustified(4, '0') + "=" + idoDoubleToSt(bkmTimeL[i]);
        if (bkmAllCommandsL[i] != "") {
            st += " $ " + bkmAllCommandsL[i];
        }
        if (bkmCommentL[i] != "") {
            st += "  # " + bkmCommentL[i];
        }
        bkmFileSt += st + "\n";
    }
    if (bkmFajlStOriginal != bkmFileSt) {
        qStToFile(bkmFileName, bkmFileSt);
        bkmFajlStOriginal = bkmFileSt;
    }
}
