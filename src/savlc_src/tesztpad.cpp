#include "tesztpad.h"
#include "ui_tesztpad.h"
#include "samainwindow.h"

extern SaMainWindow *w;
extern QString exePath;
extern void alapMod();

TesztPad::TesztPad(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TesztPad)
{
    ui->setupUi(this);
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(showTessdataPanel()));
    tessdataPanel = new TessdataPanel();
    connect(tessdataPanel->ui->okButton, SIGNAL(clicked()), this, SLOT(okTessdataPanel()));
    connect(tessdataPanel->ui->megseButton, SIGNAL(clicked()),
            tessdataPanel, SLOT(close()));
    QDir export_folder(exePath + "/../tessdata/");
    export_folder.setNameFilters(QStringList()<<"*.traineddata");
    QStringList stl = export_folder.entryList();
    for (int i=0; i<stl.size(); i++) {
        cbList.append(new QCheckBox(stl[i], tessdataPanel));
        tessdataPanel->ui->verticalLayout->addWidget(cbList.last());
    }
}

TesztPad::~TesztPad()
{
    for (int i=0; i<cbList.size(); i++) {
        delete(cbList[i]);
    }
    delete(tessdataPanel);
    delete ui;
}

void TesztPad::mousePressEvent(QMouseEvent *e) {
    //qWarning("TesztPad::mousePressEvent");
    w->releaseMouse();
    QWidget::mousePressEvent(e);
}

void TesztPad::mouseReleaseEvent(QMouseEvent *e) {
    //qWarning("TesztPad::mouseReleaseEvent");
    //w->grabMouse();
    QWidget::mouseReleaseEvent(e);
}

void TesztPad::showTessdataPanel() {
    for (int i=0; i<cbList.size(); i++) {
        if (w->ocr->tessdataSt.indexOf(cbList[i]->text().replace(".traineddata", "")) > -1) {
            cbList[i]->setChecked(true);
        }
    }
    tessdataPanel->exec();
}

void TesztPad::okTessdataPanel() {
    //init
    //w->ocr->tessdataSt = "";
    QString tessdataSt = "";
    for (int i=0; i<cbList.size(); i++) {
        if (cbList[i]->isChecked()) {
            tessdataSt += cbList[i]->text().replace(".traineddata", "") + "+";
        }
    }
    tessdataSt = tessdataSt.left(tessdataSt.length()-1);
    qWarning("tessdataSt: " + tessdataSt.toUtf8());
    w->ocr->tessdataSt = tessdataSt;
    if (w->ocr->tess.Init(exePath.toLatin1() + "/../tessdata", w->ocr->tessdataSt.toLatin1())) {
        qWarning("OCRTesseract: Could not initialize tesseract.");
        return;
    }
    tessdataPanel->close();
}

void TesztPad::keyPressEvent(QKeyEvent *e) {
    if (e->key() == Qt::Key_Escape) {
        alapMod();
    } else {
        QWidget::keyPressEvent(e);
    }
}

void TesztPad::setColor(QColor col) {
    //pushButton_3 (Video feirat szín) színét állítja be
    QPalette p = ui->pushButton_3->palette();
    p.setColor(QPalette::Active, QPalette::Window, col);
    QString st = "background: rgba(" + QString::number(col.red()) + ", " +
            QString::number(col.green()) + ", " +
            QString::number(col.blue()) + ", 100%);";
    ui->pushButton_3->setPalette(p);
    ui->pushButton_3->setStyleSheet(st);
}
