#include <conio.h>
#include "samainwindow.h"

extern SaMainWindow *w;

long maxKarakterSzam = 32000;

int pipeKerdez(LPWSTR pipeNev,  LPWSTR kerdes, wchar_t* valasz)	//ez kérdez pipe-on keresztül a CedictServertől
{
    HANDLE hPipe;
    BOOL fSuccess;
    DWORD cbRead, dwMode;

    //Teszt:
    //wcscpy(valasz, kerdes0);  //ez jó
    //wcscpy(valasz, kerdes);  //ez jó
    //return 0;

    // Try to open a named pipe; wait for it, if necessary.
    while (1)
    {
        hPipe = CreateFileW(
            pipeNev,        // pipe name
            GENERIC_READ |
            GENERIC_WRITE,  // read and write access
            0,              // no sharing
            NULL,           // default security attributes
            OPEN_EXISTING,  // opens existing pipe
            0,              // default attributes
            NULL);          // no template file

        // Break if the pipe handle is valid.
        if (hPipe != INVALID_HANDLE_VALUE)
            break;

        // Exit if an error other than ERROR_PIPE_BUSY occurs.
        if (GetLastError() != ERROR_PIPE_BUSY)
        {
//            wchar_t v[200];
//            wsprintfW(v, L"_HIBA: Nem nyithato meg a pipe. GLE = %d, pipeNev = %s",
//                    GetLastError(), pipeNev);
            QString st = w->satr("_HIBA: Nem nyitható meg a pipe. Biztos hogy fut a CedictServer program?");
            st += QString(" (GLE = %1, pipeNev = %2)").arg(GetLastError()).arg(QString::fromUtf16((const ushort *)pipeNev));
            LPWSTR v = (LPTSTR)st.utf16();
            wcscpy(valasz, v);
            return 0;
        }

        // All pipe instances are busy, so wait for 3600 seconds.
        if (!WaitNamedPipeW(pipeNev, 3600000))
        {
            wcscpy(valasz, L"_HIBA nem nyitható meg a pipe, 3600 s lejárt.");
            return 0;
        }
    }

    // The pipe connected; change to message-read mode.
    dwMode = PIPE_READMODE_MESSAGE;
    fSuccess = SetNamedPipeHandleState(
        hPipe,    // pipe handle
        &dwMode,  // new pipe mode
        NULL,     // don't set maximum bytes
        NULL);    // don't set maximum time
    if (!fSuccess)
    {
        wchar_t v[200];
        wsprintfW(v, L"_HIBA: SetNamedPipeHandleState hibas. GLE = %d", GetLastError());
        wcscpy(valasz, v);
        return 0;
    }

    // Send a message to the pipe server and read the response.
    fSuccess = TransactNamedPipe(
        hPipe,                                    // pipe handle
        kerdes,                                   // message to server
        (wcslen(kerdes) + 1) * sizeof(wchar_t),   // message length
        valasz,                                   // buffer to receive reply
        (maxKarakterSzam + 1) * sizeof(wchar_t),  // size of read buffer
        &cbRead,                                  // bytes read
        NULL);                                    // not overlapped

    if (!fSuccess && (GetLastError() != ERROR_MORE_DATA))
    {
        wcscpy(valasz, L"_HIBA: TransactNamedPipe hiba.");
        return 0;
    }

    while (1)
    {
        // Break if TransactNamedPipe or ReadFile is successful
        if (fSuccess)
            break;

        // Read from the pipe if there is more data in the message.
        fSuccess = ReadFile(
            hPipe,                                    // pipe handle
            valasz,                                   // buffer to receive reply
            (maxKarakterSzam + 1) * sizeof(wchar_t),   // size of buffer
            &cbRead,                                  // number of bytes read
            NULL);                                    // not overlapped

                      // Exit if an error other than ERROR_MORE_DATA occurs.
        if (!fSuccess && (GetLastError() != ERROR_MORE_DATA))
            break;
    }

    _getch();
    CloseHandle(hPipe);
    return 0;
}
