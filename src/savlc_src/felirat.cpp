﻿#include "felirat.h"
#include "samainwindow.h"

extern QString fileToQString(QString fajlNev);
extern SaMainWindow *w;

Felirat::Felirat(QObject *parent) :
    QObject(parent)
{
}

void Felirat::beolvas()
{
    for(int i=0; i<4; i++) {
        L[i].clear();
    }
    t1.clear();
    t2.clear();

    QStringList feliratStL = fileToQString(abszolutFajlPath).split("\n");
    QString st = "";
    QRegExp rx1("^(\\d\\d):(\\d\\d):(\\d\\d),(\\d\\d\\d) --> (\\d\\d):(\\d\\d):(\\d\\d),(\\d\\d\\d)$"); //Qt-ben minden \ helyett \\ kell!
    QRegExp rx2("(.*)\\n\\d+\\n$"); //Qt-ben minden \ helyett \\ kell!
    double ido;
    QString stApp = "";
    QStringList stAppL;
    for (int sor = 1; sor < feliratStL.size(); sor++) {
        st = feliratStL[sor];
        //qWarning("st = " + st.toUtf8());

        if ( rx1.indexIn(st) != -1 ) {  // --> -s sor:
            if ( rx2.indexIn(stApp) != -1 ) {   //felirat sorszámát levesszük
                stApp = rx2.cap(1);
                stApp = stApp.trimmed();
                L[3].append(stApp);
                stAppL = stApp.split("\n");
                L[0].append("" + stAppL.at(0));
                if (stAppL.size()>1) {
                    L[1].append("" + stAppL.at(1));
                    stApp = "";
                    for(int i=2; i < stAppL.size(); i++) {
                        stApp = stApp + stAppL.at(i) + "\n";
                    }
                    stApp = stApp.trimmed();
                    L[2].append("" + stApp);
                } else {
                    L[1].append("");
                    L[2].append("");
                }
                stApp = "";
            }
            ido = rx1.cap(1).toInt()*3600 + rx1.cap(2).toInt()*60 + rx1.cap(3).toInt() +
                    rx1.cap(4).toInt()/1000.0;
            t1.append(ido);
            ido = rx1.cap(5).toInt()*3600 + rx1.cap(6).toInt()*60 + rx1.cap(7).toInt() +
                    rx1.cap(8).toInt()/1000.0;
            t2.append(ido);
        } else {
            stApp = stApp + st + "\n";
            //qWarning("stApp1 = " + stApp.toUtf8());
        }
    }
    if ( stApp.length() > 0 ) {
        stApp = stApp.trimmed();
        L[3].append(stApp);
        stAppL = stApp.split("\n");
        L[0].append("" + stAppL.at(0));
        if (stAppL.size()>1) {
            L[1].append("" + stAppL.at(1));
            stApp = "";
            for(int i=2; i < stAppL.size(); i++) {
                stApp = stApp + stAppL.at(i) + "\n";
            }
            stApp = stApp.trimmed();
            L[2].append("" + stApp);
        } else {
            L[1].append("");
            L[2].append("");
        }
    }
    this->N = L[3].size();

    kovTeendo = KIIR;
    kovT = -1;
    iAkt = -1;
    for(int i=0; i<4; i++) {
        text[i] = "";
    }
}

void Felirat::kezel(double mIdo, int feliratPause, double feliratPauseIdo) {
    int iAkt2 = -1;
    if (feliratPause != 0) {
        iAkt = keres(feliratPauseIdo);
        iAkt2 = iAkt;
    } else {
        iAkt = keres(mIdo);
        if (t1[iAkt] <= mIdo && mIdo <= t2[iAkt]) {
            iAkt2 = iAkt;
        }
    }

    if (-1 < iAkt2) {
        iAkt = iAkt2;
        for(int i=0; i<4; i++) {
            text[i] = L[i][iAkt];
        }
    } else {
        for(int i=0; i<4; i++) {
            text[i] = "";
        }
    }
}

int Felirat::keres(double mIdo) {
    if (iAkt > -1) {
        if ((t1[iAkt] <= mIdo && (iAkt == t1.size()-1 || mIdo <= t1[iAkt+1]))) {
            return iAkt;
        } else {
            if (iAkt < t1.size()-1 && t1[iAkt+1] <= mIdo && mIdo <= t2[iAkt+1])  {
                return iAkt+1;
            }
        }
    }

    int idx = w->keresIdx(t1, mIdo);
    if (idx == -1) {
        idx = 0;
    }
    return idx;
}
