TARGET = ../../../../bin_dev/saVLC
TEMPLATE = app

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

INCLUDEPATH += \
    ../vlc/include \
    ../tess/include \
    ../tess/leptonica/src \
    ../tess/tesseract/api \
    ../tess/tesseract/ccutil \
    ../tess/tesseract/android \
    ../tess/tesseract/api \
    ../tess/tesseract/ccmain \
    ../tess/tesseract/ccstruct \
    ../tess/tesseract/ccutil \
    ../tess/tesseract/classify \
    ../tess/tesseract/cmake \
    ../tess/tesseract/contrib \
    ../tess/tesseract/cube \
    ../tess/tesseract/cutil \
    ../tess/tesseract/dict \
    ../tess/tesseract/doc \
    ../tess/tesseract/java \
    ../tess/tesseract/neural_networks \
    ../tess/tesseract/opencl \
    ../tess/tesseract/snap \
    ../tess/tesseract/tessdata \
    ../tess/tesseract/testing \
    ../tess/tesseract/textord \
    ../tess/tesseract/training \
    ../tess/tesseract/viewer \
    ../tess/tesseract/vs2010 \
    ../tess/tesseract/wordrec \
    ../tess/tesseract/android\jni \
    ../tess/tesseract/cmake\templates \
    ../tess/tesseract/java\com \
    ../tess/tesseract/java\com\google \
    ../tess/tesseract/java\com\google\scrollview \
    ../tess/tesseract/java\com\google\scrollview\events \
    ../tess/tesseract/java\com\google\scrollview\ui \
    ../tess/tesseract/neural_networks\runtime \
    ../tess/tesseract/tessdata\configs \
    ../tess/tesseract/tessdata\tessconfigs \
    ../tess/tesseract/testing\reports \
    ../tess/tesseract/vs2010\include \
    ../tess/tesseract/vs2010\libtesseract \
    ../tess/tesseract/vs2010\port \
    ../tess/tesseract/vs2010\tesseract \

LIBS += ../../../bin_dev/libvlc.dll
LIBS += ../../../bin_dev/libvlccore.dll
LIBS += ../../../bin_dev/tessQt.dll

HEADERS += \
    satextedit.h \
    felirat.h \
    savideo.h \
    samainwindow.h \
    sapoti.h \
    pipe.h \
    ocropt.h \
    tesztpad.h \
    sadefs.h \
    tessdatapanel.h

SOURCES += main.cpp \
    satextedit.cpp \
    felirat.cpp \
    savideo.cpp \
    samainwindow.cpp \
    billmapfeltolto.cpp \
    sapoti.cpp \
    ocropt.cpp \
    tesztpad.cpp \
    tessdatapanel.cpp \
    pipe.cpp

RC_FILE = SaVLC.rc

OTHER_FILES += \
    SaVLC.rc

FORMS += \
    tesztpad.ui \
    tessdatapanel.ui
