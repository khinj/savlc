#ifndef SAPOTI_H
#define SAPOTI_H

#include <QtGui>

class SaPoti : public QSlider {

    Q_OBJECT

public:
    SaPoti(Qt::Orientation orientation, QWidget *parent = 0);
    ~SaPoti();
    void resizeEvent(QResizeEvent *e);
    QString getStilus();
    void setStilus(QString st);
    QColorDialog *colorDialog;
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);
public slots:
    void showContextMenu(const QPoint &pt);
    void showSliderszinDialog();
    void showHatterszinDialog();
    void setSliderSzin(QColor col);
    void setHatterSzin(QColor col);


};

#endif // SAPOTI_H
