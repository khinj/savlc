﻿#ifndef SATEXTEDIT_H
#define SATEXTEDIT_H

#include <QTextEdit>
#include <QColorDialog>
#include <QFontDialog>

class SaTextEdit : public QTextEdit
{
    Q_OBJECT
public:
    SaTextEdit(QWidget *parent = 0);
    ~SaTextEdit();
    void setText2(QString st0);
    int feliratIdx; //melyik felirat tartozik ehhez a SaTextEdit-hez
    int sorIdx;     //melyik sort kell megjeleníteni (0: ch, 1: pin, 2: fordítás, 3: mind
    int idx;        //0: 1/ch, 1: 1/pin, 2: 1/ford, 3: 1, 4: 2, 5: idő, 6: Súgó
    QString getStilus();
    void setStilus(QString st);
    void setLathato(bool lathato0);
    bool getLathato();
    void keyPressEvent(QKeyEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseDoubleClickEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    QWidget *keret; //a keret rajzolásához
    void resizeEvent(QResizeEvent *e);
    QColorDialog *colorDialog;
    QFontDialog *fontDialog;
    void saSetStyleSheet(QString par, QString value);
    QColor betuSzin, hatterSzin;
    QMouseEvent *saMouseEvent;
    QList<QPoint> selList;
    void selTorol();
    QString selBeallitas();


private:
    bool lathato;

signals:
    void setTextSignal(QString st);
    void szerkesztesSignal(int idx);

public slots:
    void szerkesztesSlot();
    void showContextMenu(const QPoint &pt);
    void showBetuszinDialog();
    void showHatterszinDialog();
    void showFeliratszinDialog();
    void showFontDialog();
    void cedictKeresSlot();
    void setBetuSzin(QColor col);
    void setBetuSzinDialog(QColor col);
    void setHatterSzin(QColor col);
    void setHatterSzinDialog(QColor col);
    void setSaFont(QFont font);
    void setSaFontDialog(QFont font);
};

#endif // SATEXTEDIT_H
