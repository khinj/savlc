﻿#ifndef FELIRAT_H
#define FELIRAT_H

#include <QObject>
#include <QStringList>

class Felirat : public QObject
{
    Q_OBJECT
public:
    Felirat(QObject *parent = 0);
    QStringList L[4]; //0: ch, 1: pin, 2: ford, 3: Össz
    QList<double> t1, t2;   //felirat start és stop
    enum{
        SEMMI,
        KIIR,
        LEVESZ
    } kovTeendo;
    double kovT;    //mikor van a kovetkező teendő
    int iAkt;   //aktuális felirat index
    int N;  //felirat sorok száma (L.size)
    QString abszolutFajlPath;   //Feliratfájl teljes elérési úttal
    void beolvas();     //abszolutFajlPath felírtafájl beolvasása
    void kezel(double mIdo, int feliratPause, double feliratPauseIdo);
    int keres(double masterPos);
    QString text[4];    //éppen aktuális felírat (0: ch, 1: pin, 2: ford, 3: Össz)
    
signals:
    
public slots:
    
};

#endif // FELIRAT_H
