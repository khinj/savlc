#ifndef OCROPT_H
#define OCROPT_H

#include <QImage>
#include <QPoint>
#include <QLabel>
#include <QThread>
#include "tesztpad.h"
#include "ui_tesztpad.h"

#include <iostream>
#include <memory>
#include <allheaders.h> // leptonica main header for image io
#include <baseapi.h> // tesseract main header

#include "satextedit.h"

class WorkerThread : public QThread
{
    Q_OBJECT
public:
    bool m_abort;
    void run();
};

class OcrOpt
{
public:
    OcrOpt();
    ~OcrOpt();
    QWidget *line1, *line2, *line3, *line4; //feliratkijelölés kerete
    QPoint posPressGlobal, posPress; //feliratkijelölés keretének kezdő sarka (global és w koordináták)
    int vastagsag;          //feliratkijelölés keretének vastagsága
    QImage im1;    //eredeti felirat image
    QImage im2;    //részlegesen optimalizált felirat image
    QImage im3;    //optimalizált felirat image
    tesseract::TessBaseAPI tess;
    QString ocrTxt;

    void setKeretPress(QPoint pos, int vastagsag0);   //keret beállítás mousePress-kor
    void setKeretMove(QPoint pos0);    //keret beállítás mouseMove-kor
    void setKeretRelease(QPoint pos0); //keret beállítás mouseRelease-kor, pm1 feltöltése
    QPoint saMap(QPoint p0);
    void optimalizal();     //kijelölt felirat optimalizálása tesseract-ocr -hez (im1 -> im2)
    void felismer();        //im2 felismerése tess -el
    WorkerThread *workerThread;
    QString kornyekiMxSt(QImage im, int x, int y);
    QColor videoFeliratSzin; //video felirat szín
    quint8 szinHasonlosag(const QColor &c1, const QColor &c2);
    bool isHasonloSzin(const QColor &c1, const QColor &c2, int d);
    bool isHatarSzin(quint8 **mezo, const QImage &img, int x, int y, int x2, int y2, int d);
    quint8 **mezo;
    quint8 **mezo2;
    quint8 **mezoH;
    quint8 **mezoV;
    enum hvHasonlosag{stagnal, novekszik, csokken};
    hvHasonlosag hvHasonlosagFv(QColor c1, QColor c2, int d);
    void nullasokBeallitasa(char mezoHV, int tol, int ig, int k, int mire);
    bool szomszedPontVizsgalat(int i, int j);
    QString tessdataSt;
};

#endif // OCROPT_H
