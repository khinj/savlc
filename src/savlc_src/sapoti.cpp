﻿#include <QRegExp>
#include"samainwindow.h"

extern bool ablakMozgatas;
extern void changePos(double pos);
extern SaMainWindow *w;
extern void setABszakasz();
extern void alapMod();
extern void feliratKezel(bool saLoopHivjaMeg);
extern double Ugras_rovid, Ugras_kozepes, Ugras_hosszu, mesterIdo;
extern QLabel *imgSzerkesztes;
extern QLabel *imgMozgatas;


SaPoti::SaPoti(Qt::Orientation orientation, QWidget *parent) :
        QSlider(orientation, parent) {
    this->hide();
    this->setObjectName("W_10 (Poti)");
    this->setAutoFillBackground(true);
    this->show();
    this->colorDialog = new QColorDialog;
    this->setFocusPolicy(Qt::NoFocus);
}

SaPoti::~SaPoti() {
    delete(this->colorDialog);
}

void SaPoti::resizeEvent(QResizeEvent *e) {
    //this->setGeometry(0, 0, width(), height());
    QString st = this->styleSheet();
    if (ablakMozgatas) {
        st = st.replace(QRegExp("border: \\d*px"), "border: 4px");
    } else {
        st = st.replace(QRegExp("border: \\d*px"), "border: 0px");
    }
    this->setStyleSheet(st);
}

QString SaPoti::getStilus() {
    QString st = this->styleSheet();
    st = st.replace(QRegExp("border: \\d*px solid blue; background: rgba\\("), "");
    st = st.replace("%);", "");
    st = st.replace(", ", ",");
    if (this->isVisible()) {
        st = st + "|1";
    } else {
        st = st + "|0";
    }
    return st;
}

void SaPoti::setStilus(QString st) {
    QStringList stL = st.split("|");
    this->setVisible(stL[1].contains("1"));
    stL = stL[0].split(",");
    QString stStilus = stL[0] + ", " + stL[1] + ", " + stL[2] + ", " + stL[3];
    stStilus = "border: 0px solid blue; background: rgba(" +
            stStilus + "%);";
    this->setStyleSheet(stStilus);
}

void SaPoti::showContextMenu(const QPoint &pt) {
    QMenu *menu = new QMenu;
    menu->addSeparator();
//    QAction *sliderSzinAction = new QAction(QString::fromUtf8("Sliderszín"), this);
//    sliderSzinAction->connect(sliderSzinAction, SIGNAL(triggered()), this, SLOT(showSliderszinDialog()));
//    menu->addAction(sliderSzinAction);
    QAction *hatterSzinAction = new QAction(w->satr("Háttérszín"), this);
    hatterSzinAction->connect(hatterSzinAction, SIGNAL(triggered()), this, SLOT(showHatterszinDialog()));
    menu->addAction(hatterSzinAction);
    QAction *foablakAction = new QAction(w->satr("Főablak keret ki/be"), this);
    foablakAction->connect(foablakAction, SIGNAL(triggered()), w, SLOT(foablakKeretTg()));
    menu->addAction(foablakAction);
    menu->addMenu(w->nyelvMenu);
    menu->addMenu(w->cedictMenu);
    menu->exec(pt);
}

void SaPoti::showSliderszinDialog() {
    colorDialog->setCurrentColor(this->palette().button().color());
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setHatterSzin(QColor)));
    colorDialog->connect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setSliderSzin(QColor)));
    colorDialog->show();
}

void SaPoti::showHatterszinDialog() {
    colorDialog->setCurrentColor(this->palette().color(QPalette::Base));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setSliderSzin(QColor)));
    colorDialog->connect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setHatterSzin(QColor)));
    colorDialog->show();
}

void SaPoti::setSliderSzin(QColor col) {
//    qWarning("setSliderSzin:");
    this->hide();
    QPalette pal = this->palette();
    pal.setColor(QPalette::Window, col);
    this->setPalette(pal);
    this->show();
}

void SaPoti::setHatterSzin(QColor col) {
//    qWarning("setHatterSzin:");
    this->hide();
    QPalette pal = this->palette();
    pal.setColor(QPalette::Window, col);
    this->setPalette(pal);
    //Ez a StyleSheet beállítás:
    QString ssst = this->getStilus();
    ssst =
            QString::number(col.red()) + "," +
            QString::number(col.green()) + "," +
            QString::number(col.blue()) + "," +
            ssst.split(",")[3];
    this->setStilus(ssst);
    this->show();
}

void SaPoti::mousePressEvent(QMouseEvent *event) {
    if (!imgMozgatas->isVisible()) {
        if (event->button() == Qt::LeftButton) {
            changePos(event->x()*1.0 / width());
            feliratKezel(false);
        } else if (event->button() == Qt::MiddleButton) {
            w->player->tgPlayPause();
        } else if (event->button() == Qt::RightButton) {
            this->showContextMenu(event->globalPos());
        } else if (event->button() == Qt::XButton2) {
            alapMod();
            setABszakasz();
            feliratKezel(false);
        }
    }
}

void SaPoti::mouseMoveEvent(QMouseEvent *event) {
    changePos(event->x()*1.0 / width());
    feliratKezel(false);
}

void SaPoti::wheelEvent (QWheelEvent *event) {
    double incr;
    if (event->modifiers() == Qt::ShiftModifier) {
        incr = event->delta() < 0 ? Ugras_kozepes : -Ugras_kozepes;
        w->changePos(mesterIdo + incr);
    } else if (event->modifiers() == Qt::ShiftModifier + Qt::AltModifier) {
        incr = event->delta() < 0 ? Ugras_hosszu : -Ugras_hosszu;
        w->changePos(mesterIdo + incr);
    } else {
        incr = event->delta() < 0 ? Ugras_rovid : -Ugras_rovid;
        w->changePos(mesterIdo + incr);
    }
    feliratKezel(false);

}
