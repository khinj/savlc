﻿#ifndef SAMAINWINDOW_H
#define SAMAINWINDOW_H

#include <QtGui>
#include "savideo.h"
#include "satextedit.h"
#include "sapoti.h"
#include "beallitasform.h"
#include "ocropt.h"

class Sleeper : public QThread
{
public:
    static void usleep(unsigned long usecs);
    static void msleep(unsigned long msecs);
    static void sleep(unsigned long secs);
};

class SaMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    SaMainWindow();

    void closeEvent(QCloseEvent *event);
    //void mouseDoubleClickEvent(QMouseEvent *);
    void keyPressEvent(QKeyEvent *e);
    void mousePressEvent (QMouseEvent *e);
    void mouseMoveEvent (QMouseEvent *e);
    void mouseReleaseEvent (QMouseEvent *e);
    void wheelEvent (QWheelEvent *e);
    void subtitleFileUpdate (int feliratIdx);
    QWidget *egerAlatt(QPoint p);
    QPoint eredetiGlobalPos;
    QWidget *mFr;
    QRect eredetiRect;
    SaVideo *player;
    QList<SaTextEdit*> teL; //ez BRUTÁLIS volt!
                            //      (amikor 5 volt, akkor Debug simán működött, a Release meg lefagyott)
    SaPoti *poti;
    void changePos(double);
    bool foglalt, potiFoglalt;
    enum {balFelso, jobbFelso, jobbAlso, balAlso, felso, jobb, also, bal, kozep, ures} megfogas;
    bool cedictKerdezFoglalt;
    QString pipeNev;
    void mySelectionChanged(SaTextEdit *ablak);
    enum KeretStyle {semmi, mozgat, szerkeszt};
    void setKeretStyle(KeretStyle kt);
    void setEgerKurzor(QCursor);
    QColorDialog *colorDialog;
    static QString nyelv;  //aktuáluis nyelv, pl hun, eng
    static QString nyelvSt;    //aktuális nyelvi fordító file
    static QString satr(QByteArray ba);
    QMenu *nyelvMenu;
    QMenu *cedictMenu;
    void nyelvBeallitas();
    static QString cedictSzotar;  //aktuáluis Cedict szótár, pl FvChToEnNyers|, FvJpToEnNyers|
    OcrOpt *ocr;
    TesztPad *tesztPad;
    bool optFoglalt;
    SaTextEdit *utolsoKijelolesFokusz;
    SaTextEdit *utolsoSzerkesztesFokusz;
    int keresIdx(QList<double> list, double mIdo);
    QPoint cursorStartEndCh;    //azért, hogy a Cedict ablak ne tűnjön el felíratszerkesztés után
    bool kellFeliratMentes;
    bool saMainWindowMouseMoveEventFoglalt;
    QTimer *timerS;
    void kurzorBeallitas();
    QString cedictStilusSt;     //azért, hogy dupla klikk esetén ne állítódjon el a Cedict ablak stílusa
    bool isKijelolesUjSelSepa;
    int cedictAblakState;   //0: eredeti, 1: Cedict ablak mindig látható, 2: Cedict ablaknál van a fókusz
    QScrollBar *vScrollBar, *hScrollBar;
    void resizeEvent(QResizeEvent *e);
    int sbWidth = 15;
    struct {
        int xMin = 0;
        int yMin = 0;
        int xMax = 0;
        int yMax = 0;
    } xyMM;
    void xyMMFv();
    QMutex scrollBarsRefreshMutex;
    QActionGroup* nyelvMenuGroup;
    QActionGroup* cedictMenuGroup;
    void cedictMenuSetChecked(QString cedictDict0);
    Qt::MouseButton mouseButtonPressed;
    void saveIttJarokPos();

public slots:
    void openFileS();
    void changePoti(int);
    void szerkesztesSlot(int);
    void feliratMentMent();
    void feliratMentKilep();
    void cedictKerdez(QString st);
    void setEgerKurzorSlot(QCursor);
    void showContextMenu(const QPoint &pt);
    void showHatterszinDialog();
    void showFeliratszinDialog();
    void setHatterSzin(QColor col);
    void setFeliratSzin(QColor col);
    void foablakKeretTg();
    void nyelvValtas(QString);
    void cedictValtas(QString);
    void optimalizal(int);
    void felismer();
    void felismer2();
    void scrollBarsRefresh();
    void scrollWindowRefresh(int value);

signals:
    void mySelectionChangedSignal(SaTextEdit *ablak);
    void setEgerKurzorSignal(QCursor);
    void nyelvSignal(QString);
    void cedictSignal(QString);
};

#endif // SAMAINWINDOW_H
