#include "tessdatapanel.h"
#include "ui_tessdatapanel.h"

TessdataPanel::TessdataPanel(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TessdataPanel)
{
    ui->setupUi(this);
}

TessdataPanel::~TessdataPanel()
{
    delete ui;
}
