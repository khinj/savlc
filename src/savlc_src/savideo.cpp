﻿#include "savideo.h"
#include "samainwindow.h"

#define qtu( i ) ((i).toUtf8().constData())

void saLoop();
void setABszakasz();
bool isVege;
extern double mesterIdo, videoHossz, vilagossag, elesseg, telitettseg, gamma, tB, hangKesleltetes;
extern int szinHo, hangEro;
extern bool ablakMozgatas;
extern QApplication *app;
extern int pauseMod;
extern void billKezel(QKeyEvent *);
extern SaMainWindow *w;
extern QString exePath, exeName, videoFile;
extern void iAktNullaz();
extern QStringList aspectRatioTomb;
extern int aspectRatioIdx;
extern QLabel *imgSzerkesztes, *imgMozgatas;
extern bool kellRepaint;
extern int startDelay;

SaVideo::SaVideo(QWidget *parent) :
    QFrame(parent)
{
    vlcPlayer = NULL;

    /* Initialize libVLC */
    //vlcInstance = libvlc_new(0, NULL);    //eredeti
    const char * const vlc_args[] = {
                                "--no-sub-autodetect-file", //ne legyen felirat
                                "--no-osd",                 //ne legyen On Screen Display
                                "--no-spu",                 //ne legyen sub-picture
                                "--no-snapshot-preview",    //ne legyen snapshot preview
                                };
    vlcInstance = libvlc_new (sizeof(vlc_args) / sizeof(vlc_args[0]), vlc_args);

    /* Complain in case of broken installation */
    if (vlcInstance == NULL) {
        QMessageBox::critical(this, "Qt libVLC player", "Could not init libVLC");
        exit(1);
    }

    timer = NULL;
    aspectRatioTomb << "alap: " << "16:9" << "4:3" << "1:1" << "16:10" << "221:100" <<
            "235:100" << "239:100" << "5:4";
    aspectRatioIdx = 0;
    this->setObjectName("W_11 (Player)");
    initUI();
    this->keret = new QWidget(this);
    this->keret->setObjectName("keretObject");
    this->keret->setStyleSheet("");
    this->keret->setEnabled(false);
    this->keret->show();
    this->setStyleSheet("border: 4px solid blue; background: darkorange;");
}

SaVideo::~SaVideo() {
    /* Release libVLC instance on quit */
    if (vlcInstance) {
        libvlc_release(vlcInstance);
    }
    delete(timer);
}

void SaVideo::initUI() {
    ;
}

void SaVideo::openFile(QString fileOpen0) {
    if (timer) {
        timer->stop();
    }
    videoHossz = 0.0;
    /* Stop if something is playing */
    if (vlcPlayer && libvlc_media_player_is_playing(vlcPlayer)) {
        finish();
    }
    QString fileOpen = fileOpen0.replace("/", "\\\\");  //sa: ez kell!

    //qWarning(QString::fromUtf8("Video fájl: ").toLatin1() + fileOpen.toUtf8());

    /* Create a new Media */
    libvlc_media_t *vlcMedia = libvlc_media_new_path(vlcInstance, qtu(fileOpen));
    if (!vlcMedia) {
        return;
    }

    /* Create a new libvlc player */
    vlcPlayer = libvlc_media_player_new_from_media (vlcMedia);

    /* Release the media */
    libvlc_media_release(vlcMedia);

    /* Integrate the video in the interface */
    libvlc_media_player_set_hwnd(vlcPlayer, this->keret->winId());

    libvlc_video_set_mouse_input(vlcPlayer, false);     //nem jól működik
    libvlc_video_set_key_input(vlcPlayer, false);     //?
    w->setObjectName(exeName + " - " + QFileInfo(videoFile).fileName());
    w->setWindowTitle(exeName + " - " + QFileInfo(videoFile).fileName());
    this->changeVolume(50);

    libvlc_video_set_adjust_int(vlcPlayer, libvlc_adjust_Enable, 1);

    do {
        Sleeper::msleep(600);
        kezdet();
    } while (videoHossz<1.0);

    setVilagossag(vilagossag);
    setElesseg(elesseg);
    setTelitettseg(telitettseg);
    setGamma(gamma);
    setSzinHo(szinHo);
    changeVolume(hangEro);
    setHangKesleltetes(hangKesleltetes);
    setAspectRatio();
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(saUpdate()));

    //aspectRatioTomb[0] beállítása:
    vWidth = libvlc_video_get_width(vlcPlayer);
    vHeight = libvlc_video_get_height(vlcPlayer);
    QString st = QString::number((int) (100.0 * vWidth / vHeight)) + ":100";
//    qWarning("vWidth: " + QString::number(vWidth).toUtf8() +
//            ", vHeight: " + QString::number(vHeight).toUtf8() + ", st = " + st.toUtf8());
    aspectRatioTomb[0] = aspectRatioTomb[0] + st;
    kellRepaint = false;

    timer->start(100);
}

void SaVideo::tgPlayPause() {
    if (!vlcPlayer) {
        return;
    }
    if (libvlc_media_player_is_playing(vlcPlayer)) {
        pause();
    } else if (imgSzerkesztes->isHidden() && !isVege) {
        play();
    }
}

void SaVideo::play() {
    libvlc_media_player_play(vlcPlayer);
    pauseMod = 0;

    //Hide mouse pointer at the bottom of the screen:
    QCursor c = w->cursor();
    int w = QApplication::desktop()->screenGeometry().width() / 2;
    int h = QApplication::desktop()->screenGeometry().height();
    c.setPos((QPoint(w, h)));
    c.setShape(Qt::BlankCursor);
    setCursor(c);
}

void SaVideo::pause() {
    if (libvlc_media_player_is_playing(vlcPlayer)) {
        libvlc_media_player_pause(vlcPlayer);   // libvlc_media_player_stop(vlcPlayer) not good here
    }
    pauseMod = 1;
    //Egér mutatás:
    QCursor c = w->cursor();
    c.setShape(Qt::ArrowCursor);
    setCursor(c);
}


int SaVideo::changeVolume(int vol) { /* Called on volume slider change */
    if (vlcPlayer)
        return libvlc_audio_set_volume (vlcPlayer, vol);
    return 0;
}

double SaVideo::getPos() {
    double er = -1.0;
    if (vlcPlayer) {
        er = libvlc_media_player_get_time(vlcPlayer) / 1000.0;
    }
    return er;
}

void SaVideo::setPos(double pos0) {
    if (vlcPlayer) {
        //qWarning("setPos: pos0 = %f", pos0);
        double pos = qMin(pos0, videoHossz - 1.0);
        //qWarning("setPos: pos = %f, videoHossz = %f", pos, videoHossz);
        libvlc_media_player_set_position(vlcPlayer, pos / videoHossz);
        mesterIdo = pos;
    }
}

void SaVideo::saUpdate() {
    if (!vlcPlayer)
        return;

    // Stop the media
    if (mesterIdo >= videoHossz - 0.4) {
        if (!isVege) {
            libvlc_media_player_stop(vlcPlayer);
            Sleeper::msleep(300);
            libvlc_media_player_play(vlcPlayer);
            Sleeper::msleep(300);
            libvlc_media_player_pause(vlcPlayer);
            libvlc_media_player_set_position(vlcPlayer, (videoHossz - 0.4) / videoHossz);
            Sleeper::msleep(300);
            pauseMod = 1;
            isVege = true;
        }
    } else {
        isVege = false;
        saLoop();
    }
}

void SaVideo::finish() {
    if(vlcPlayer) {
        if (libvlc_media_player_is_playing(vlcPlayer))
        {
            /* Pause */
            libvlc_media_player_pause(vlcPlayer);
            libvlc_media_player_play(vlcPlayer);
        }

        //stop the media player
        libvlc_media_player_stop(vlcPlayer);

        //release the media player
        libvlc_media_player_release(vlcPlayer);

        //Reset application values
        vlcPlayer = NULL;
    }
}

void SaVideo::about()
{
    QMessageBox::about(this, "SaVLC Qt és libVLC alapon", QString::fromUtf8(libvlc_get_version()) );
}

void SaVideo::tgFullscreen() {
    if (!imgSzerkesztes->isVisible()) {
        if (w->isFullScreen()) {
            //qWarning("FULL -> nem FULL");
            w->setWindowFlags(eredetiFlag);
            w->setGeometry(eredetiRectWin);
            this->setGeometry(eredetiRectVideo);
            this->lower();
            if (eredetiMax) {
                w->showMaximized();
            } else {
                w->showNormal();
            }
        }
        else {
            //qWarning("nem FULL -> FULL");
            eredetiFlag = w->windowFlags();
            eredetiRectVideo = this->geometry();
            eredetiRectWin = w->geometry();
            eredetiMax = w->isMaximized();
            w->setWindowFlags(Qt::Window);
            w->showFullScreen();
            this->setGeometry(QApplication::desktop()->screenGeometry());
        }
    }
}

void SaVideo::closeEvent(QCloseEvent *event) {
    finish();
    event->accept();
}

void SaVideo::setVilagossag(double vilagossag) {
    libvlc_video_set_adjust_float(vlcPlayer, libvlc_adjust_Brightness, vilagossag);
}

void SaVideo::setElesseg(double elesseg) {
    libvlc_video_set_adjust_float(vlcPlayer, libvlc_adjust_Contrast, elesseg);
}

void SaVideo::setTelitettseg(double telitettseg) {
    libvlc_video_set_adjust_float(vlcPlayer, libvlc_adjust_Saturation, telitettseg);
}

void SaVideo::setGamma(double gamma) {
    libvlc_video_set_adjust_float(vlcPlayer, libvlc_adjust_Gamma, gamma);
}

void SaVideo::setSzinHo(int szinHo0) { //[0 - 360)
    libvlc_video_set_adjust_int(vlcPlayer, libvlc_adjust_Hue, szinHo0);
}

void SaVideo::setSebesseg(double sebesseg) {
    //qWarning("sebesseg = %f", sebesseg);
    libvlc_media_player_set_rate(vlcPlayer, sebesseg);
}

void SaVideo::kezdet() {
    videoHossz = libvlc_media_player_get_length(vlcPlayer) / 1000.0;
    this->resizeEvent(0);

    //Egér legyen képernyő legalján:
    QCursor c = w->cursor();
    int w = QApplication::desktop()->screenGeometry().width() / 2;
    int h = QApplication::desktop()->screenGeometry().height();
    c.setPos((QPoint(w, h)));
    setCursor(c);

    /* Start playback */
    libvlc_media_player_play(vlcPlayer);

}

void SaVideo::resizeEvent(QResizeEvent *e) {
    if (ablakMozgatas && !w->isFullScreen()) {
        this->keret->setGeometry(8, 8, this->width()-16, this->height()-16);
    } else {
        this->keret->setGeometry(0, 0, this->width()-0, this->height()-0);
    }
}

void SaVideo::setHangKesleltetes(double hk) {
    int64_t t = hk*1000000.0;
    //qWarning("setHangKesleltetes = %f", t/1000000.0);
    libvlc_audio_set_delay(vlcPlayer, t);
}

void SaVideo::setNextFrame() {
    libvlc_media_player_next_frame(vlcPlayer);
}

void SaVideo::keyPressEvent(QKeyEvent *e) {
    //qWarning("SaVideo::keyPressEvent:");
    billKezel(e);
    //w->setFocus();
}

void SaVideo::setAspectRatio() {
    if (aspectRatioIdx >= aspectRatioTomb.size()) {
        aspectRatioIdx = 0;
    } else if (aspectRatioIdx < 0) {
        aspectRatioIdx = aspectRatioTomb.size()-1;
    }
    if (aspectRatioIdx==0) {
        QString st = aspectRatioTomb[aspectRatioIdx];
        st = st.replace("alap: ", "");
        libvlc_video_set_aspect_ratio(vlcPlayer, st.toLatin1().data());
    } else {
        libvlc_video_set_aspect_ratio(vlcPlayer, aspectRatioTomb[aspectRatioIdx].toLatin1().data());
    }
    //char *ch = libvlc_video_get_aspect_ratio(vlcPlayer);
    //QString as = ch;
    //libvlc_free(ch);
    //qWarning("aspectRatio = " + as.toUtf8() + " , idx = " + QString::number(aspectRatioIdx).toUtf8());
}

void SaVideo::mousePressEvent(QMouseEvent *e) {
    //Egér mutatás:
    QCursor c = this->cursor();
    c.setShape(Qt::ArrowCursor);
    setCursor(c);

    ((SaMainWindow*) w)->mousePressEvent(e);
}

void SaVideo::mouseMoveEvent(QMouseEvent *e) {
    //qWarning("SaVideo::mouseMoveEvent:");
    ((SaMainWindow*) w)->mouseMoveEvent(e);
}

void SaVideo::mouseReleaseEvent(QMouseEvent *e) {
    ((SaMainWindow*) w)->mouseReleaseEvent(e);
}

bool SaVideo::takeSnapshot(QString file) {
    libvlc_video_set_adjust_int(vlcPlayer, libvlc_adjust_Enable, 0);
    bool b = libvlc_video_take_snapshot(vlcPlayer, 0, file.toUtf8(), 0, 0);
    libvlc_video_set_adjust_int(vlcPlayer, libvlc_adjust_Enable, 1);
    return b;
}

bool SaVideo::takeSnapshotOCR() {
    QString st = exePath + "../resource/" + "/tmp.png";
    return takeSnapshot(st);
}
