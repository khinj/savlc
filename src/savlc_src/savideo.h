#ifndef PLAYER
#define PLAYER

#include <QtGui>
#include <vlc/vlc.h>

class SaVideo : public QFrame {

    Q_OBJECT

public:
    SaVideo(QWidget *parent = 0);
    virtual ~SaVideo();

    libvlc_instance_t *vlcInstance;
    libvlc_media_player_t *vlcPlayer;
    int vWidth, vHeight;

    virtual void closeEvent(QCloseEvent*);
    void initUI();
    void setVilagossag(double);
    void setElesseg(double);
    void setTelitettseg(double);
    void setGamma(double);
    void setSzinHo(int);
    void setSebesseg(double);
    void setAspectRatio();
    double getPos();
    void kezdet();
    void resizeEvent(QResizeEvent *e);
    void keyPressEvent(QKeyEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    QRect eredetiRectWin, eredetiRectVideo;
    Qt::WindowFlags eredetiFlag;
    bool eredetiMax;
    QTimer *timer;
    QWidget *keret;
    bool takeSnapshotOCR();
    bool takeSnapshot(QString file);
    void play();
    void pause();

public slots:
    void openFile(QString);
    void tgPlayPause();
    void finish();
    void about();
    void tgFullscreen();
    void setHangKesleltetes(double);
    int changeVolume(int);
    void setPos(double);
    void saUpdate();
    void setNextFrame();
};

#endif
