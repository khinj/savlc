#include <math.h>
#include "samainwindow.h"
#include "ocropt.h"
#include "sadefs.h"

extern SaMainWindow *w;
extern QString exePath, exeName, openedFileName;

void WorkerThread::run() {
//    qWarning("WorkerThread::run():");
    m_abort = false;
    w->ocr->optimalizal();
}

OcrOpt::OcrOpt() {
    line1 = new QWidget();
    line1->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
    line2 = new QWidget();
    line2->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
    line3 = new QWidget();
    line3->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
    line4 = new QWidget();
    line4->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);

    // setup
    //tess.SetPageSegMode(tesseract::PSM_AUTO);
    tess.SetPageSegMode(tesseract::PSM_SINGLE_LINE);
    tess.SetVariable("save_best_choices", "T");
    workerThread = new WorkerThread();
}

OcrOpt::~OcrOpt() {
    line1->hide();
    line2->hide();
    line3->hide();
    line4->hide();
    delete(line1);
    delete(line2);
    delete(line3);
    delete(line4);
}

void OcrOpt::setKeretPress(QPoint pos, int vastagsag0) {   //keret beállítás mousePress-kor
    vastagsag = vastagsag0;
    QString st = "QWidget {border: " + QString::number(vastagsag) + "px dotted darkblue; ";
    st += "background: yellow; }";
    line1->setStyleSheet(st);
    line2->setStyleSheet(st);
    line3->setStyleSheet(st);
    line4->setStyleSheet(st);
    posPressGlobal = pos;
    posPress = w->mapFromGlobal(pos);
    line1->setGeometry(posPress.x(), posPress.y(), vastagsag, vastagsag);
    line2->setGeometry(posPress.x(), posPress.y(), vastagsag, vastagsag);
    line3->setGeometry(posPress.x(), posPress.y(), vastagsag, vastagsag);
    line4->setGeometry(posPress.x(), posPress.y(), vastagsag, vastagsag);
    line1->setParent(w);
    line2->setParent(w);
    line3->setParent(w);
    line4->setParent(w);
    line1->show();
    line2->show();
    line3->show();
    line4->show();
    line1->raise();
    line2->raise();
    line3->raise();
    line4->raise();
}

void OcrOpt::setKeretMove(QPoint pos0) {    //keret beállítás mouseMove-kor
    QPoint pos = w->mapFromGlobal(pos0);
    int x1 = qMin(posPress.x(), pos.x());
    int x2 = qMax(posPress.x(), pos.x());
    int y1 = qMin(posPress.y(), pos.y());
    int y2 = qMax(posPress.y(), pos.y());
    line1->setGeometry(x1, y1, x2 - x1, vastagsag);
    line2->setGeometry(x2 - vastagsag, y1 + vastagsag, vastagsag, y2 - y1 - vastagsag);
    line3->setGeometry(x1, y2 - vastagsag, x2 - x1 - vastagsag, vastagsag);
    line4->setGeometry(x1, y1 + vastagsag, vastagsag, y2 - y1 - vastagsag);
}

void OcrOpt::setKeretRelease(QPoint pos0) { //keret beállítás mouseRelease-kor, pm1 feltöltése
    line1->hide();
    line2->hide();
    line3->hide();
    line4->hide();

    QPoint pos = w->mapFromGlobal(pos0);
    //globál koordináták:
    int x1 = qMin(posPress.x(), pos.x());
    int x2 = qMax(posPress.x(), pos.x());
    int y1 = qMin(posPress.y(), pos.y());
    int y2 = qMax(posPress.y(), pos.y());

    if (qAbs(x1 - x2) > vastagsag *2 || qAbs(y1 - y2) > vastagsag *2) {
        QFile::remove(exePath + "../resource/" + "tmp.png");
        QFile::remove(exePath + "../resource/" + "tmp2.bmp");
        posPress = QPoint(-1, -1);
        w->player->takeSnapshotOCR();

        //koordináták im-en:
        QPoint p;
        p = this->saMap(QPoint(x1, y1));
        x1 = p.x();
        y1 = p.y();
        p = this->saMap(QPoint(x2, y2));
        x2 = p.x();
        y2 = p.y();

        QImage im = QImage(exePath + "../resource/" + "tmp.png");
        im1 = im.copy(x1, y1, x2-x1, y2-y1);

        optimalizal();
    }
}

void OcrOpt::optimalizal() {     //kijelölt felirat optimalizálása tesseract-ocr -hez (im1 -> im3)
    //qWarning("OcrOpt::optimalizal:");

//    im1 = QImage(exePath + "../resource/" + "tmp_i.png");    //teszteléshez

    w->tesztPad->ui->label_1->setPixmap(QPixmap::fromImage(
            im1.scaled(w->tesztPad->ui->label_1->size(),
            Qt::KeepAspectRatio)));
    im2 = im1.copy(im1.rect());
    im3 = im1.copy(im1.rect());
    int pot1 = w->tesztPad->ui->pot1->value();
    int pot2 = w->tesztPad->ui->pot2->value();
    int pot3 = w->tesztPad->ui->pot3->value();
    int pot4 = w->tesztPad->ui->pot4->value();
    int pot5 = w->tesztPad->ui->pot5->value();
    int pot6 = w->tesztPad->ui->pot6->value();
    int pot7 = w->tesztPad->ui->pot7->value();
    int pot8 = w->tesztPad->ui->pot8->value();
    int pot9 = w->tesztPad->ui->pot9->value();
    int pot10 = w->tesztPad->ui->pot10->value();
    w->tesztPad->ui->pot1->setToolTip(w->satr("Mit rajzoljon ki im3-ként"));
    w->tesztPad->ui->pot2->setToolTip(w->satr("Határszínkülönbség"));
    w->tesztPad->ui->pot3->setToolTip(w->satr("Video feliratszín különbség biztos igen"));
    w->tesztPad->ui->pot4->setToolTip(w->satr("Video feliratszín különbség biztos nem"));
    w->tesztPad->ui->pot5->setToolTip(w->satr("mezoH/V max delta"));
    w->tesztPad->ui->pot6->setToolTip(w->satr(
            "mezoH/V egymást követő mélypontok közötti max táv"));
    w->tesztPad->ui->pot7->setToolTip(w->satr("Egyengetés számlálója"));
    w->tesztPad->ui->pot8->setToolTip(w->satr(""));
    w->tesztPad->ui->pot9->setToolTip(w->satr(""));
    w->tesztPad->ui->pot10->setToolTip(w->satr(""));

    mezo = (quint8**) malloc(sizeof(quint8*) * im1.width());
    mezo2 = (quint8**) malloc(sizeof(quint8*) * im1.width());
    mezoH = (quint8**) malloc(sizeof(quint8*) * im1.width());
    mezoV = (quint8**) malloc(sizeof(quint8*) * im1.width());
    for (int i=0; i<im1.width(); i++) {
        mezo[i] = (quint8*) malloc(sizeof(quint8) * im1.height());
        mezo2[i] = (quint8*) malloc(sizeof(quint8) * im1.height());
        mezoH[i] = (quint8*) malloc(sizeof(quint8) * im1.height());
        mezoV[i] = (quint8*) malloc(sizeof(quint8) * im1.height());
    }

    //mezo[i][j] kódok:
    //0: nincs feldolgozva / bizonytalan
    //1: határszín
    //2: biztosan video felirat szín
    //3: biztosan nem video felirat szín

    //mezoH/V[i][j] kódok:
    //0: nincs feldolgozva / bizonytalan
    //1: mélypont
    //2: növekszik
    //3: tetőpont
    //4: csökken

    //mezo nullázása, mezo2 beállítása:
    for (int j=0; j<im1.height(); j++) {
        for (int i=0; i<im1.width(); i++) {
            mezo[i][j] = 0;
            mezo2[i][j] = szinHasonlosag(QColor(im1.pixel(i, j)), videoFeliratSzin);
        }
    }

    //szélek beállítása határszínre:
    for (int i=0; i<im1.width(); i++) {
        mezo[i][0] = 1;
        mezo[i][im1.height()-1] = 1;
    }
    for (int i=0; i<im1.height(); i++) {
        mezo[0][i] = 1;
        mezo[im1.width()-1][i] = 1;
    }

    bool vanValtozas;

    //határszín beállítása:
    do {
        vanValtozas = false;
        for (int j=1; j<im1.height()-1; j++) {
            for (int i=1; i<im1.width()-1; i++) {
                if (workerThread->m_abort) {
                    return;
                }
                if (mezo[i][j] == 0 && (
                        isHatarSzin(mezo, im1, i, j, i-1, j-1, pot2) ||
                        isHatarSzin(mezo, im1, i, j, i,   j-1, pot2) ||
                        isHatarSzin(mezo, im1, i, j, i+1, j-1, pot2) ||
                        isHatarSzin(mezo, im1, i, j, i-1, j,   pot2) ||
                        isHatarSzin(mezo, im1, i, j, i+1, j,   pot2) ||
                        isHatarSzin(mezo, im1, i, j, i-1, j+1, pot2) ||
                        isHatarSzin(mezo, im1, i, j, i,   j+1, pot2) ||
                        isHatarSzin(mezo, im1, i, j, i+1, j+1, pot2))) {
                    mezo[i][j] = 1;
                    vanValtozas = true;
                }
            }
        }
    } while (vanValtozas);

    //biztos video felirat szín, biztos nem video felirat szín beállítása:
    for (int j=1; j<im1.height()-1; j++) {
        for (int i=1; i<im1.width()-1; i++) {
            if (workerThread->m_abort) {
                return;
            }
            if (mezo[i][j] == 0) {
                if (isHasonloSzin(this->videoFeliratSzin, QColor(im1.pixel(i, j)), pot3)) {
                    mezo[i][j] = 2;
                } else if (!isHasonloSzin(this->videoFeliratSzin, QColor(im1.pixel(i, j)), pot4)) {
                    mezo[i][j] = 3;
                }
            }
        }
    }

    #define TINTASZIN QColor(0, 0, 0)       //tinta
    #define PAPIRSZIN QColor(255, 255, 255) //papír
    #define SZIN0 QColor(255, 0, 0)         //nincs feldolgozva / bizonytalan
    #define SZIN1 QColor(127, 127, 255)     //határszín
    #define SZIN2 QColor(0, 0, 0)           //biztosan video felirat szín
    #define SZIN3 QColor(255, 255, 255)     //biztosan nem video felirat szín
    #define SZINNOVEKSZIK QColor(155, 155, 0)     //mezoH/V-ben növekedés
    #define SZINCSOKKEN QColor(0, 155, 155)     //mezoH/V-ben csokkenés

    //mezo kirajzolása tesztpadon:
    for (int j=0; j<im1.height(); j++) {
        for (int i=0; i<im1.width(); i++) {
            if (workerThread->m_abort) {
                return;
            }
            switch (mezo[i][j]) {
                case 0:                     //nincs feldolgozva / bizonytalan
                    im2.setPixel(i, j, SZIN0.rgb());
                    break;
                case 1:                     //határszín
                    im2.setPixel(i, j, SZIN1.rgb());
                    break;
                case 2:                     //biztosan video felirat szín
                    im2.setPixel(i, j, SZIN2.rgb());
                    break;
                case 3:                     //biztosan nem video felirat szín
                    im2.setPixel(i, j, SZIN3.rgb());
                    break;
                default:
                    qWarning("hiba switch_1:");
            }
        }
    }
    w->tesztPad->ui->label_2->setPixmap(QPixmap::fromImage(
            im2.scaled(w->tesztPad->ui->label_2->size(),
            Qt::KeepAspectRatio)));

    //mezo2 kirajzolása tesztpadon:
    if (pot1 == 0) {
        for (int j=0; j<im1.height(); j++) {
            for (int i=0; i<im1.width(); i++) {
                im3.setPixel(i, j, QColor(mezo2[i][j], mezo2[i][j], mezo2[i][j]).rgb());
            }
        }
        w->tesztPad->ui->label_3->setPixmap(QPixmap::fromImage(
                im3.scaled(w->tesztPad->ui->label_3->size(),
                Qt::KeepAspectRatio)));
    }

    //mezo2 kirajzolása tesztpadon határszín figyelembe vételével:
    if (pot1 == 1) {
        for (int j=0; j<im1.height(); j++) {
            for (int i=0; i<im1.width(); i++) {
                if (mezo[i][j] == 1) {  //határszín
                    im3.setPixel(i, j, SZIN3.rgb());
                } else {
                    im3.setPixel(i, j, QColor(mezo2[i][j], mezo2[i][j], mezo2[i][j]).rgb());
                }
            }
        }
        w->tesztPad->ui->label_3->setPixmap(QPixmap::fromImage(
                im3.scaled(w->tesztPad->ui->label_3->size(),
                Qt::KeepAspectRatio)));
    }

    //mezoH beállítása:
    for (int j=0; j<im1.height(); j++) {
        int elsoBizonytalanHely = 1;
        for (int i=0; i<im1.width(); i++) {
            if (workerThread->m_abort) {
                return;
            }
            if (i == 0) {
                mezoH[i][j] = 1;
            } else if (i == 1) {
                mezoH[i][j] = 0;
            } else {
                if (mezoH[i-1][j] == 1 || mezoH[i-1][j] == 3) {
                    qWarning("OcrOpt:: mezoH ertek hiba1: %d", mezoH[i-1][j]);
                    break;
                }
                hvHasonlosag h = hvHasonlosagFv(QColor(im1.pixel(i-1, j)),
                        QColor(im1.pixel(i, j)), pot5);
                if (i == im1.width()-1) {
                    if (h == novekszik) {
                        if (mezoH[i-1][j] == 0 || mezoH[i-1][j] == 4) {
                            nullasokBeallitasa('H', elsoBizonytalanHely, i-2, j, 1);
                        }
                        mezoH[i-1][j] = 3;
                    } else if (h == csokken) {
                        if (mezoH[i-1][j] == 0 || mezoH[i-1][j] == 2) {
                            nullasokBeallitasa('H', elsoBizonytalanHely, i-1, j, 3);
                        }
                    } else {        //stagnál:
                        if (mezoH[i-1][j] == 0) {
                            if (elsoBizonytalanHely == 1) {
                                nullasokBeallitasa('H', elsoBizonytalanHely, i-1, j, 1);
                            } else {
                                qWarning("OcrOpt:: mezoH ertek hiba2: %d", mezoH[i-1][j]);
                                break;
                            }
                        } else if (mezoH[i-1][j] == 2) {
                            nullasokBeallitasa('H', elsoBizonytalanHely, i-1, j, 3);
                        } else if (mezoH[i-1][j] == 4) {
                            nullasokBeallitasa('H', elsoBizonytalanHely, i-1, j, 1);
                        } else {
                            qWarning("OcrOpt:: mezoH ertek hiba3: %d", mezoH[i-1][j]);
                            break;
                        }
                    }
                    mezoH[i][j] = 1;
                    elsoBizonytalanHely = i;
                } else {    //0 < i < im1.width()-1:
                    if (h == novekszik) {
                        if (mezoH[i-1][j] == 0 || mezoH[i-1][j] == 4) {
                            nullasokBeallitasa('H', elsoBizonytalanHely, i-1, j, 1);
                        }
                        mezoH[i][j] = 2;
                        elsoBizonytalanHely = i;
                    } else if (h == csokken) {
                        if (mezoH[i-1][j] == 0 || mezoH[i-1][j] == 2) {
                            nullasokBeallitasa('H', elsoBizonytalanHely, i-1, j, 3);
                        }
                        mezoH[i][j] = 4;
                        elsoBizonytalanHely = i;
                    } else {        //stagnál:
                        mezoH[i][j] = mezoH[i-1][j];
                    }
                }
            }
        }
    }

    //mezoH mélypontok közötti táv ell.:
    for (int j=0; j<im1.height(); j++) {
        int utolsoMelypont = 0;
        int i = 1;
        while (i < im1.width()) {
            if (workerThread->m_abort) {
                return;
            }
            if (mezoH[i][j] == 1 || i == im1.width()-1) {
                if (i - utolsoMelypont > pot6) {
                    nullasokBeallitasa('H', utolsoMelypont, i, j, 1);
                }
                utolsoMelypont = i;
            }
            i++;
        }
    }

    //mezoH kirajzolása tesztpadon:
    if (pot1 == 2) {
        for (int j=0; j<im1.height(); j++) {
            for (int i=0; i<im1.width(); i++) {
                if (mezoH[i][j] == 0) {
                    im3.setPixel(i, j, SZIN0.rgb());
                } else if (mezoH[i][j] == 1) {
                    im3.setPixel(i, j, PAPIRSZIN.rgb());
                } else if (mezoH[i][j] == 2) {
                    im3.setPixel(i, j, SZINNOVEKSZIK.rgb());
                } else if (mezoH[i][j] == 3) {
                    im3.setPixel(i, j, TINTASZIN.rgb());
                } else if (mezoH[i][j] == 4) {
                    im3.setPixel(i, j, SZINCSOKKEN.rgb());
                }
            }
        }
        w->tesztPad->ui->label_3->setPixmap(QPixmap::fromImage(
                im3.scaled(w->tesztPad->ui->label_3->size(),
                Qt::KeepAspectRatio)));
    }

    //mezoV beállítása:
    for (int i=0; i<im1.width(); i++) {
        int elsoBizonytalanHely = 1;
        for (int j=0; j<im1.height(); j++) {
            if (workerThread->m_abort) {
                return;
            }
            if (j == 0) {
                mezoV[i][j] = 1;
            } else if (j == 1) {
                mezoV[i][j] = 0;
            } else {
                if (mezoV[i][j-1] == 1 || mezoV[i][j-1] == 3) {
                    qWarning("OcrOpt:: mezoV ertek hiba1: %d", mezoV[i][j-1]);
                    break;
                }
                hvHasonlosag h = hvHasonlosagFv(QColor(im1.pixel(i, j-1)),
                        QColor(im1.pixel(i, j)), pot5);
                if (j == im1.height()-1) {
                    if (h == novekszik) {
                        if (mezoV[i][j-1] == 0 || mezoV[i][j-1] == 4) {
                            nullasokBeallitasa('V', elsoBizonytalanHely, j-2, i, 1);
                        }
                        mezoV[i][j-1] = 3;
                    } else if (h == csokken) {
                        if (mezoV[i][j-1] == 0 || mezoV[i][j-1] == 2) {
                            nullasokBeallitasa('V', elsoBizonytalanHely, j-1, i, 3);
                        }
                    } else {        //stagnál:
                        if (mezoV[i][j-1] == 0) {
                            if (elsoBizonytalanHely == 1) {
                                nullasokBeallitasa('V', elsoBizonytalanHely, j-1, i, 1);
                            } else {
                                qWarning("OcrOpt:: mezoV ertek hiba2: %d", mezoV[i][j-1]);
                                break;
                            }
                        } else if (mezoV[i][j-1] == 2) {
                            nullasokBeallitasa('V', elsoBizonytalanHely, j-1, i, 3);
                        } else if (mezoV[i][j-1] == 4) {
                            nullasokBeallitasa('V', elsoBizonytalanHely, j-1, i, 1);
                        } else {
                            qWarning("OcrOpt:: mezoV ertek hiba3: %d", mezoV[i][j-1]);
                            break;
                        }
                    }
                    mezoV[i][j] = 1;
                    elsoBizonytalanHely = j;
                } else {    //0 < i < im1.height()-1:
                    if (h == novekszik) {
                        if (mezoV[i][j-1] == 0 || mezoV[i][j-1] == 4) {
                            nullasokBeallitasa('V', elsoBizonytalanHely, j-1, i, 1);
                        }
                        mezoV[i][j] = 2;
                        elsoBizonytalanHely = j;
                    } else if (h == csokken) {
                        if (mezoV[i][j-1] == 0 || mezoV[i][j-1] == 2) {
                            nullasokBeallitasa('V', elsoBizonytalanHely, j-1, i, 3);
                        }
                        mezoV[i][j] = 4;
                        elsoBizonytalanHely = j;
                    } else {        //stagnál:
                        mezoV[i][j] = mezoV[i][j-1];
                    }
                }
            }
        }
    }

    //mezoV mélypontok közötti táv ell.:
    for (int i=0; i<im1.width(); i++) {
        int utolsoMelypont = 0;
        int j = 1;
        while (j < im1.height()) {
            if (workerThread->m_abort) {
                return;
            }
            if (mezoV[i][j] == 1 || j == im1.height()-1) {
                if (j - utolsoMelypont > pot6) {
                    nullasokBeallitasa('V', utolsoMelypont, j, i, 1);
                }
                utolsoMelypont = j;
            }
            j++;
        }
    }

    //mezoV kirajzolása tesztpadon:
    if (pot1 == 3) {
        for (int j=0; j<im1.height(); j++) {
            for (int i=0; i<im1.width(); i++) {
                if (mezoV[i][j] == 0) {
                    im3.setPixel(i, j, SZIN0.rgb());
                } else if (mezoV[i][j] == 1) {
                    im3.setPixel(i, j, PAPIRSZIN.rgb());
                } else if (mezoV[i][j] == 2) {
                    im3.setPixel(i, j, SZINNOVEKSZIK.rgb());
                } else if (mezoV[i][j] == 3) {
                    im3.setPixel(i, j, TINTASZIN.rgb());
                } else if (mezoV[i][j] == 4) {
                    im3.setPixel(i, j, SZINCSOKKEN.rgb());
                }
            }
        }
        w->tesztPad->ui->label_3->setPixmap(QPixmap::fromImage(
                im3.scaled(w->tesztPad->ui->label_3->size(),
                Qt::KeepAspectRatio)));
    }

    //3x3-as környezet vizsgálata, itt már nem használjuk a mezo[][] -t:
    QColor cAkt, c;
    int d1; //
    int d2;
    #ifndef OSSZEVET
    #define OSSZEVET \
        \

    #endif
    im3 = im2.copy(im2.rect());     //im3 létrehozása
    do {
        vanValtozas = false;
        for (int j=0; j<im1.height(); j++) {
            for (int i=0; i<im1.width(); i++) {
                if (workerThread->m_abort) {
                    return;
                }
                if (im2.pixel(i, j) == SZIN1.rgb() ||
                        im2.pixel(i, j) == SZIN3.rgb()) {       //nem video felirat szín:
                    im3.setPixel(i, j, SZIN3.rgb());
                } else if (im2.pixel(i, j) == SZIN2.rgb()) {    //video felirat szín:
                    im3.setPixel(i, j, SZIN2.rgb());
                } else if (im2.pixel(i, j) == SZIN0.rgb()){     //bizonytalan:
                    bool t = false;
                    t = t || szomszedPontVizsgalat(i-1, j-1);
                    t = t || szomszedPontVizsgalat(i  , j-1);
                    t = t || szomszedPontVizsgalat(i+1, j-1);
                    t = t || szomszedPontVizsgalat(i-1, j  );
                    t = t || szomszedPontVizsgalat(i+1, j  );
                    t = t || szomszedPontVizsgalat(i-1, j+1);
                    t = t || szomszedPontVizsgalat(i  , j+1);
                    t = t || szomszedPontVizsgalat(i+1, j+1);
                    if (t) {
                        im3.setPixel(i, j, SZIN2.rgb());
                    } else {
                        im3.setPixel(i, j, SZIN3.rgb());
                    }
                }
            }
        }
        im2 = im3.copy(im3.rect());
    } while (vanValtozas);

    if (pot1 == 4) {
        //im3 kirajzolása tesztpadon:
        w->tesztPad->ui->label_3->setPixmap(QPixmap::fromImage(
                im3.scaled(w->tesztPad->ui->label_3->size(),
                Qt::KeepAspectRatio)));
    }

    //papírszín - tintaszín beállítsa:
    for (int j=0; j<im3.height(); j++) {
        for (int i=0; i<im3.width(); i++) {
            if (workerThread->m_abort) {
                return;
            }
            QColor c(im3.pixel(i, j));
            if (c == SZIN2) {
                im3.setPixel(i, j, TINTASZIN.rgb());
            } else {
                im3.setPixel(i, j, PAPIRSZIN.rgb());
            }
        }
    }

    if (pot1 == 5) {
        //im3 kirajzolása tesztpadon:
        w->tesztPad->ui->label_3->setPixmap(QPixmap::fromImage(
                im3.scaled(w->tesztPad->ui->label_3->size(),
                Qt::KeepAspectRatio)));
    }

    //egyengetés:
    int szamlalo = 0;
    vanValtozas = true;
    while (szamlalo < pot7 && vanValtozas) {
        vanValtozas = false;
        szamlalo++;
        im2 = im3.copy(im3.rect());

        for (int j=1; j<im2.height()-1; j++) {
           for (int i=1; i<im2.width()-1; i++) {
                if (workerThread->m_abort) {
                    return;
                }
                QColor c(im2.pixel(i, j));
                if (c == TINTASZIN){
                    QString st = kornyekiMxSt(im2, i, j);
                    if (    st == "" ||
                            st == "00000000" ||
                            st == "11100000" ||
                            st == "00101001" ||
                            st == "00000111" ||
                            st == "11101001" ||     //  L
                            st == "00101111" ||     //  L
                            st == "10010111" ||     //  L
                            st == "11110100" ||     //  L
                            st == "10010100"    ) {
                        im3.setPixel(i, j, PAPIRSZIN.rgb());
                        vanValtozas = true;
                    }
                } else if (c == PAPIRSZIN){
                    QString st = kornyekiMxSt(im2, i, j);
                    if (    st == "11111111" ||     //  \
                            st == "10000001" ||     //  \
                            st == "00100100" ||     //  /
                            st == "00011000" ||     //  -
                            st == "01000010" ||     //  |
                            st == "01011010" ||     //  +
                            st == "11111000" ||     //  u
                            st == "01101011" ||     //  u
                            st == "00011111" ||     //  u
                            st == "11010110" ||     //  u
                            st == "00010010" ||     //
                            st == "01010000" ||     //
                            st == "01001000" ||     //
                            st == "00001010" ||     //
                            st == "01010010" ||     //
                            st == "01011000" ||     //
                            st == "01001010" ||     //
                            st == "00011010" ||     //
                            st.replace("0", "").length() >=6    ) {
                        im3.setPixel(i, j, TINTASZIN.rgb());
                        vanValtozas = true;
                    }
                }
            }
        }
    };

    if (pot1 == 6) {
        //im3 kirajzolása tesztpadon egyengetés után:
        w->tesztPad->ui->label_3->setPixmap(QPixmap::fromImage(
                im3.scaled(w->tesztPad->ui->label_3->size(),
                Qt::KeepAspectRatio)));
    }

    for (int i=0; i<im1.width(); i++) {
        free(mezo[i]);
        free(mezo2[i]);
        free(mezoH[i]);
        free(mezoV[i]);
    }
    free(mezo);
    free(mezo2);
    free(mezoH);
    free(mezoV);

    if (!w->tesztPad->isVisible()) {
        felismer();
    }
}

void OcrOpt::felismer() {
    w->tesztPad->ui->pushButton->setEnabled(false);
    w->tesztPad->ui->pushButton->setVisible(false);
    w->tesztPad->ui->tedit->setPlainText("");

    // 副圃涮 「
    // 副圃涮 「

    QString st = exePath + "../resource/" + "tmp2.bmp";
    im3.save(st, "BMP");

    // read image
    PIX* pixs = pixRead(st.toUtf8());
    if (!pixs) {
        qWarning("Cannot open input file: " + st.toUtf8());
        return;
    }

    // recognize
    tess.SetImage(pixs);
    tess.Recognize(0);

    // get result and delete[] returned char* string
//    std::cout << std::unique_ptr<char[]>(tess.GetUTF8Text()).get() << std::endl;
    char *ch = tess.GetUTF8Text();
    ocrTxt = QString::fromUtf8(ch);
    ocrTxt = ocrTxt.trimmed();
    ocrTxt = ocrTxt.replace(" ", "").replace("_", QString::fromUtf8("一"));
    w->tesztPad->ui->tedit->setPlainText(ocrTxt);
    if (w->teL[9]->textCursor().selectedText() != "") {
        st = ocrTxt;
    } else {
        st = w->teL[9]->toPlainText();
        if (st.endsWith("+")) {
            st = st.left(st.length() - 1) + " " + ocrTxt;
        } else if (st.startsWith("+")) {
            st = ocrTxt + " " + st.right(st.length() - 1);
        } else {
            st = ocrTxt;
        }
    }
    w->teL[9]->setText2(st);
    w->teL[9]->setLathato(true);
    if (w->cedictAblakState == 1) {
        w->cedictAblakState = 2;
    }
    w->mySelectionChanged(w->teL[9]);
    delete[] ch;

    // cleanup
    tess.Clear();
    pixDestroy(&pixs);
    w->tesztPad->ui->pushButton->setEnabled(true);
    w->tesztPad->ui->pushButton->setVisible(true);

    w->teL[9]->selectAll();
    w->mySelectionChanged(w->teL[9]);
    w->teL[9]->setFocus();
}

QPoint OcrOpt::saMap(QPoint p0) {
//w pos -> img pos
    QPoint p = w->player->mapFromParent(p0);
    p = w->player->keret->mapFromParent(p);
    QImage im = QImage(exePath + "../resource/" + "tmp.png");
    double imw = im.width();
    double imh = im.height();
    double kerw = w->player->keret->width();
    double kerh = w->player->keret->height();
    double kerhIdealis = kerw / imw * imh;
    double korr = 0.5 * (kerh - kerhIdealis);
    QPoint pEr;
    if (korr > 0.0) {
        pEr.setX(1.0 * imw / kerw * p.x());
        pEr.setY(kerh / kerhIdealis * imh / kerh * (p.y() - korr));
    } else {
        double kerwIdealis = kerh / imh * imw;
        korr = 0.5 * (kerw - kerwIdealis);
        pEr.setX(kerw / kerwIdealis * imw / kerw * (p.x() - korr));
        pEr.setY(1.0 * imh / kerh * (p.y()));
    }
    return pEr;
}

QString OcrOpt::kornyekiMxSt(QImage im, int x, int y) {
    QString er ="";
    if (    x == 0 || x == im.width() - 1 ||
            y == 0 || y == im.height() - 1   ) {
        ;   //semmi
    } else {
        er += im.pixel(x-1, y-1) == TINTASZIN.rgb() ? "1" : "0";
        er += im.pixel(x,   y-1) == TINTASZIN.rgb() ? "1" : "0";
        er += im.pixel(x+1, y-1) == TINTASZIN.rgb() ? "1" : "0";
        er += im.pixel(x-1, y)   == TINTASZIN.rgb() ? "1" : "0";
        er += im.pixel(x+1, y)   == TINTASZIN.rgb() ? "1" : "0";
        er += im.pixel(x-1, y+1) == TINTASZIN.rgb() ? "1" : "0";
        er += im.pixel(x,   y+1) == TINTASZIN.rgb() ? "1" : "0";
        er += im.pixel(x+1, y+1) == TINTASZIN.rgb() ? "1" : "0";
    }
    return er;
}

quint8 OcrOpt::szinHasonlosag(const QColor &c1, const QColor &c2) {
    #define C 255.0 / (255.0 * sqrt(3.0));
    double er1 = pow(c1.red() - c2.red(), 2.0) + pow(c1.green() - c2.green(), 2.0) +
            pow(c1.blue() - c2.blue(), 2.0);
    double er2 = sqrt(er1);
    int er3 = er2 * C;
    //qWarning("er1: %f, er2: %f, er3: %d", er1, er2, er3);
    if (er3 < 0 || er3 > 255) {
        qWarning("HIBA, OcrOpt::szinHasonlosag, er3: %d", er3);
        exit(1);
    }
    return er3;
}

bool OcrOpt::isHasonloSzin(const QColor &c1, const QColor &c2, int d) {
    return (this->szinHasonlosag(c1, c2) <= d);
}

bool OcrOpt::isHatarSzin(quint8 **mezo, const QImage &img, int x, int y, int x2, int y2, int d) {
    return (mezo[x2][y2] == 1 && isHasonloSzin(img.pixel(x, y), img.pixel(x2, y2), d));
}

OcrOpt::hvHasonlosag OcrOpt::hvHasonlosagFv(QColor c1, QColor c2, int d) {
    quint8 h2 = szinHasonlosag(this->videoFeliratSzin, c2);
    quint8 h1 = szinHasonlosag(this->videoFeliratSzin, c1);
    if (h1-h2 > d) {
        return novekszik;
    } else if (h2-h1 > d) {
        return csokken;
    } else {
        return stagnal;
    }
}

void OcrOpt::nullasokBeallitasa(char mezoHV, int tol, int ig, int k, int mire) {
    if (mezoHV == 'H') {
        for(int i=tol; i<=ig; i++) {
            mezoH[i][k] = mire;
        }
    } else if (mezoHV == 'V') {
        for(int j=tol; j<=ig; j++) {
            mezoV[k][j] = mire;
        }
    }
}

bool OcrOpt::szomszedPontVizsgalat(int i, int j) {
    bool t = false;
    t = t || (mezo[i][j] == 2);     //biztosan video felirat szín
    t = t || (mezoH[i][j] == 3);    //tetőpont
    t = t || (mezoV[i][j] == 3);    //tetőpont
    return t;
}
