#ifndef TESZTPAD_H
#define TESZTPAD_H

#include <QWidget>
#include <QCheckBox>
#include "tessdatapanel.h"
#include "ui_tessdatapanel.h"

namespace Ui {
class TesztPad;
}

class TesztPad : public QWidget
{
    Q_OBJECT
    
public:
    explicit TesztPad(QWidget *parent = 0);
    ~TesztPad();
    Ui::TesztPad *ui;
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void keyPressEvent(QKeyEvent *e);
    TessdataPanel *tessdataPanel;
    QList<QCheckBox*> cbList;
    void setColor(QColor);

public slots:
    void showTessdataPanel();
    void okTessdataPanel();
};

#endif // TESZTPAD_H
