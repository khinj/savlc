﻿SaVLC
======

[Description in English here.](../readme.md)  

A SaVLC elsősorban azért lett kifejlesztve, hogy a feliratok tetszőlegesen elhelyezhetők legyenek. Ez a program *.srt feliratfájlokat (SubRip felirat szöveg fájl formátum) tud kezelni. A feliratok betűmérete Ctrl + egér görgővel beállítható. Lehetőség van a szövegmezők betűtípusának, betűszínének és háttérszínének beállítására is. A SaVLC rendelkezik továbbá egyéb, a VLC médialejátszóban is ismert funkciókkal, pl. gyorsabb - lassabb tempó, A-B szakasz, világosság, kontraszt, telítettség beállítás. Extra funkció: a [CedictServer](https://bitbucket.org/khinj/cedictserver)-hez kapcsolódva a kínai felirat kijelölt szavait azonnal keresi a CEDICT szótárban.

A program és minta video innen tölthető le: [SaVLC.zip, SaVLC - Minta Video - Sample Video.zip](https://1drv.ms/f/s!AhEINfn0bmWKgQavfQq5bVxJpoRv). Kicsomagolás után futtasd a SaVLC\release\SaVLC.exe programot.

![Image](SaVLC.jpg "screenshot")

**Web:** [https://bitbucket.org/khinj/SaVLC](https://bitbucket.org/khinj/SaVLC).

**Rövid leírás:**

A SaVLC alkalmazást azért kezdtem el fejleszteni, hogy segítse a kínai nyelvtanulásomat. Egy VLC-hez hasonló videólejátszót akartam, de olyat, amely a feliratokat kifinomultabban tudja kezelni, például egyszerre mutatja a kínai, pinjin és a fordítás feliratait, és könnyebben beállítható a betűméret vagy a felirat helye a képernyőn. Ezt az alkalmazást Windows 10 operációs rendszeren fejlesztetem.

**Felhasználóknak:**

Töltsd le a `SaVLC.zip` fájlt a [OneDrive](https://1drv.ms/f/s!AhEINfn0bmWKgQavfQq5bVxJpoRv) helyről, és csomagold ki. A `saVLC.exe` program a `SaVLC\Bin\` mappában van.  
Miután elindítottad az alkalmazást, nyomd meg az F1-t, ez megmutatja, milyen funkciók érhetők el milyen gyorsbillentyűkkel.  
Letöltheted a [SaVLC - Minta Video - Sample Video.zip](https://1drv.ms/f/s!AhEINfn0bmWKgQavfQq5bVxJpoRv) fájlt is, amely minta video és felirat fájlokat tartalmaz, ezzel kipróbálhatod, hogyan működik a SaVLC.

 

**Választható kiegészítések:**

* [CedictServer](https://bitbucket.org/khinj/cedictserver): ennek használatával látható a kijelölt kínai felirat fordítása.

* tessdata: Használható karakterfelismarés (OCR - Optical Character Recognition) némi korlátokkal (nem jól működik olyankor, amikor a felirat színe hasonló a héttér színéhez). Töltsd le a `chi_sim.traineddata` és/vagy a `chi_tra.traineddata` fájlokat a [Data Files for Version 3.04/3.05](https://github.com/tesseract-ocr/tesseract/wiki/Data-Files#data-files-for-version-304305) tesseract weboldalról, és tedd be ezeket a fájlokat a `SaVLC\tessdata\` könyvtárba.

**Fejlesztőknek:**

Ezeket a "hozzávalókat" használtam a SaVLC fejlesztésekor:

* Windows 10 operációs rendszer (Win OS)

* Qt4.8

* Qt Creator 2.5.2

* MinGW (gcc (GCC) 4.8.1)

* Git

* [vlc-3.0.21.tar.xz](https://download.videolan.org/pub/videolan/vlc/3.0.21/)

* [vlc-3.0.21-win32.zip](https://download.videolan.org/pub/videolan/vlc/3.0.21/win32/)

* tesseract (3.05.01)

* leptonica (commit 3860acc9bb03e7cf79c47e7b798f1bdfeae1a4e6, ez volt a legutóbbi commit a master branch-en 2017-12-15 20:08:05-kor).

Más verziókat is használhatsz, de így nagyobb az esélye, hogy módosítanod kell a *.pro fájlokat vagy a forráskódot a sikeres build-eléshez (különösen a Qt esetében).

A sikeres build-elés után a `SaVLC\bin_dev\` könytár tartalmazza a `saVLC.exe`-t. Ebben lesznek a `libtessQt.a` és a `tessQt.dll` fájlok is, amelyek más projektekben is használhatók a tesseract-os `baseapi.h`-val és a leptonica-s `allheaders.h`-val együtt.

1.) Miután a Win OS, Qt4.8, Qt Creator, MinGW, Git installálva van, klónozd a [git repo of SaVLC](https://bitbucket.org/khinj/savlc/src/master/)-t, például így:

    git clone https://bitbucket.org/khinj/savlc.git

2.) A `vlc-x.x.x.tar.xz`-ben levő `vlc-x.x.x\include\vlc\`-ből másold át a fájlokat a `SaVLC\src\vlc\include\vlc\`-be (pl. [7-Zip](https://www.7-zip.org/)-pel).

3.) A `vlc-x.x.x-win32.zip`-ből másold át a `plugins` mappát (tartalmával együtt), továbbá a `libvlc.dll` és a `libvlccore.dll` fájlokat a `vlc-x.x.x\`-ból a `SaVLC\bin\`-ba.

4.) A git-bash -ban használd ezeket a parancsokat:  

    cd SaVLC\src\tess\  
    git clone https://github.com/tesseract-ocr/tesseract  
    cd tesseract  
    git checkout 3.05.01  
    cd ..  
    git clone https://github.com/DanBloomberg/eptonica.git  
    cd leptonica  
    git checkout 3860acc9bb03e7cf79c47e7b798f1bdfeae1a4e6

5.) Módosítsd a `SaVLC\src\tess\leptonica\src\environ.h` fájlt úgy, hogy ezek a makrók 0-k legyenek, kivéve: USE_BMPIO legyen 1:

    #define  HAVE_LIBJPEG     0  
    #define  HAVE_LIBTIFF     0  
    #define  HAVE_LIBPNG      0  
    #define  HAVE_LIBZ        0  
    #define  HAVE_LIBGIF      0  
    #define  HAVE_LIBUNGIF    0  
    #define  HAVE_LIBWEBP     0  
    #define  HAVE_LIBJP2K     0  
    ...  
    #define  USE_BMPIO        1  
    #define  USE_PNMIO        0  
    #define  USE_JP2KHEADER   0  
    #define  USE_PDFIO        0  
    #define  USE_PSIO         0

6.) A `SaVLC\src\tess\tesseract\ccutil\ccutil.h` fájlban, az első "#ifdef _WIN32" sor után tedd be ezt a sort:

    #define WINDLLNAME "libtesseract.dll"

7.) A Qt Creator -ban nyisd meg a `SaVLC\src\SaVLC.pro` fájlt, és módosítsd a forráskódot, ahogy akarod, majd build-ej. Sok szerencsét!

**Licensz:**

Az általam hozzáadott rész teljesen szabadon felhasználható. A fentebb említett "hozzávalók" licenszei a nekik megfelelő webhelyeken vannak. Nincs semmi garancia.
